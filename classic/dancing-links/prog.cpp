#include <iostream>
#include <vector>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
* Dancing Links, Partitions, Generating Subsets
*/

/**
* Linked list
* - all items have 'head' pointer
* - both side of the list indicate "the end" by pointing to 'head'
*/
struct node_type {
	node_type(): val(0), prev(0), next(0) {
		this->head = this;
	}
	node_type(node_type* head, uint32 val): val(val), head(head), prev(head), next(head) {
		if (!this->head->next) {
			this->head->next = this;
		}
	}

	uint32 val;
	node_type *head, *prev, *next;
};

void print_subsets(node_type* curr) {
	if (!curr) return;

	if (curr == curr->head) {
		for (node_type* c = curr->head->next; c != curr; c = c->next) {
			cout << c->val << ' ';
		}
		cout << endl;
		return;
	}

	/**
	* `curr` is kept.
	*/
	print_subsets(curr->next);

	/**
	* `curr` is removed.
	*/
	curr->prev->next = curr->next;
	curr->next->prev = curr->prev;
	print_subsets(curr->next);
	curr->next->prev = curr;
	curr->prev->next = curr;
}

int main(void) {
	ios::sync_with_stdio(false);

	cout << "Enter size of set <number>:";
	uint32 set_size = 0; cin >> set_size;
	vector<uint32> L;
	for (uint32 i = 1; i <= set_size; i++) {
		L.push_back(i);
	}

	vector<node_type*> nodes;
	nodes.push_back(new node_type());
	node_type* head = nodes.back();
	node_type* prev = head;
	for (uint32 l_ix = 0; l_ix < L.size(); l_ix++) {
		nodes.push_back(new node_type(head, L[l_ix]));
		node_type* curr = nodes.back();
		curr->prev = prev;
		prev->next = curr;
		prev = head->prev = curr;
	}
	cout << "All subsets: " << endl;
	print_subsets(head->next);

	for (uint32 i = 0; i < nodes.size(); i++) delete nodes[i];
	nodes.clear();

	return 0;
}
