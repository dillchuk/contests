#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <list>
#include <string>
#include <set>
#include <time.h>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* 2D 2-Dimensional String Search, Aho-Corasick, KMP.
*
* Research: http://www.cs.uku.fi/~kilpelai/BSA05/lectures/slides04.pdf
*/

struct node_type {

	/**
	* Root constructor.
	*/
	node_type(string alphabet): alphabet(alphabet), root(this), fail(0) {
		this->ix = this->nodes.size();
		this->nodes.push_back(this);

		for (string::iterator a_it = this->alphabet.begin();
			a_it != this->alphabet.end(); ++a_it) {
			this->links[*a_it] = this;
		}
	}

	/**
	* Child constructor.
	*/
	node_type(node_type* parent): root(parent->root), fail(0) {
		this->ix = this->root->nodes.size();
		this->root->nodes.push_back(this);
	}

	~node_type() {
		if (!this->is_root()) return;
		for (uint32 i = 1; i < this->nodes.size(); i++) {
			delete this->nodes[i];
		}
		this->nodes.clear();
	}

	node_type* follow(char c) {
		node_type* curr = this;
		while (curr->go(c) == 0) {
			curr = curr->fail;
		}
		return curr->go(c);
	}

	node_type* go(char c) {
		return this->links[c];
	}

	node_type* set_go(char c) {
		node_type* go = this->links[c];
		if (go && go->is_root()) {
			go = 0;
		}
		if (go == 0) {
			this->links[c] = new node_type(this);
		}
		return this->links[c];
	}

	void set_word_end(const string& word) {
		this->words.insert(word);
	}

	bool is_root() const {
		return (this->root->nodes[0] == this);
	}

	uint32 ix;
	map<char, node_type*> links;

	set<string> words;

	node_type* fail;

	node_type* root;

	/**
	* Root only.
	*/
	string alphabet;
	vector<node_type*> nodes;
};

void phase1_add_words(node_type* root, const vector<string>& words) {
	if (!root->is_root()) {
		cout << "Called `phase1` on non-root";
		exit(1);
	}

	for (vector<string>::const_iterator w_it = words.begin(); w_it != words.end(); ++w_it) {
		node_type* curr = root;
		for (string::const_iterator c_it = w_it->begin(); c_it != w_it->end(); ++c_it) {
			curr = curr->set_go(*c_it);
		}
		curr->set_word_end(*w_it);
	}
}

void phase2_automatize(node_type* root) {
	if (!root->is_root()) {
		cout << "Called `phase2` on non-root";
		exit(1);
	}
	list<node_type*> queue;
	for (string::const_iterator a_it = root->alphabet.begin(); a_it != root->alphabet.end(); ++a_it) {
		node_type* q = root->go(*a_it);
		if (!q->is_root()) {
			q->fail = q->root;
			queue.push_back(q);
		}
	}
	while (!queue.empty()) {
		node_type* r = queue.front(); queue.pop_front();
		for (string::const_iterator a_it = root->alphabet.begin(); a_it != root->alphabet.end(); ++a_it) {
			node_type* u = r->go(*a_it);
			if (u != 0) {
				queue.push_back(u);
				u->fail = r->fail->follow(*a_it);
				copy(u->fail->words.begin(), u->fail->words.end(), inserter(u->words, u->words.begin()));
			}
		}
	}
}

/**
* Find finishing state of the given word.  Then, whenever search
* encounters this node, this word has just been found.
*
* N.B. Use during a 2D search:
* 1. Create dictionary containing each row of 2D needle,
*    then generate list containing final states for each (in order)
* 2. Pass row by row in haystack, replacing character with current dictionary state
* 3. Pass col by col from 2, searching for list (from 1) using KMP.
*/
node_type* find_word(node_type* root, const string& word) {
	node_type* curr = root;
	for (string::const_iterator c_it = word.begin(); c_it != word.end(); ++c_it) {
		curr = curr->go(*c_it);
		if (curr == 0) return 0;
	}
	return curr;
}

/**
* Restartable KMP Search, use as follows:
*
* uint32 h_ix = 0, n_ix = 0;
* vector<int32> kmptab = kmp_table(needle);
* for (;;) {
*	h_ix = kmp_search(haystack, needle, kmptab, h_ix, n_ix);
*	if (h_ix == haystack.size()) break;
*
*	n_ix = kmptab.back();
*	h_ix += needle.size() - n_ix;
* }
*
* If found, returns index.  If not, returns size of haystack (invalid index).
*/
vector<int32> kmp_table(vector<uint32> w);
uint32 kmp_search(const vector<uint32>& haystack, const vector<uint32>& needle, const vector<int32>& kmptab, uint32 h_ix = 0, uint32 n_ix = 0) {

	while (h_ix + n_ix < haystack.size()) {
		if (needle[n_ix] == haystack[h_ix + n_ix]) {
			if (n_ix == needle.size() - 1) return h_ix;
			n_ix++;
		}
		else {
			h_ix += n_ix - kmptab[n_ix];
			if (kmptab[n_ix] >= 0) {
				n_ix = kmptab[n_ix];
			}
			else {
				n_ix = 0;
			}
		}
	}

	/**
	* Not found, return string end.
	*/
	return haystack.size();
}
vector<int32> kmp_table(vector<uint32> w) {

	/**
	* Makes for restartable search.
	* Optimization idea: make input const reference, generate final value below explicitly.
	*/
	w.push_back(0);

	uint32 pos = 2, cand = 0;

	vector<int32> t(max(static_cast<int32>(2), static_cast<int32>(w.size())));
	t[0] = -1;
	t[1] = 0;
	while (pos < w.size()) {
		if (w[pos - 1] == w[cand]) {
			cand++;
			t[pos] = cand;
			pos++;
		}
		else if (cand > 0) {
			cand = t[cand];
		}
		else {
			t[pos] = 0;
			pos++;
		}
	}
	return t;
}

int main(void) {
	srand(time(0));
	ios::sync_with_stdio(false);

	/**
	* Inputs.
	*/
	string alphabet = "abcde";

	vector<string> needle_rows;
	uint32 nrows = 3, ncols = 2;
	for (uint32 r_ix = 0; r_ix < nrows; r_ix++) {
		string nrow;
		for (uint32 c_ix = 0; c_ix < ncols; c_ix++) {
			nrow += alphabet[rand() % alphabet.size()];
		}
		needle_rows.push_back(nrow);
	}

	vector<string> haystack_rows;
	uint32 hrows = 2000, hcols = 3000;
	for (uint32 r_ix = 0; r_ix < hrows; r_ix++) {
		string hrow;
		for (uint32 c_ix = 0; c_ix < hcols; c_ix++) {
			hrow += alphabet[rand() % alphabet.size()];
		}
		haystack_rows.push_back(hrow);
	}

	/**
	* Create Aho-Corasick dictionary.
	*/
	node_type* root = new node_type(alphabet);
	phase1_add_words(root, needle_rows);
	phase2_automatize(root);

	/**
	* Run each row of the haystack through our dictionary and
	* record the results of each cell in h_state_cols. Flip row/columns
	* since the columns become our next focus.
	*/
	vector< vector<uint32> > h_state_cols;
	for (uint32 c_ix = 0; c_ix < hcols; c_ix++) {
		h_state_cols.push_back(vector<uint32>(hrows));
	}
	for (uint32 r_ix = 0; r_ix < haystack_rows.size(); r_ix++) {
		node_type* curr = root;
		for (uint32 c_ix = 0; c_ix < haystack_rows[r_ix].size(); c_ix++) {
			curr = curr->follow(haystack_rows[r_ix][c_ix]);
			h_state_cols[c_ix][r_ix] = curr->ix;
		}
	}

	/**
	* Where haystack contains needle, we'll find n_fingerprint running down that column.
	*/
	vector<uint32> n_fingerprint;
	for (uint32 n_ix = 0; n_ix < needle_rows.size(); n_ix++) {
		node_type* state = find_word(root, needle_rows[n_ix]);
		n_fingerprint.push_back(state->ix);
	}
	uint32 found_count = 0;
	for (uint32 c_ix = ncols - 1; c_ix < h_state_cols.size(); c_ix++) {
		uint32 h_ix = 0, n_ix = 0;
		vector<int32> kmptab = kmp_table(n_fingerprint);
		for (;;) {
			h_ix = kmp_search(h_state_cols[c_ix], n_fingerprint, kmptab, h_ix, n_ix);
			if (h_ix == haystack_rows.size()) break;

			found_count++;
			n_ix = kmptab.back();
			h_ix += needle_rows.size() - n_ix;
		}
	}
	cout << "Total found: " << found_count << endl;

	delete root; root = 0;
	return 0;
}
