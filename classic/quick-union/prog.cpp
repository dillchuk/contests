#include <iostream>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Performs a series of unions, returning the distinct set IDs.
*
* Quick Union, Path Compression
* Algorithms in C++ Program 1.3 & 1.4
*/

#define MAX_ID 100000000

struct quick_union {

	quick_union(): sets(1), size(1) {}

	/**
	* Insert linked pair or a lone (unlinked) item.
	* Keep IDs as low as you can.
	*/
	void insert(uint32 a, uint32 b = 0) {
		if (b == 0) b = a;

		if (a == 0) {
			cout << "Items must have id > 0" << endl;
			exit(1);
		}

		/**
		* Grow.
		*/
		uint32 max_id = max(max(a, b), static_cast<uint32>(this->sets.size() - 1));
		if (max_id > MAX_ID) {
			cout << "ID is too high" << endl;
			exit(1);
		}
		if (max_id >= this->sets.size()) {
			this->sets.resize(max_id + 1);
			this->size.resize(max_id + 1);
		}
		if (this->sets[a] == 0) this->sets[a] = a;
		if (this->sets[b] == 0) this->sets[b] = b;
		this->size[a] = max(this->size[a], static_cast<uint32>(1));
		this->size[b] = max(this->size[b], static_cast<uint32>(1));

		a = this->find(a);
		b = this->find(b);
		if (a == b) return;

		/**
		* Weighted union; keep tree balanced.
		*/
		if (this->size[a] < this->size[b]) {
			this->sets[a] = b;
			this->size[b] += this->size[a];
		}
		else {
			this->sets[b] = a;
			this->size[a] += this->size[b];
		}
	}

	uint32 find(uint32 id) {
		if (id >= this->sets.size()) {
			cout << "find() ID too large, not encountered yet" << endl;
			exit(1);
		}
		for (; id != this->sets[id]; id = this->sets[id]) {
			this->sets[id] = this->sets[this->sets[id]]; // collapse tree a bit
		}
		if (id == 0) {
			cout << "find() ID not encountered yet" << endl;
			exit(1);
		}
		return id;
	}

	uint32 count() {
		set<uint32> set_ids;
		for (uint32 i = 1; i < this->sets.size(); i++) {
			if (this->sets[i] == 0) continue;
			set_ids.insert(this->find(i));
		}
		return set_ids.size();
	}

	vector<uint32> sets, size;
};

int main(void) {
	ios::sync_with_stdio(false);

	quick_union qu;

	qu.insert(1, 2);
	cout << qu.count() << endl;
	qu.insert(5, 8);
	cout << qu.count() << endl;
	qu.insert(9, 4);
	cout << qu.count() << endl;
	qu.insert(9, 6);
	cout << qu.count() << endl;
	qu.insert(3, 5);
	cout << qu.count() << endl;
	qu.insert(7, 1);
	cout << qu.count() << endl;
	qu.insert(2, 5);
	cout << qu.count() << endl;
	qu.insert(3, 2);
	cout << qu.count() << endl;

	return 0;
}
