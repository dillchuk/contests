#include <iostream>
#include <vector>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* Binary Indexed Tree.
*
* @see http://community.topcoder.com/tc?module=Static&d1=tutorials&d2=binaryIndexedTrees
* @see https://en.wikipedia.org/wiki/Fenwick_tree
*/

/**
* Suitable for:
* - adding values to all indices across a range; O(lg n)
* - setting individual values; O(lg n)
* - querying individual values; O(lg n)
*
* Not suitable for:
* - querying sum totals across a range
*
* N.B. Indexed from [1, n].
*/
struct binary_indexed_tree {

	binary_indexed_tree(uint32 n): atom(n + 1), range(n + 1) {}

	/**
	* Returns the value of the last 1-bit.
	* e.g. 1011000 => 1000
	* (And therefore 1011000 is responsible for [1011000 - 1000 + 1, 1011000])
	*/
#define RESPONSIBILITY(id) (id & -static_cast<int32>(id))

	int64 get(uint32 id) const {
		int64 sum = this->atom[id];
		for (uint32 i = id; i < this->range.size() && i > 0; i += RESPONSIBILITY(i)) {
			sum += this->range[i];
		}
		return sum;
	}

	void set(uint32 id, int64 val) {
		int64 orig = this->get(id);
		this->atom[id] += val - orig;
	}

	void add(uint32 ida, uint32 idb, int64 val) {
		uint32 i = ida;
		while (i <= idb && i > 0) {
			bool is_range = false;
			for (;;) {

				/**
				* Looking at 1001, the digits above (rightmost 1) responsibility are 0.
				* That's good and means those ranges are still open and available.
				*
				* Looking at 1011, the digit directly above responsiblility is a 1.
				* That's no good: that range has closed.
				*/
				uint32 ri = RESPONSIBILITY(i);
				bool bigger_range_open = (RESPONSIBILITY(i + ri) >> 1 == ri);
				bool too_low = (i - ri + 1 < ida);
				bool too_high = (i + ri > idb);
				if (!bigger_range_open || too_low || too_high) break;

				i += ri;
				is_range = true;
			}
			vector<int64>& to = is_range ? this->range : this->atom;
			to[i] += val;
			i++;
		}
	}

	vector<int64> atom, range;
};

int main(void) {
	binary_indexed_tree bit(20000000);
	bit.add(1, 18000000, 10);
	bit.add(100000, 10000000, -5);
	bit.add(4000001, 20000000, 22);
	bit.set(4000001, 101);
	cout << bit.get(4000000) << ' ' << bit.get(4000001) << ' ' << bit.get(4000002) << endl;
	return 0;
}

