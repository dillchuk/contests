#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*/

/**
* Restartable KMP Search, use as follows:
*
* uint32 h_ix = 0, n_ix = 0;
* vector<int32> kmptab = kmp_table(needle);
* for (;;) {
*	h_ix = kmp_search(haystack, needle, kmptab, h_ix, n_ix);
*	if (h_ix == haystack.size()) break;
*
*	n_ix = kmptab.back();
*	h_ix += needle.size() - n_ix;
* }
*
* If found, returns index.  If not, returns size of haystack (invalid index).
*/
vector<int32> kmp_table(vector<uint32> w);
uint32 kmp_search(const vector<uint32>& haystack, const vector<uint32>& needle, const vector<int32>& kmptab, uint32 h_ix = 0, uint32 n_ix = 0) {

	while (h_ix + n_ix < haystack.size()) {
		if (needle[n_ix] == haystack[h_ix + n_ix]) {
			if (n_ix == needle.size() - 1) return h_ix;
			n_ix++;
		}
		else {
			h_ix += n_ix - kmptab[n_ix];
			if (kmptab[n_ix] >= 0) {
				n_ix = kmptab[n_ix];
			}
			else {
				n_ix = 0;
			}
		}
	}

	/**
	* Not found, return string end.
	*/
	return haystack.size();
}
vector<int32> kmp_table(vector<uint32> w) {

	/**
	* Makes for restartable search.
	* Optimization idea: make input const reference, generate final value below explicitly.
	*/
	w.push_back(0);

	uint32 pos = 2, cand = 0;

	vector<int32> t(max(static_cast<int32>(2), static_cast<int32>(w.size())));
	t[0] = -1;
	t[1] = 0;
	while (pos < w.size()) {
		if (w[pos - 1] == w[cand]) {
			cand++;
			t[pos] = cand;
			pos++;
		}
		else if (cand > 0) {
			cand = t[cand];
		}
		else {
			t[pos] = 0;
			pos++;
		}
	}
	return t;
}

int main(void) {
	return 0;
}
