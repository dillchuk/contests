#include <iostream>
#include <vector>
#include <string>

using namespace std;
typedef unsigned long uint32;
typedef long int32;

/**
* Author Derek Illchuk dillchuk@gmail.com
*
* Algorithm X, Queens problem, Dancing links, Backtracking, Exact cover problem.
*
* See http://arxiv.org/pdf/cs.DS/0011047.pdf
* See https://en.wikipedia.org/wiki/Knuth%27s_Algorithm_X
*/

struct column_type {
	column_type(): ix(-1), is_primary(true) {}
	column_type(uint32 ix, string descrip, bool is_primary = true): ix(ix), descrip(descrip), is_primary(is_primary) {}
	string descrip;
	int32 ix;

	/**
	* true: must be covered once.
	* false: covered once or not at all.
	*/
	bool is_primary;
};
bool operator<(const column_type& lhs, const column_type& rhs) {
	return (lhs.ix < rhs.ix);
}

struct node_type {

	node_type(): l(0), r(0), u(0), d(0), c(0), size(0) {}

	static void cover_column(node_type* header) {
		header->l->r = header->r;
		header->r->l = header->l;
		for (node_type* i = header->d; i != header; i = i->d) {
			for (node_type* j = i->r; j != i; j = j->r) {
				j->u->d = j->d;
				j->d->u = j->u;
				j->c->size--;
			}
		}
	}
	static void uncover_column(node_type* header) {
		for (node_type* i = header->u; i != header; i = i->u) {
			for (node_type* j = i->l; j != i; j = j->l) {
				j->c->size++;
				j->u->d = j;
				j->d->u = j;
			}
		}
		header->l->r = header;
		header->r->l = header;
	}

	node_type *l, *r; // left/right
	node_type *u, *d; // up/down
	node_type *c;     // column header

	column_type col;

	/**
	 * Used only in column header.
	 */
	uint32 size;
};

struct root_type: node_type {

	node_type* new_node(column_type col = column_type()) {
		node_type* node = new node_type();
		node->col = col;
		this->nodes.push_back(node);
		return node;
	}

	~root_type() {
		for (
			vector<node_type*>::iterator child_it = this->nodes.begin();
			child_it != this->nodes.end(); ++child_it
			) {
			delete *child_it;
		}
		this->nodes.clear();
	}

	/**
	 * Creates the entire structure.
	 */
	root_type(vector<column_type>& cols, vector< vector<uint32> >& grid_cols_spec) {
		uint32 num_cols = cols.size();
		uint32 num_rows = grid_cols_spec[0].size();

		vector< vector<node_type*> > grid_cols(num_cols);
		for (uint32 col_ix = 0; col_ix < num_cols; col_ix++) {
			grid_cols[col_ix].resize(num_rows);

			for (uint32 row_ix = 0; row_ix < num_rows; row_ix++) {
				if (grid_cols_spec[col_ix][row_ix] == 0) continue;
				node_type* node = this->new_node(cols[col_ix]);
				grid_cols[node->col.ix][row_ix] = node;
			}
		}

		/**
		 * Column headers are created/linked in a circular fashion.
		 *
		 * It looks like this:
		 * root <==> col 0 <==> col 1 <==> ... <==> root
		 */
		node_type* prev_header = this;
		for (uint32 col_ix = 0; col_ix < num_cols; col_ix++) {
			node_type* header = this->new_node(cols[col_ix]);
			header->c = header;
			header->l = prev_header;
			header->l->r = header;
			prev_header = header;
		}
		this->l = prev_header;
		prev_header->r = this;

		/**
		 * Convert the grid to our doubly-linked circular node lists.
		 * Really, see Dancing Links, Donald Knuth.
		 *
		 * It looks like this:
		 * col 0 <==> col 1 <==> ...
		 *  ||         ||
		 * node  <==> node <==>  ...
		 *  ||         ||
		 * node  <=============> ...
		 *  ||         ||
		 * col 0      col 1
		 */
		for (node_type* header = this->r; header != this; header = header->r) {
			node_type *back = header;
			uint32 size = 0;
			for (uint32 row_ix = 0; row_ix < grid_cols[0].size(); row_ix++) {
				node_type* curr = grid_cols[header->col.ix][row_ix];
				if (curr == 0) continue;

				curr->c = header;
				curr->u = back;
				back->d = curr;
				back = curr;
				size++;
			}
			header->u = back;
			back->d = header;
			header->size = size;
		}
		for (uint32 row_ix = 0; row_ix < grid_cols_spec[0].size(); row_ix++) {
			node_type *front = 0, *back = 0;
			for (node_type* header = this->r; header != this; header = header->r) {

				node_type* curr = grid_cols[header->col.ix][row_ix];
				if (curr == 0) continue;

				if (back) {
					curr->l = back;
					back->r = curr;
				}
				if (front == 0) {
					front = curr;
				}
				back = curr;
			}
			if (back) {
				front->l = back;
				back->r = front;
			}
		}
	}

	bool search(vector< vector<column_type> >& solution) {

		/**
		* Success: primary covered.
		*/
		bool all_covered = true;
		for (node_type* r = this->r; r != this; r = r->r) {
			if (!r->col.is_primary) continue;
			all_covered = false;
			break;
		}
		if (all_covered) return true;

		/**
		* Failure: column is empty but needs to be covered, no chance.
		*/
		for (node_type* r = this->r; r != this; r = r->r) {
			if (r->size > 0 || !r->col.is_primary) continue;
			return false;
		}

		/**
		* Use the shortest column to choose the next row (search heuristic).
		*/
		node_type* header = 0;
		uint32 min_size = 0xFFFFFFFFLL;
		for (node_type* j = this->r; j != this; j = j->r) {
			if (!j->col.is_primary) continue;
			if (j->size >= min_size) continue;
			header = j;
			min_size = header->size;
		}

		/**
		* Cover this column and all others that can be reached.
		* This removes all other associated rows because they
		* can no longer be reached from root header.
		*/
		vector<column_type> these_cols;
		these_cols.push_back(header->col);
		this->cover_column(header);
		for (node_type* r = header->d; r != header; r = r->d) {

			for (node_type* j = r->r; j != r; j = j->r) {
				these_cols.push_back(j->c->col);
				this->cover_column(j->c);
			}

			solution.push_back(these_cols);
			if (this->search(solution)) return true;
			solution.pop_back();

			for (node_type* j = r->l; j != r; j = j->l) {
				this->uncover_column(j->c);
				these_cols.pop_back();
			}
		}
		this->uncover_column(header);
		return false;
	}

	vector<node_type*> nodes;
};


int main(void) {
	ios::sync_with_stdio(false);

	/**
	* Input size of row, column names, then 0/1 matrix (row-by-row).
	*
7
1 2 3 4 5 6 7
0 0 1 0 1 1 0
1 0 0 1 0 0 1
0 1 1 0 0 1 0
1 0 0 1 0 0 0
0 1 0 0 0 0 1
0 0 0 1 1 0 1
-1

	*
	* What is one set of rows that contains exactly one 1 in each column?
	*/
	cout << "Enter number of columns: ";
	uint32 num_cols = 0; cin >> num_cols;

	cout << "Enter column names:" << endl;
	vector<column_type> cols(num_cols);
	for (uint32 col_ix = 0; col_ix < num_cols; col_ix++) {
		string descrip; cin >> descrip;
		cols[col_ix] = column_type(col_ix, descrip);
	}

	cout << "Enter matrix rows (finish with -1):" << endl;
	vector< vector<uint32> > grid_cols(num_cols);
	uint32 c_ix = 0;
	for (;;) {
		int32 cell = 0; cin >> cell;
		if (cell < 0) break;
		grid_cols[c_ix % num_cols].push_back(cell);
		c_ix++;
	}

	root_type root(cols, grid_cols);
	vector< vector<column_type> > solution;
	root.search(solution);

	/**
	* Solution is a a list of rows, each containing (distinct) columns.
	*/
	if (solution.empty()) {
		cout << "No solution" << endl;
	}
	else {
		cout << "A solution uses the rows that correspond to these column sets:" << endl;
		for (uint32 s_ix = 0; s_ix < solution.size(); s_ix++) {
			cout << "{";
			for (vector<column_type>::iterator s_it = solution[s_ix].begin(); s_it != solution[s_ix].end(); ++s_it) {
				cout << s_it->descrip << ' ';
			}
			cout << "}" << endl;
		}
	}

	return 0;
}

