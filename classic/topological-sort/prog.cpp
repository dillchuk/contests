#include <iostream>
#include <vector>
#include <list>
#include <algorithm>

using namespace std;
typedef unsigned long uint32;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* TOPOSORT, Topological sort, directed graph, dependencies, adjacency lists, O(|V| + |E|)
* @see https://en.wikipedia.org/wiki/Topological_sorting
*/

struct node_t {
	node_t(): id(0), incoming(0) {}

	void direct_to(node_t* o) {
		outgoing.push_back(o);
		o->incoming++;
	}

	uint32 id;

	/**
	* Incoming: need degree only
	* Outgoing: need list for search
	*/
	uint32 incoming;
	vector<node_t*> outgoing;
};

int main(void) {

	/**
	* Example input:
8
1 4
1 2
4 2
4 3
3 2
5 2
3 5
8 2
8 6
0
	*/

	/**
	* Nodes [1, ..., num_nodes]
	*/
	cout << "Enter number of jobs.  (These will be indexed from 1 to your entered value.): ";
	uint32 num_nodes = 0;
	cin >> num_nodes;
	vector<node_t> nodes(num_nodes + 1);
	for (uint32 i = 1; i < nodes.size(); i++) {
		nodes[i].id = i;
	}

	cout << "Enter before/after dependencies separated by space e.g. 4 8 (finish with 0)" << endl;
	for (uint32 i = 0;; i++) {
		uint32 id_from = 0, id_to = 0;
		cin >> id_from;
		if (id_from == 0) {
			break;
		}
		cin >> id_to;

		if (max(id_from, id_to) > num_nodes) {
			cout << "Skipping, dependency ID must be within [1, number of jobs]" << endl;
			continue;
		}
		if (id_from == id_to) {
			cout << "Skipping, job is dependency of itself" << endl;
			continue;
		}
		node_t *node_from = &nodes[id_from];
		node_t *node_to = &nodes[id_to];
		node_from->direct_to(node_to);
	}

	/**
	* Start with left-most (incoming-free) nodes.
	*/
	vector<node_t*> topolog;
	list<node_t*> working;
	for (uint32 i = 1; i < nodes.size(); i++) {
		if (nodes[i].incoming) continue;
		working.push_back(&nodes[i]);
	}

	/**
	* Search for nodes with no more prerequisites/incoming;
	* they are added to topological sort result.
	*/
	while (!working.empty()) {
		node_t* from = working.front();
		working.pop_front();
		topolog.push_back(from);
		for (uint32 i = 0; i < from->outgoing.size(); i++) {
			node_t* to = from->outgoing[i];
			to->incoming--;
			if (to->incoming > 0) continue;
			working.push_back(to);
		}
	}

	/**
	* Incoming edge remains?  This means a node has an unmet prerequisite;
	* graph has a cycle thus topological sort is impossible.
	*/
	bool has_cycle = false;
	for (uint32 i = 1; i < nodes.size(); i++) {
		if (nodes[i].incoming == 0) continue;
		has_cycle = true;
		break;
	}
	if (has_cycle) {
		cout << "Dependency loop, can't sort topologically." << endl;
	}
	else {
		cout << "Topological sort:" << endl;
		for (uint32 i = 0; i < topolog.size(); i++) {
			cout << topolog[i]->id << ' ';
		}
		cout << endl;
	}

	return 0;
}
