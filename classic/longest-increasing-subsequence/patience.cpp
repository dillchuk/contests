#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>

using namespace std;
typedef unsigned long uint32;
typedef long int32;

/**
* Implemented by: Derek Illchuk dillchuk@gmail.com
*
* Longest increasing subsequence, O(nlgn), patience sort.
*
* @see http://www.cs.princeton.edu/courses/archive/spring13/cos423/lectures/LongestIncreasingSubsequence.pdf
* @see https://en.wikipedia.org/wiki/Patience_sorting
*/

vector<int32> longest_increasing_sub(const vector<int32>& I) {

	/**
	* List of stacks; stack-contents descending, tops-of-stacks ascending.
	*/
	vector< vector<uint32> > stacks;

	/**
	* LIS backlinks from input index to some previous (or I.size() for none).
	*/
	vector<uint32> prevs(I.size(), I.size());

	for (uint32 i = 0; i < I.size(); i++) {
		uint32 begin = 0, end = stacks.size();
		uint32 go = end;
		while (begin < end) {
			uint32 mid = (begin + end) / 2;
			if (I[stacks[mid].back()] >= I[i]) {
				go = end = mid;
			}
			else {
				begin = mid + 1;
			}
		}

		if (go == stacks.size()) {
			stacks.resize(stacks.size() + 1);
		}
		stacks[go].push_back(i);
		if (go > 0) {
			prevs[i] = stacks[go - 1].back();
		}
	}

	vector<int32> LIS(stacks.size());
	for (uint32 i = stacks.back().back(), r = LIS.size() - 1; i < I.size(); i = prevs[i], r--) {
		LIS[r] = I[i];
	}
	return LIS;
}

int main(void) {
	srand(time(0));
	uint32 n = 20000000;

	vector<int32> I(n);
	for (uint32 i = 0; i < n; i++) {
		I[i] = rand() % n;
	}
	vector<int32> LIS = longest_increasing_sub(I);
	for (uint32 i = 0; i < LIS.size(); i++) {
		cout << LIS[i] << endl;
	}

	return 0;
}
