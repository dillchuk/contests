#include <iostream>
#include <vector>
#include <algorithm>
#include <time.h>
#include <limits.h>

using namespace std;
typedef unsigned long uint32;
typedef long int32;

/**
* Implemented by: Derek Illchuk dillchuk@gmail.com
*
* Longest increasing subsequence, O(nlgn).
*
* @see https://en.wikipedia.org/wiki/Longest_increasing_subsequence
*/

vector<int32> longest_increasing_sub(vector<int32>& I) {
	vector<int32> prev(I.size());

	/**
	* M[j] = i means longest increasing sub length j ends on i
	*/
	vector<int32> M(I.size() + 1, LONG_MIN);

	uint32 longest = 0;
	for (uint32 i = 0; i < I.size(); i++) {
		uint32 begin = 1, end = longest + 1;

		while (begin < end) {
			uint32 mid = (begin + end) / 2;
			if (I[M[mid]] < I[i]) {
				begin = mid + 1;
				continue;
			}
			end = mid;
		}

		uint32 curr_longest = begin;
		prev[i] = M[curr_longest - 1];
		M[curr_longest] = i;
		longest = max(longest, curr_longest);
	}

	vector<int32> result(longest);
	int32 ix = M[longest];
	for (int32 s_ix = longest - 1; s_ix >= 0; s_ix--) {
		result[s_ix] = I[ix];
		ix = prev[ix];
	}
	return result;
}

int main(void) {
	srand(time(0));
	uint32 n = 20000000;

	vector<int32> I(n);
	for (uint32 i = 0; i < n; i++) {
		I[i] = rand();
	}
	cout << longest_increasing_sub(I).size() << endl;

	return 0;
}
