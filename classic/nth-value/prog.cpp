#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Implemented by: Derek Illchuk dillchuk@gmail.com
*
* Find nth largest value of unsorted list in O(n), Quick Select algorithm.
*/

#define MAX_N 50000000LL

/**
* @see https://en.wikipedia.org/wiki/Quicksort partition(A, lo, hi)
*/
uint32 partition(vector<uint32>& L, uint32 begin, uint32 end) {
	uint32 pivot_ix = (begin + end) / 2; // quick and effective
	uint32 pivot_val = L[pivot_ix];
	swap(L[pivot_ix], L[end - 1]);

	uint32 store_ix = begin;
	for (uint32 i = begin; i < end; i++) {
		if (L[i] >= pivot_val) continue;
		swap(L[i], L[store_ix]);
		store_ix++;
	}
	swap(L[store_ix], L[end - 1]);
	return store_ix;
}

/**
* @see https://en.wikipedia.org/wiki/Quickselect
*
* Given an unsorted list, returns SORTED-LIST[nth] in O(n).
*/
uint32 nth_value(uint32 nth, vector<uint32>& L) {
	if (nth >= L.size()) {
		cout << "Invalid `nth`" << endl;
		exit(EXIT_FAILURE);
	}

	uint32 begin = 0, end = L.size();
	uint32 part = end;
	while (part != nth) {
		part = partition(L, begin, end);
		if (part < nth) {
			begin = part + 1;
		}
		else if (part > nth) {
			end = part;
		}
	}
	return L[part];
}

int main(void) {
	srand(time(0));

	/**
	* Set up values 0 to MAX_N-1, so (nth_value == n).  Then shuffle!
	*/
	vector<uint32> L(MAX_N);
	for (uint32 i = 0; i < L.size(); i++) {
		L[i] = i;
	}
	random_shuffle(L.begin(), L.end());
	uint32 nth_ix = rand() % L.size();
	cout << "Nth value " << nth_ix << " = " << nth_value(nth_ix, L) << " (should be the same in this demonstration)" << endl;
	
	return 0;
}
