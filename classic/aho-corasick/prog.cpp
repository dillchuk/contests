#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <list>
#include <string>
#include <set>
#include <time.h>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* Aho-Corasick dictionary
*
* Research: http://www.cs.uku.fi/~kilpelai/BSA05/lectures/slides04.pdf
*/

struct node_type {

	/**
	* Root constructor.
	*/
	node_type(string alphabet): alphabet(alphabet), root(this), fail(0) {
		this->ix = this->nodes.size();
		this->nodes.push_back(this);

		for (string::iterator a_it = this->alphabet.begin();
			a_it != this->alphabet.end(); ++a_it) {
			this->links[*a_it] = this;
		}
	}

	/**
	* Child constructor.
	*/
	node_type(node_type* parent): root(parent->root), fail(0) {
		this->ix = this->root->nodes.size();
		this->root->nodes.push_back(this);
	}

	~node_type() {
		if (!this->is_root()) return;
		for (uint32 i = 1; i < this->nodes.size(); i++) {
			delete this->nodes[i];
		}
		this->nodes.clear();
	}

	node_type* follow(char c) {
		node_type* curr = this;
		while (curr->go(c) == 0) {
			curr = curr->fail;
		}
		return curr->go(c);
	}

	node_type* go(char c) {
		return this->links[c];
	}

	node_type* set_go(char c) {
		node_type* go = this->links[c];
		if (go && go->is_root()) {
			go = 0;
		}
		if (go == 0) {
			this->links[c] = new node_type(this);
		}
		return this->links[c];
	}

	void set_word_end(const string& word) {
		this->words.insert(word);
	}

	bool is_root() const {
		return (this->root->nodes[0] == this);
	}

	uint32 ix;
	map<char, node_type*> links;

	set<string> words;

	node_type* fail;

	node_type* root;

	/**
	* Root only.
	*/
	string alphabet;
	vector<node_type*> nodes;
};

void phase1_add_words(node_type* root, const vector<string>& words) {
	if (!root->is_root()) {
		cout << "Called `phase1` on non-root";
		exit(1);
	}

	for (vector<string>::const_iterator w_it = words.begin(); w_it != words.end(); ++w_it) {
		node_type* curr = root;
		for (string::const_iterator c_it = w_it->begin(); c_it != w_it->end(); ++c_it) {
			curr = curr->set_go(*c_it);
		}
		curr->set_word_end(*w_it);
	}
}

void phase2_automatize(node_type* root) {
	if (!root->is_root()) {
		cout << "Called `phase2` on non-root";
		exit(1);
	}
	list<node_type*> queue;
	for (string::const_iterator a_it = root->alphabet.begin(); a_it != root->alphabet.end(); ++a_it) {
		node_type* q = root->go(*a_it);
		if (!q->is_root()) {
			q->fail = q->root;
			queue.push_back(q);
		}
	}
	while (!queue.empty()) {
		node_type* r = queue.front(); queue.pop_front();
		for (string::const_iterator a_it = root->alphabet.begin(); a_it != root->alphabet.end(); ++a_it) {
			node_type* u = r->go(*a_it);
			if (u != 0) {
				queue.push_back(u);
				u->fail = r->fail->follow(*a_it);
				copy(u->fail->words.begin(), u->fail->words.end(), inserter(u->words, u->words.begin()));
			}
		}
	}
}

/**
* Find finishing state of the given word.  Then, whenever search
* encounters this node, this word has just been found.
*
* N.B. Use during a 2D search:
* 1. Create dictionary containing each row of 2D needle,
*    then generate list containing final states for each (in order)
* 2. Pass row by row in haystack, replacing character with current dictionary state
* 3. Pass col by col from 2, searching for list (from 1) using KMP.
*/
node_type* find_word(node_type* root, const string& word) {
	node_type* curr = root;
	for (string::const_iterator c_it = word.begin(); c_it != word.end(); ++c_it) {
		curr = curr->go(*c_it);
		if (curr == 0) return 0;
	}
	return curr;
}

int main(void) {
	ios::sync_with_stdio(false);

	/**
	* Dictionary: create.
	*/
	vector<string> words;
	words.push_back("ban");
	words.push_back("band");
	words.push_back("and");
	words.push_back("nd");
	words.push_back("dad");
	words.push_back("dana");
	words.push_back("bbbbbbb");
	string alphabet = "abdmn";
	node_type* root = new node_type(alphabet);
	phase1_add_words(root, words);
	phase2_automatize(root);

	/**
	* Dictionary: query.
	*/
	string haystack = "bandanaddadd";
	node_type* curr = root;
	for (string::iterator h_it = haystack.begin(); h_it != haystack.end(); ++h_it) {
		curr = curr->follow(*h_it);
		for (set<string>::iterator w_it = curr->words.begin(); w_it != curr->words.end(); ++w_it) {
			cout << *w_it << endl;
		}
	}

	delete root; root = 0;
	return 0;
}
