#include <iostream>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

using namespace std;
typedef unsigned long uint32;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* In a list where element X occurs more than half *guaranteed*, find it in O(n)
* with additional memory lg(max value) + lg(n).
*/

#define MAJOR_VALUE 12345
#define N SHRT_MAX
#define VAL_END SHRT_MAX

int main(void) {

	/**
	* Initialize, ensuring > 50% majority.
	*/
	srand(time(0));
	vector<short int> L(N, MAJOR_VALUE);
	uint32 minority_n = N / 2 - 1;
	for (uint32 i = 0; i < minority_n; i++) {
		L[i] = rand() % VAL_END;
	}
	random_shuffle(L.begin(), L.end());

	/**
	* Additional memory: lg(n):         count
	*                    lg(max value): element value
	*
	* Memory regarding input initialized/streaming are here not under scrutiny.
	*/
	short int count = 1;
	short int major = L[0];

	/**
	* Each element value casts a vote:
	* - strengthen majority if matching
	* - weaken majority if not
	* - or even switch majority
	*
	* Note: with our memory constraints we can't guarantee
	* a bona-fide majority in a single sweep.  However,
	* this majority is declared in problem description.
	*/
	for (uint32 i = 1; i < L.size(); i++) {
		if (L[i] == major) {
			count++; continue;
		}
		if (count > 1) {
			count--; continue;
		}
		major = L[i];
	}
	cout << major << " result should match known" << endl << MAJOR_VALUE << endl;;

	return 0;
}
