#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <map>
#include <time.h>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com

* Rectangle intersection, including abutment.
*/

#define MAX_N 100000

struct rectangle_type {
	rectangle_type(uint32 id, int32 x1, int32 y1, int32 x2, int32 y2): id(id) {
		this->xl = min(x1, x2);
		this->xh = max(x1, x2);
		this->yl = min(y1, y2);
		this->yh = max(y1, y2);
		if (this->xl == this->xh) throw "rectangle_type() parameter error";
		if (this->yl == this->yh) throw "rectangle_type() parameter error";
	}

	uint32 area() {
		return (this->xh - this->xl) * (this->yh - this->yl);
	}

	/**
	* Coords low/high.
	*/
	int32 xl, xh;
	int32 yl, yh;

	uint32 id;
};

/**
* Sort rectangles initially by `yl`; imperative for working of `stab_y`.
*/
struct rectangle_sort_yl {
	bool operator()(const rectangle_type* a, const rectangle_type* b) {
		if (a->yl < b->yl) return true;
		if (a->yl > b->yl) return false;
		return (a < b);
	}
};

void report_pair(rectangle_type* a, rectangle_type* b) {

	/**
	* `a` and `b` intersect!  N.B. You'll definitely get duplicates.
	*/
}

void stab_y(const vector<rectangle_type*>& a, const vector<rectangle_type*>& b) {

	/**
	* Ends of arrays are marked with a null pointer.
	*/
	uint32 i = 0, j = 0;
	while (a[i] != 0 && b[j] != 0) {

		/**
		* Perfect match on buttom of abutment, so not sure which to increment.
		* Here, make sure you're calling stab_y with a & b reversed too.
		*/
		if (a[i]->yl == b[j]->yl) {
			if (a[i] != b[j]) {
				report_pair(a[i], b[j]);
			}
			j++;
		}

		else if (a[i]->yl < b[j]->yl) {
			uint32 k = j;
			while (b[k] != 0 && b[k]->yl <= a[i]->yh) {
				report_pair(a[i], b[k]);
				k++;
			}
			i++;
		}
		else {
			uint32 k = i;
			while (a[k] != 0 && a[k]->yl <= b[j]->yh) {
				report_pair(b[j], a[k]);
				k++;
			}
			j++;
		}
	}
}

#define ONLY_IN_1ST(r, x_mid) (r->xh <= x_mid)
#define ONLY_IN_2ND(r, x_mid) (r->xl >= x_mid)
#define ABUTS_FRONT(r, x_mid) (r->xh == x_mid)
#define ABUTS_BACK(r, x_mid) (r->xl == x_mid)
#define IN_1ST_OVER_2ND(r, x_mid, x_max) (r->xl <= x_mid && r->xh >= x_max)
#define IN_2ND_OVER_1ST(r, x_mid, x_min) (r->xh >= x_mid && r->xl <= x_min)
#define IN_BOTH(r, x_mid) (r->xl <= x_mid && r->xh >= x_mid)

/**
* Optimization: allocate these temporary arrays once; track size manually.
*/
vector<rectangle_type*>
s1_only(MAX_N + 1), s2_only(MAX_N + 1),
s1_jump_2(MAX_N + 1), s2_jump_1(MAX_N + 1),
s1_abuts(MAX_N + 1), s2_abuts(MAX_N + 1);
uint32 s1o = 0, s2o = 0, s1j2 = 0, s2j1 = 0, s1a = 0, s2a = 0;

void detect(vector<rectangle_type*>& rs, const vector<int32>& xs, uint32 begin = 0, uint32 end = -1) {
	end = min(end, static_cast<uint32>(xs.size()));

	int32 size = end - begin;
	if (size == 0) return;

	uint32 mid = (begin + end) / 2;
	int32 x_min = xs[begin], x_mid = xs[mid], x_max = xs[end - 1];

	/**
	* Check for abutment.  It's imperative that this is checked for
	* each entry in xs.
	*/
	if (size == 1) {
		s1a = s2a = 0;
		for (uint32 i = 0; i < rs.size(); i++) {
			rectangle_type* rect = rs[i];
			if (ABUTS_FRONT(rect, x_mid)) {
				s1_abuts[s1a++] = rect;
			}
			else if (ABUTS_BACK(rect, x_mid)) {
				s2_abuts[s2a++] = rect;
			}
		}
		s1_abuts[s1a] = s2_abuts[s2a] = 0; // sentinel/end
		stab_y(s1_abuts, s1_abuts); // stacked (in 1st)
		stab_y(s2_abuts, s2_abuts); // stacked (in 2nd)
		stab_y(s1_abuts, s2_abuts); // side-by-side, or kitty corner
		stab_y(s2_abuts, s1_abuts); // ditto (inputs reversed, necessary)
		return;
	}

	s1o = s2o = s1j2 = s2j1 = 0;
	for (uint32 i = 0; i < rs.size(); i++) {
		rectangle_type* rect = rs[i];

		if (ONLY_IN_1ST(rect, x_mid)) {
			s1_only[s1o++] = rect;
		}
		else if (ONLY_IN_2ND(rect, x_mid)) {
			s2_only[s2o++] = rect;
		}
		if (IN_1ST_OVER_2ND(rect, x_mid, x_max)) {
			s1_jump_2[s1j2++] = rect;
		}
		if (IN_2ND_OVER_1ST(rect, x_mid, x_min)) {
			s2_jump_1[s2j1++] = rect;
		}
	}
	s1_only[s1o] = s2_only[s2o] = s1_jump_2[s1j2] = s2_jump_1[s2j1] = 0; // sentinel/end
	stab_y(s1_jump_2, s2_only);
	stab_y(s2_jump_1, s1_only);
	stab_y(s1_jump_2, s2_jump_1);

	vector<rectangle_type*> r1, r2;
	for (uint32 i = 0; i < rs.size(); i++) {
		rectangle_type* rect = rs[i];

		if (IN_BOTH(rect, x_mid) || ONLY_IN_1ST(rect, x_mid) || IN_1ST_OVER_2ND(rect, x_mid, x_max)) {
			r1.push_back(rect);
		}
		if (IN_BOTH(rect, x_mid) || ONLY_IN_2ND(rect, x_mid) || IN_2ND_OVER_1ST(rect, x_mid, x_min)) {
			r2.push_back(rect);
		}
	}

	/**
	* [begin, mid + 1) is [begin, mid], so use if possible.
	* Just don't repeat the current [begin, end).
	*/
	if (mid + 1 < end) {
		detect(r1, xs, begin, mid + 1);
	}
	else {
		detect(r1, xs, begin, mid);
	}
	detect(r2, xs, mid, end);
}


int main(void) {
	ios::sync_with_stdio(false);

	/**
	* Input:
	* num_rectangles
	* x_min y_min width(in x) height(in y) (for each rectangle)
	*/
	uint32 n = 0; cin >> n;

	vector<rectangle_type> rectangles;
	for (uint32 i = 1; i <= n; i++) {
		int32 x = 0, y = 0, xw = 0, yh = 0;
		cin >> x >> y >> xw >> yh;
		rectangles.push_back(rectangle_type(i, x, y, x + xw, y + yh));
	}

	vector<int32> xs;
	vector<rectangle_type*> rs;
	for (uint32 i = 0; i < rectangles.size(); i++) {
		xs.push_back(rectangles[i].xl);
		xs.push_back(rectangles[i].xh);
		rs.push_back(&rectangles[i]);
	}
	sort(rs.begin(), rs.end(), rectangle_sort_yl());
	sort(xs.begin(), xs.end());
	xs.erase(unique(xs.begin(), xs.end()), xs.end());
	detect(rs, xs);

	return 0;
}