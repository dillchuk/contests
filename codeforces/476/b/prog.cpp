#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <list>
#include <string>
#include <numeric>
#include <iomanip>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

uint32 known_position(string moves) {
	int32 pos = 0;
	for (string::iterator m_it = moves.begin(); m_it != moves.end(); ++m_it) {
		switch (*m_it) {
			case '+':
				pos++; break;
			case '-':
				pos--; break;
		}
	}
	return pos;
}

uint32 count_unrecognized(string moves) {
	uint32 count = 0;
	for (string::iterator m_it = moves.begin(); m_it != moves.end(); ++m_it) {
		if (*m_it != '?') continue;
		count++;
	}
	return count;
}

uint32 factorial(uint32 n) {
	uint32 result = 1;
	for (uint32 i = 2; i <= n; i++) {
		result *= i;
	}
	return result;
}

int main(void) {
	ios::sync_with_stdio(false);

	string actual, performed; cin >> actual >> performed;
	int32 pos_begin = known_position(actual);
	int32 pos_end = known_position(performed);
	uint32 distance = abs(pos_begin - pos_end);

	double p = 0;
	uint32 moves = count_unrecognized(performed);
	if (moves >= distance) {
		uint32 moves_cancel = moves - distance;
		if (moves_cancel % 2 == 0) {
			int32 l = moves_cancel / 2;
			int32 r = distance + l;

			/**
			* e.g.
			* distance = 2
			* moves = 6
			* How many orderings of RRRRLL?  That's 6 choose 4 (or 6 choose 2).
			* "Six places where R can go, choose 4"
			*/
			double paths_correct = factorial(l + r) / (factorial(l) * factorial(r));
			double paths_possible = pow(2, moves);
			p = paths_correct / paths_possible;
		}
	}
	cout << fixed << setprecision(12) << p << endl;

	return 0;
}
