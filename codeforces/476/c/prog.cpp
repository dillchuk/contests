#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <set>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

#define MODULO 1000000007

int main(void) {
	ios::sync_with_stdio(false);

	/**
	* Result is a sum of an (a)x(b-1) grid.
	*
	* e.g. a = 2 (rows), b = 4 (cols)
	*
	* %b == 1   %b == 2   %b == 3   (%b == 0 avoided)
	* 4x1x1 +1  4x2x1 +2  4x3x1 +3
	* 4x1x2 +1  4x2x2 +2  4x3x2 +3
	*
	*
	*/
	uint64 a = 0, b = 0; cin >> a >> b;

	/**
	* Pull lone additions out of the grid above a(1 + 2 + ... + b-1)
	*/
	uint64 bseries = ((b * (b - 1)) / 2) % MODULO;
	uint64 result = bseries * a;

	/**
	* Rows: factor out (1 + 2 + ... + b-1)
	*/
	for (uint64 row_id = 1; row_id <= a; row_id++) {
		result += b * row_id % MODULO * bseries;
		result %= MODULO;
	}
	result %= MODULO;
	cout << result << endl;

	return 0;
}
