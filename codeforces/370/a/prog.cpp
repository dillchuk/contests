#include <iostream>
#include <cmath>
#include <algorithm>

/**
* How many moves rook, bishop, king from/to location; runs one piece at a time (easy).
*/

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

typedef pair<int32, int32> ipr;

ipr calc_delta(ipr start_pos, ipr target_pos) {
	ipr delta;
	delta.first = abs(start_pos.first - target_pos.first);
	delta.second = abs(start_pos.second - target_pos.second);
	return delta;
}

int32 min_king_moves(ipr start_pos, ipr target_pos) {
	ipr delta = calc_delta(start_pos, target_pos);

	/**
	 * First, diagonals; second, straight.
	 */
	uint32 min_move = min(delta.first, delta.second);
	delta.first -= min_move;
	delta.second -= min_move;
	return min_move + delta.first + delta.second;
}

int32 min_bishop_moves(ipr start_pos, ipr target_pos) {

	/**
	 * Bishop can't go to half the board.
	 */
	bool start_diag = (start_pos.first + start_pos.second) % 2;
	bool target_diag = (target_pos.first + target_pos.second) % 2;
	if (start_diag != target_diag) {
		return 0;
	}

	ipr delta = calc_delta(start_pos, target_pos);
	return (delta.first == delta.second) ? 1 : 2;
}

int32 min_rook_moves(ipr start_pos, ipr target_pos) {
	ipr delta = calc_delta(start_pos, target_pos);
	return (delta.first == 0 || delta.second == 0) ? 1 : 2;
}

int main(void) {
	ios::sync_with_stdio(false);

	ipr start_pos; cin >> start_pos.first >> start_pos.second;
	ipr target_pos; cin >> target_pos.first >> target_pos.second;

	int32 min_r = min_rook_moves(start_pos, target_pos);
	int32 min_b = min_bishop_moves(start_pos, target_pos);
	int32 min_k = min_king_moves(start_pos, target_pos);
	cout << min_r << ' ' << min_b << ' ' << min_k << endl;

	return 0;
}
