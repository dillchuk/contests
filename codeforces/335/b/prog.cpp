#include <iostream>
#include <vector>
#include <string>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

#define TARGET_EVEN 100

int main(void) {
	ios::sync_with_stdio(false);

	string good_enough;
	string in; cin >> in;

	/**
	* Keep only two latest rows; cycle between.
	*/
	vector< vector<string> > t;
	t.push_back(vector<string>(in.size()));
	t.push_back(vector<string>(in.size()));
	for (uint32 j = 0; j < in.size() && good_enough.empty(); j++) {
		uint32 row_ix = j % 2;
		uint32 prev_row_ix = row_ix ^ 1;
		t[row_ix] = vector<string>(in.size()); // zeroed

		t[row_ix][j] = in[j];
		for (int32 i = j - 1; i >= 0; i--) {
			string cand_match = "";
			if (in[i] == in[j]) {
				cand_match = in[j] + t[prev_row_ix][i + 1] + in[j];

				/**
				* Found a good-enough string, so short-circuit.
				*/
				if (cand_match.size() >= TARGET_EVEN) {
					good_enough = cand_match;
					break;
				}
			}

			/**
			* It may be safe to just take `cand_match` if set; can you prove it?
			*/
			t[row_ix][i] = cand_match;
			string &cand_prev = t[prev_row_ix][i];
			if (t[row_ix][i].size() < cand_prev.size()) {
				t[row_ix][i] = cand_prev;
			}
			string &cand_curr = t[row_ix][i + 1];
			if (t[row_ix][i].size() < cand_curr.size()) {
				t[row_ix][i] = cand_curr;
			}
		}
	}

	if (!good_enough.empty()) {

		/**
		 * Our result may be one bigger than needed (by one); if so, remove the lone middle.
		 * (We don't need to handle the case where target is odd and good_enough.size() is even.)
		 */
		if (good_enough.size() % 2 == 1) {
			good_enough.erase(good_enough.size() / 2, 1);
		}
		cout << good_enough << endl;
	}
	else {
		string best = (t[0][0].size() > t[1][0].size()) ? t[0][0] : t[1][0]; // a little lazy
		cout << best << endl;
	}

	return 0;
}
