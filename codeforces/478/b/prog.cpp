#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <map>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

typedef map<uint64, uint64> size_counts;

uint64 unique_pairs(uint64 n) {

	/**
	* Avoid overflow.
	*/
	if (n % 2 == 0) {
		return (n / 2) * (n - 1);
	}
	else {
		return ((n - 1) / 2) * n;
	}
}

/**
* Split as evenly as possible.
*/
size_counts split_min(uint64 n, uint64 m) {
	size_counts result;

	uint64 each = n / m;
	uint64 remain = n - each * m;

	result[each] = m - remain;
	result[each + 1] = remain;
	return result;
}

/**
* All groups get one, remainder dumped into single group.
*/
size_counts split_max(uint64 n, uint64 m) {
	size_counts result;

	result[1] = m - 1;
	uint64 remain = n - result[1];
	result[remain] = 1;

	return result;
}

uint64 tally(size_counts counts) {
	uint64 result = 0;
	for (size_counts::iterator c_it = counts.begin(); c_it != counts.end(); ++c_it) {
		result += unique_pairs(c_it->first) * c_it->second;
	}
	return result;
}

int main(void) {
	ios::sync_with_stdio(false);

	uint64 n = 0, m = 0; cin >> n >> m;
	uint64 min_k = tally(split_min(n, m));
	uint64 max_k = tally(split_max(n, m));

	cout << min_k << ' ' << max_k << endl;

	return 0;
}
