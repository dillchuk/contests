#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>

/**
* You have r red, g green and b blue balloons. To decorate a single table
* for the banquet you need exactly three balloons. Three balloons attached
* to some table shouldn't have *all* the same color. What maximum number t
* of tables can be decorated if we know number of balloons of each color?
*/

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

int main(void) {
	ios::sync_with_stdio(false);

	uint64 a, b, c; cin >> a >> b >> c;
	a = min(a, 2 * (b + c));
	b = min(b, 2 * (a + c));
	c = min(c, 2 * (a + b));
	uint64 result = (a + b + c) / 3;
	cout << result << endl;

	return 0;
}
