#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

int main(void) {
	ios::sync_with_stdio(false);

	vector<uint32> state(5);
	for (uint32 i = 0; i < 5; i++) {
		cin >> state[i];
	}

	uint32 sum = 0;
	for (uint32 i = 0; i < 5; i++) {
		sum += state[i];
	}
	if (sum % 5 != 0) {
		cout << -1 << endl;
	}
	else {
		uint32 initial = sum / 5;
		if (initial == 0) {
			cout << -1 << endl;
		}
		else {
			cout << initial << endl;
		}
	}

	return 0;
}
