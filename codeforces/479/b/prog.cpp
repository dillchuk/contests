#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <list>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

struct tower {
	tower(uint32 id, uint32 h) {
		this->id = id;
		this->h = h;
	}
	uint32 id, h;
};

bool operator<(tower a, tower b) {
	if (a.h < b.h) return true;
	if (a.h > b.h) return false;
	return (a.id < b.id);
}

int main(void) {
	ios::sync_with_stdio(false);

	uint32 n = 0, max_moves = 0; cin >> n >> max_moves;
	list<tower> towers;
	for (uint32 id = 1; id <= n; id++) {
		uint32 height = 0; cin >> height;
		towers.push_back(tower(id, height));
	}

	/**
	* Sort initially, then keep sorted manually throughout.
	*/
	vector< pair<uint32, uint32> > moves;
	towers.sort();
	uint32 instability = 0xFFFFFFFF;
	uint32 num_moves = 0;
	for (uint32 move_ix = 0; move_ix < max_moves; move_ix++) {

		/**
		* Can't level down anymore.
		*/
		instability = towers.back().h - towers.front().h;
		if (instability <= 1) break;
		num_moves++;

		/**
		* Make the move.
		*/
		moves.push_back(make_pair(towers.back().id, towers.front().id));
		towers.back().h--;
		towers.front().h++;

		/**
		* I tried performing a swap sort on only the front/back elements,
		* but there was no time improvement with this input.
		*/
		towers.sort();
	}

	instability = towers.back().h - towers.front().h;
	cout << instability << ' ' << num_moves << endl;
	for (uint32 m_ix = 0; m_ix < moves.size(); m_ix++) {
		cout << moves[m_ix].first << ' ' << moves[m_ix].second << endl;
	}

	return 0;
}
