#include <iostream>
#include <vector>
#include <set>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

typedef pair<uint64, uint64> pair64;
pair64 pair_null;

int main(void) {
	ios::sync_with_stdio(false);

	uint64 mark_num = 0; cin >> mark_num;
	uint64 mark_max = 0; cin >> mark_max;
	uint64 x = 0, y = 0; cin >> x >> y;
	if (x > y) {
		swap(x, y);
	}

	set<uint64> ruler;
	for (uint32 m_ix = 0; m_ix < mark_num; m_ix++) {
		uint64 mark = 0; cin >> mark;
		ruler.insert(mark);
	}

	bool found_x = false, found_y = false;

	/**
	* Value given when both solved with a single (new) mark.
	*/
	uint64 solution_shared = 0;

	for (set<uint64>::iterator r_it = ruler.begin(); r_it != ruler.end() && (!found_x || !found_y); ++r_it) {
		uint64 curr = *r_it;

		/**
		* Match exactly.
		*/
		if (ruler.count(curr + x)) {
			found_x = true;
		}
		if (ruler.count(curr + y)) {
			found_y = true;
		}

		/**
		* Solve both: place a new mark between two existing.
		*/
		if (!solution_shared && ruler.count(curr + y + x)) {
			solution_shared = curr + x;
		}

		/**
		* Solve both: place a new mark outside of two existing.
		* Make sure the spot on the ruler actually exists.
		*/
		uint64 to_diff = curr + y - x;
		if (!solution_shared && ruler.count(to_diff)) {
			uint64 candidate_hi = curr + y;
			int64 candidate_lo = to_diff - y;
			uint64 candidate = 0;
			if (candidate_hi <= mark_max) {
				candidate = candidate_hi;
			}
			else if (candidate_lo >= 0) {
				candidate = candidate_lo;
			}
			solution_shared = candidate ? candidate : solution_shared;
		}
	}

	vector<uint64> output;
	if (found_x && found_y) {}
	else if (found_x) {
		output.push_back(y);
	}
	else if (found_y) {
		output.push_back(x);
	}
	else if (solution_shared) {
		output.push_back(solution_shared);
	}
	else {
		output.push_back(x);
		output.push_back(y);
	}
	cout << output.size() << endl;
	for (uint32 i = 0; i < output.size(); i++) {
		cout << output[i] << ' ';
	}
	cout << endl;

	return 0;
}
