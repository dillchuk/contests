#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

int main(void) {
	ios::sync_with_stdio(false);

	uint32 a = 0, b = 0, c = 0; cin >> a >> b >> c;

	/**
	* Just try them all.
	*/
	uint32 best = 0;
	best = max(best, a + b + c);
	best = max(best, a * b * c);
	best = max(best, a + b * c);
	best = max(best, (a + b) * c);
	best = max(best, a * b + c);
	best = max(best, a * (b + c));
	cout << best << endl;

	return 0;
}
