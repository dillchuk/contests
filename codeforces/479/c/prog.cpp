#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <set>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

struct subject_type {
	subject_type(): a(0), b(0) {}
	subject_type(uint64 a, uint64 b): a(a), b(b) {}
	uint64 a, b;
};

bool operator<(subject_type l, subject_type r) {
	if (l.a < r.a) return true;
	if (l.a > r.a) return false;
	return (l.b < r.b);
}

int main(void) {
	ios::sync_with_stdio(false);

	uint32 n = 0; cin >> n;
	set<subject_type> subs;
	for (uint32 i = 0; i < n; i++) {
		uint64 a = 0, b = 0; cin >> a >> b;
		subs.insert(subject_type(a, b));
	}

	uint64 prev = 0;
	for (set<subject_type>::iterator s_it = subs.begin(); s_it != subs.end(); ++s_it) {

		/**
		* Use alternate (b) if possible.
		*/
		if (s_it->b >= prev) {
			prev = s_it->b;
			continue;
		}

		/**
		* Use actual (a).
		*/
		prev = s_it->a;
	}

	cout << prev << endl;

	return 0;
}
