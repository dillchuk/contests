#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

int main(void) {
	ios::sync_with_stdio(false);

	/**
	* atoms 0, 1, 2
	* bonds_from 0->1, 1->2, 2->0
	*/
	vector<uint32> atoms(3), bonds_from(3);
	cin >> atoms[0] >> atoms[1] >> atoms[2];

	bool failed = false;
	for (;;) {

		/**
		* Can we form a bond?
		*/
		uint32 available = 0;
		for (uint32 i = 0; i < atoms.size(); i++) {
			if (!atoms[i]) continue;
			available++;
		}
		if (available <= 1) {
			failed = (available == 1);
			break;
		}

		/**
		* Bond taken between two largest.
		*/
		uint32 min_ix = min_element(atoms.begin(), atoms.end()) - atoms.begin();
		atoms[(min_ix + 1) % 3]--;
		atoms[(min_ix + 2) % 3]--;
		bonds_from[(min_ix + 1) % 3]++;
	}

	if (failed) {
		cout << "Impossible";
	}
	else {
		for (uint32 i = 0; i < atoms.size(); i++) {
			cout << bonds_from[i] << " ";
		}
	}
	cout << endl;

	return 0;
}
