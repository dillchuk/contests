#include <iostream>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <algorithm>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

#define INF64 0x8000000000000000LL
#define INF64_SQRT 3037000000LL

#define MAX_N 1000000000000000000LL // from problem description

int main( void )
{
	uint64 target = 0;
	cin >> target;

	vector<uint64> results;

	for ( uint64 series_ix = 0;; series_ix++ ) {
		uint64 series_first = (1LL << series_ix);
		if ( series_first > MAX_N ) break;

		/**
		 * Watch for overflow:
		 * - p2_input will be squared
		 * - p2_input will be multiplied by p1_to_p2_ratio
		 */
		uint64 p1_to_p2_ratio = series_first - 1;
		uint64 max_p2_input = INF64_SQRT;
		if ( p1_to_p2_ratio > 0 ) {
			max_p2_input = min(max_p2_input, INF64 / p1_to_p2_ratio);
		}

		/**
		 * Each series gives results strictly increasing, so on each do a binary search.
		 *
		 * Index is mapped from ix=>1, 2, 3... to p2_input=>1, 3, 5...
		 */
		uint64 lo = 1;
		uint64 hi = (max_p2_input + 1) / 2;
		while ( lo <= hi ) {
			uint64 mid = lo + (( hi - lo ) / 2);
			uint64 p2_input = (mid * 2) - 1;

			uint64 p2 = p2_input * ((p2_input - 1) / 2); // or `* (p2_input >> 1LL);`

			uint64 p1 = p1_to_p2_ratio * p2_input;
			uint64 result = p1 + p2;
			if ( result == target ) {
				results.push_back(p2_input << series_ix);
				break;
			}
			else if ( result < target ) {
				lo = mid + 1;
			}
			else {
				hi = mid - 1;
			}
		}
	}

	sort(results.begin(), results.end());
	if ( results.empty() ) {
		cout << -1;
	}
	else {
		for ( uint32 i = 0; i < results.size(); i++ ) {
			cout << results[i] << endl;
		}
	}

	return 0;
}
