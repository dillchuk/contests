#include <iostream>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* Research: http://sist.sysu.edu.cn/~isslxm/DSA/textbook/Skiena.-.TheAlgorithmDesignManual.pdf p134
*
* Looking for transition in some "infinite" random-access chunk, via binary search.
*/

#define TRANSITION 2223372036854775806LL
#define MAX_POW_2 0x8000000000000000LL

/**
* Transition occurs at a particular index.
* e.g. [0,0,0,1,1,1,....] transition at 3.
*/
struct random_access_transition {
	random_access_transition(uint64 transition): transition(transition) {}
	bool has_transitioned(uint64 i) const {
		return (i >= this->transition);
	}
	uint64 transition;
};

int main(void) {

	/**
	* Find initial [lo, hi] bounds, from the left in power-of-2 increments.
	*
	* `lo` always < transition
	* `hi` always >= transition
	*/
	uint64 lo = 0, hi = 1;
	random_access_transition rat(TRANSITION);
	for (; hi <= MAX_POW_2 && !rat.has_transitioned(hi); lo = hi, hi <<= 1);
	while (lo < hi) {
		uint64 mid = (lo + hi) / 2;
		if (rat.has_transitioned(mid)) {
			hi = mid;
			continue;
		}
		if (lo == mid) break;
		lo = mid;
	}
	cout << hi << " result should match" << endl << TRANSITION << " actual" << endl;

	return 0;
}
