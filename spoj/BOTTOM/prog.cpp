#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <time.h>

using namespace std;
typedef unsigned long uint32;
typedef long int32;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* BOTTOM, Strongly connected components, Kosaraju's algorithm
* @see https://en.wikipedia.org/wiki/Kosaraju%27s_algorithm
*/

struct quick_union {

	quick_union(): sets(1), size(1) {}

	/**
	* Insert linked pair or a lone (unlinked) item.
	* Keep IDs as low as you can.
	*/
	void insert(uint32 a, uint32 b = 0) {
		if (b == 0) b = a;

		if (a == 0) {
			cout << "Items must have id > 0" << endl;
			exit(1);
		}

		/**
		* Grow.
		*/
		uint32 max_id = max(max(a, b), static_cast<uint32>(this->sets.size() - 1));
		if (max_id >= this->sets.size()) {
			this->sets.resize(max_id + 1);
			this->size.resize(max_id + 1);
		}
		if (this->sets[a] == 0) this->sets[a] = a;
		if (this->sets[b] == 0) this->sets[b] = b;
		this->size[a] = max(this->size[a], static_cast<uint32>(1));
		this->size[b] = max(this->size[b], static_cast<uint32>(1));

		a = this->find(a);
		b = this->find(b);
		if (a == b) return;

		/**
		* Weighted union; keep tree balanced.
		*/
		if (this->size[a] < this->size[b]) {
			this->sets[a] = b;
			this->size[b] += this->size[a];
		}
		else {
			this->sets[b] = a;
			this->size[a] += this->size[b];
		}
	}

	uint32 find(uint32 id) {
		if (id >= this->sets.size()) return 0;
		for (; id != this->sets[id]; id = this->sets[id]) {
			this->sets[id] = this->sets[this->sets[id]]; // collapse tree a bit
		}
		return id;
	}

	vector<uint32> sets, size;
};

struct kosaraju_node_t {
	kosaraju_node_t(): ix(0), id(0), phase1_touched(false), phase2_touched(false) {}
	uint32 ix, id;

	/**
	* Enables the following to run in O(1):
	*  "Is node in stack?"
	*  "Remove from stack"
	*/
	bool phase1_touched, phase2_touched;
};
typedef kosaraju_node_t node_t;

struct kosaraju_graph_t {
	kosaraju_graph_t(uint32 size): nodes(size), edge_out(size), edge_in(size) {
		for (int32 i = size - 1; i >= 0; i--) {
			this->nodes[i].ix = i;			 // ix [0, size - 1]
			this->nodes[i].id = i + 1;		 // id [1, size]
			this->components.insert(this->nodes[i].id);
		}
	}

	void direct(uint32 ix_from, uint32 ix_to) {
		this->edge_out[ix_from].push_back(&this->nodes[ix_to]);
		this->edge_in[ix_to].push_back(&this->nodes[ix_from]);
	}

	/**
	* Phase 1 (linear time):
	* - grab an unseen node and perform depth-first search.
	* - add node to stack after children processed.
	*/
	void phase1() {
		uint32 v = 0;
		while (this->stack.size() < this->nodes.size()) {
			node_t* node = 0;
			for (; v < this->nodes.size(); v++) {
				if (this->nodes[v].phase1_touched) continue;
				node = &this->nodes[v];
				break;
			}
			phase1_dfs(node);
		}
	}
	void phase1_dfs(node_t* curr) {
		curr->phase1_touched = true;
		for (vector<node_t*>::iterator o_it = this->edge_out[curr->ix].begin(); o_it != this->edge_out[curr->ix].end(); ++o_it) {
			if ((*o_it)->phase1_touched) continue;
			this->phase1_dfs(*o_it);
		}
		this->stack.push_back(curr);
	}

	/**
	* Phase 2 (linear time):
	* - pop node off stack.
	* - edges traversed against directed edges.
	* - all within traversal belong to one strongly-connected component.
	*/
	void phase2() {
		while (!this->stack.empty()) {
			node_t* node = this->stack.back(); this->stack.pop_back();
			if (node->phase2_touched) continue;

			vector<node_t*> traverse;
			traverse.push_back(node);
			while (!traverse.empty()) {
				node_t* curr = traverse.back(); traverse.pop_back();
				curr->phase2_touched = true;
				for (vector<node_t*>::iterator i_it = this->edge_in[curr->ix].begin(); i_it != this->edge_in[curr->ix].end(); ++i_it) {
					node_t* prev = *i_it;
					if (prev->phase2_touched) continue;
					this->components.insert(curr->id, prev->id);
					traverse.push_back(prev);
				}
			}
		}
	}

	vector<node_t*> sinks_sorted() {
		vector<node_t*> sinks;

		/**
		* Process of elimination: If two strongly-connected components
		* are connected, then the one directing out is not a sink.
		*/
		set<uint32> component_non_sink;
		for (uint32 i = 0; i < this->nodes.size(); i++) {
			node_t* curr = &this->nodes[i];
			for (vector<node_t*>::iterator o_it = this->edge_out[curr->ix].begin(); o_it != this->edge_out[curr->ix].end(); ++o_it) {
				if (this->components.find(curr->id) == this->components.find((*o_it)->id)) continue;
				component_non_sink.insert(this->components.find(curr->id));
				break;
			}
		}

		for (uint32 i = 0; i < this->nodes.size(); i++) {
			node_t* curr = &this->nodes[i];
			if (component_non_sink.count(this->components.find(curr->id))) continue;
			sinks.push_back(curr);
		}
		return sinks;
	}

	vector< vector<node_t*> > edge_out, edge_in;

	quick_union components;

	vector<node_t> nodes;
	vector<node_t*> stack;
};

int main(void) {

	for (;;) {

		/**
		* Nodes [1, ..., num_nodes]
		*/
		uint32 num_nodes = 0, num_edges = 0;
		cin >> num_nodes;
		if (num_nodes == 0) break;
		cin >> num_edges;
		kosaraju_graph_t graph(num_nodes);

		for (uint32 i = 0; i < num_edges; i++) {
			uint32 id_from = 0, id_to = 0;
			cin >> id_from >> id_to;
			graph.direct(id_from - 1, id_to - 1);
		}

		graph.phase1();
		graph.phase2();
		vector<node_t*> sinks = graph.sinks_sorted();
		for (uint32 i = 0; i < sinks.size(); i++) {
			cout << sinks[i]->id << ' ';
		}
		cout << endl;
	}

	return 0;
}

