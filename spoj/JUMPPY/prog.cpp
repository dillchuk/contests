#include <iostream>
#include <vector>
#include <stdio.h>
#include <cstring>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author Derek Illchuk dillchuk@gmail.com
*
* Count all rectangles marked out on the grid.
* Rectangles can't butt against another (although corner-to-corner is OK).
*/

struct cell_type {
	cell_type(): on(false), marked(false) {}
	bool on, marked;
};

struct rectangle_type {
	rectangle_type(uint32 row_ix, uint32 col_ix): row_ix(row_ix), col_ix(col_ix), h(1), w(1) {}
	uint32 row_ix, col_ix;
	uint32 h, w;
};

#define NUM_EDGES 4
inline bool is_rectangle(const rectangle_type& r, vector< vector<cell_type> >& grid_rows) {

	cell_type* edges[NUM_EDGES];

	for (uint32 row_ix = r.row_ix; row_ix < r.row_ix + r.h; row_ix++) {
		for (uint32 col_ix = r.col_ix; col_ix < r.col_ix + r.w; col_ix++) {
			cell_type& ct = grid_rows[row_ix][col_ix];

			/**
			* Rectangle must be completely filled in anew.
			*/
			if (!ct.on || ct.marked) return false;
			ct.marked = true;

			/**
			* Rectangle mustn't extend outwards.
			*/
			memset(edges, 0, sizeof(edges));
			if (row_ix == r.row_ix) {
				edges[0] = &grid_rows[row_ix - 1][col_ix]; // upwards
			}
			if (row_ix == (r.row_ix + r.h - 1)) {
				edges[1] = &grid_rows[row_ix + 1][col_ix]; // downwards
			}
			if (col_ix == r.col_ix) {
				edges[2] = &grid_rows[row_ix][col_ix - 1]; // leftwards
			}
			if (col_ix == (r.col_ix + r.w - 1)) {
				edges[3] = &grid_rows[row_ix][col_ix + 1]; // rightwards
			}
			for (uint32 i = 0; i < NUM_EDGES; i++) {
				if (!edges[i] || !edges[i]->on) continue;
				edges[i]->marked = true;
				return false;
			}
		}
	}
	return true;
}

int main(void) {
	ios::sync_with_stdio(false);

	uint32 count = 0;

	uint32 n = 0;
	scanf("%d", &n);

	/**
	* Grid has padded border.
	*/
	vector< vector<cell_type> > grid_rows(n + 2);
	for (uint32 row_ix = 0; row_ix < grid_rows.size(); row_ix++) {
		grid_rows[row_ix] = vector<cell_type>(n + 2);
		vector<cell_type>& grid_row = grid_rows[row_ix];
		if (row_ix == 0 || row_ix == grid_rows.size() - 1) continue;

		string line;
		line.resize(n);
		scanf("%s", &line[0]);
		for (uint32 c_ix = 0; c_ix < line.size(); c_ix++) {
			if (line[c_ix] == '0') continue;
			grid_row[c_ix + 1].on = true;
		}
	}

	for (uint32 row_ix = 0; row_ix < grid_rows.size(); row_ix++) {
		for (uint32 col_ix = 0; col_ix < grid_rows[row_ix].size(); col_ix++) {
			cell_type& ct = grid_rows[row_ix][col_ix];
			if (!ct.on || ct.marked) continue;

			/**
			* Will always encounter top-left first;
			* feel out rectangle's top-left sides.
			*/
			rectangle_type r(row_ix, col_ix);
			while (grid_rows[row_ix][col_ix + r.w].on) r.w++;
			while (grid_rows[row_ix + r.h][col_ix].on) r.h++;
			if (is_rectangle(r, grid_rows)) count++;
			col_ix += r.w - 1; // optimization
		}
	}

	cout << count << endl;

	return 0;
}
