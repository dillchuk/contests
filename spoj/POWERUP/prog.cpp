#include <iostream>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author Derek Illchuk dillchuk@gmail.com
* Modular exponentiation, modular power, phi, totient, power of a power.
*/

/**
* Euler's PHI function; where `n` is prime, totient is `n - 1`.
*/
#define MOD 1000000007LL
#define TOTIENT (MOD - 1)

uint64 modular_pow(uint64 base, uint64 exp, uint64 mod) {
	uint64 result = 1;
	base = base % mod;
	while (exp > 0) {
		if (exp % 2 == 1) {
			result *= base;
			result %= mod;
		}
		exp >>= 1;
		base *= base;
		base %= mod;
	}
	return result;
}

/**
* Solve a^(b^c) mod MOD.
*/
uint64 solve(uint64 a, uint64 b, uint64 c) {
	
	/**
	* Where `a % MOD == 0`, totient is unusable; fortunately answer is either 0 or 1.
	* (Because `a^anything` will never equal `1 mod MOD`.)
	*/
	bool use_totient = (modular_pow(a, TOTIENT, MOD) == 1); // same as `a % MOD == 0`
	if (!use_totient) {
		bool bc_zero = (b == 0 && c > 0);
		return (bc_zero ? 1 : 0);
	}

	/**
	* Can use totient.
	* `a^TOTIENT == 1 mod MOD`, so reduce b^c with `b^c mod TOTIENT`.
	*/
	return modular_pow(a, modular_pow(b, c, TOTIENT), MOD);
}

int main(void) {
	ios::sync_with_stdio(false);

	for (;;) {
		int64 a = 0, b = 0, c = 0; cin >> a >> b >> c;
		if (a < 0) break;

		cout << solve(a, b, c) << endl;
	}

	return 0;
}
