#include <iostream>
#include <vector>
#include <stdlib.h>
#include <limits.h>
#include <map>
#include <stdio.h>
#include <algorithm>
#include <string>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* SHPATH, Floyd-Warshall, all pairs shortest path.
* N.B. spoj SHPATH time-limit exceeded, floyd not suitable for problem.
*
* @see https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm
*/

void floyd(vector< vector<uint32> >& G) {
	for (uint32 k = 0; k < G.size(); k++) {
		for (uint32 i = 0; i < G.size(); i++) {
			for (uint32 j = 0; j < G.size(); j++) {
				uint32 through_k = G[i][k] + G[k][j];
				G[i][j] = min(G[i][j], through_k);
			}
		}
	}
}

#define MAX_NAME_LEN 10

int main(void) {
	uint32 num_cases = 0;
	scanf("%d", &num_cases);
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {

		uint32 num_nodes = 0;
		scanf("%d", &num_nodes);
		vector< vector<uint32> > G(num_nodes);
		for (uint32 i = 0; i < G.size(); i++) {
			G[i] = vector<uint32>(num_nodes, LLONG_MAX);
		}
		map<string, uint32> cities; // name, node ID
		for (uint32 id = 0; id < num_nodes; id++) {

			string name;
			name.resize(MAX_NAME_LEN + 1);
			scanf("%s", &name[0]);
			cities[name] = id;

			uint32 num_edges = 0;
			scanf("%d", &num_edges);
			for (uint32 e = 0; e < num_edges; e++) {
				uint32 edge_to = 0, weight = 0;
				scanf("%d %d", &edge_to, &weight);
				edge_to--;
				G[id][edge_to] = weight;
			}
		}

		floyd(G);

		uint32 num_queries = 0;
		scanf("%d", &num_queries);
		for (uint32 q = 0; q < num_queries; q++) {
			string begin, end;
			begin.resize(MAX_NAME_LEN + 1);
			end.resize(MAX_NAME_LEN + 1);
			scanf("%s %s", &begin[0], &end[0]);
			cout << G[cities[begin]][cities[end]] << endl;
		}
	}

	return 0;
}

