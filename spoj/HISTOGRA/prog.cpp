#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* Largest rectangle (area) within histogram (bar graph), O(n).
*
* @see https://en.wikipedia.org/wiki/All_nearest_smaller_values#Sequential_algorithm
* @see http://www.geeksforgeeks.org/largest-rectangle-under-histogram/
*/

uint64 max_area_linear(const vector<uint64>& H) {
	if (H.size() < 2 || H.front() || H.back()) {
		cout << "H must be book-ended by 0's" << endl;
		exit(1);
	}

	uint64 max_area = 0;
	vector<uint32> S(1); // sentinel
	for (uint32 i = 1; i < H.size(); i++) {
		if (H[i] >= H[S.back()]) {
			S.push_back(i);
			continue;
		}

		/**
		* This is smaller therefore marks the end of (previous) taller rectangle.
		*/
		uint32 prev_i = S.back();
		uint32 prev_w = i - 1 - S[S.size() - 2];
		uint64 prev_h = H[prev_i];
		S.pop_back();
		max_area = max(max_area, prev_w * prev_h);
		i--; // can mark the end of several
	}
	return max_area;
}

int main(void) {
	for (;;) {
		uint32 n = 0; cin >> n;
		if (n == 0) break;

		vector<uint64> H(n + 2); // book-end sentinels
		for (uint32 i = 1; i <= n; i++) {
			cin >> H[i];
		}
		cout << max_area_linear(H) << endl;
	}

	return 0;
}
