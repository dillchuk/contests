/**
* http://www.spoj.com/problems/MKMONEY/
*/

#include <iostream>
#include <math.h>

using namespace std;

typedef unsigned int uint32;
typedef int int32;

int32 main(void) {
	ios::sync_with_stdio(false);
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);

	for (uint32 case_id = 1;; case_id++) {
		double money = 0, interest = 0;
		uint32 periods = 0;
		cin >> money >> interest >> periods;
		if (!periods) break;

		/**
		* Work with whole cents only.
		*/
		double result_cents = money * 100;
		double interest_compound = interest / (100 * periods);
		for (uint32 p_ix = 0; p_ix < periods; p_ix++) {
			result_cents += result_cents * interest_compound;
			result_cents = floor(result_cents + 1e-8); // watch out for x.99999999
		}
		double result = result_cents / 100.0;

		cout << "Case " << case_id << ". $" << money << " at " << interest <<
			"% APR compounded " << periods << " times yields $" << result << endl;
	}

	return 0;
}
