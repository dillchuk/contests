#include <iostream>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
*  First result to get > MAX_GUESTS, calculate beforehand.
*/
#define SEARCH_END (447214 + 1)
#define MAX_GUESTS 100000000000

/**
* Maximum regions via cuts in a plane = n choose 0 + n choose 1 + n choose 2
*
* See "Concrete Mathematics" p499
*/
uint64 max_plane_regions(uint64 cuts) {
	if (cuts == 0) return 1;

	uint64 nc0 = 1;
	uint64 nc1 = cuts;

	/**
	* Divide before multiplying to prevent overflow where possible.
	*/
	uint64 even = (cuts % 2 == 0) ? cuts : cuts - 1;
	uint64 odd = (cuts != even) ? cuts : cuts - 1;
	uint64 nc2 = (even / 2) * odd;

	return nc0 + nc1 + nc2;
}

int main() {

	int32 num_cases = 0; cin >> num_cases;
	while (num_cases--) {

		uint64 target_regions = 0; cin >> target_regions;

		uint32 begin = 0, end = SEARCH_END;
		uint32 solution = begin;
		while (begin < end) {
			uint32 begin2 = (begin + end) / 2;

			uint64 mpr = max_plane_regions(begin2);
			if (max_plane_regions(begin2) >= target_regions) {
				end = begin2;
				solution = begin2;
			}
			else {
				begin = begin2 + 1;
			}
		}
		cout << solution << endl;
	}
	return 0;
}
