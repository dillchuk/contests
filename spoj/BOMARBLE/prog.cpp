#include <iostream>
#include <stdio.h>

using namespace std;
typedef unsigned long uint32;
typedef unsigned long long uint64;

/**
* Author Derek Illchuk dillchuk@gmail.com
* 
* Pentagonal numbers: (3n^2 - n) / 2;
* https://oeis.org/A000326
*/

int main(void) {
	ios::sync_with_stdio(false);

	for (;;) {
		uint32 n = 0; cin >> n;
		if (n == 0) break;
		n++;

		uint64 result = (3 * n * n - n) / 2;
		cout << result << endl;
	}

	return 0;
}
