/**
* http://www.spoj.com/problems/CHASE/
*
* Find maximum number of points that lie on a single line.
*/

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <algorithm>

using namespace std;

typedef unsigned int uint32;
typedef int int32;

typedef pair<int32, int32> point_type;

/**
* Calculate slopes from each point to all subsequent points, then count matches.
*/
uint32 search(const vector<point_type>& points) {
	uint32 global_max = 0;

	for (uint32 i = 0; i < points.size() - 1; i++) {
		vector<double> slopes;

		uint32 vert_max = 0;
		for (uint32 j = i + 1; j < points.size(); j++) {

			/**
			* Handle divide-by-zero.
			*/
			if (points[i].first == points[j].first) {
				vert_max++;
				continue;
			}
			double slope = (points[i].second - points[j].second) * 1.0 / (points[i].first - points[j].first);
			slopes.push_back(slope);
		}
		sort(slopes.begin(), slopes.end());

		uint32 curr = 1, curr_max = max(static_cast<uint32>(1), vert_max);
		for (uint32 s_ix = 1; s_ix < slopes.size(); s_ix++) {
			if (slopes[s_ix - 1] == slopes[s_ix]) {
				curr++;
				curr_max = max(curr_max, curr);
			}
			else {
				curr = 1;
			}
		}
		global_max = max(global_max, curr_max);
	}
	return global_max;
}

int32 main(void) {
	ios::sync_with_stdio(false);

	uint32 num_cases = 0; scanf("%d", &num_cases);
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {
		uint32 num_points = 0; scanf("%d", &num_points);
		vector<point_type> points(num_points);
		for (uint32 point_ix = 0; point_ix < num_points; point_ix++) {
			scanf("%d %d", &points[point_ix].first, &points[point_ix].second);
		}
		printf("%d\n", search(points));
	}
	return 0;
}
