# SPOJ.com #

These solutions are for educational purposes only. Please don't submit them as your own.

Anyway, these are for those problems already thoroughly solved by others.  This means they won't really improve your score much, nor affect other SPOJers.
