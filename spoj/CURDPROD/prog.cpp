#include <iostream>
#include <vector>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Maximum time uses only one lowest-quality machine.
* 10^9 (quality) * 10^9 (target)
*/
#define TIME_UPPER 1000000000000000001LL

uint64 production(vector<uint32>& machines, uint64 time) {
	uint64 total = 0;
	for (uint32 m_ix = 0; m_ix < machines.size(); m_ix++) {
		total += time / machines[m_ix];
	}
	return total;
}

int main( void )
{
	ios::sync_with_stdio(false);

	uint32 num_cases = 0; cin >> num_cases;
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {

		uint32 num_machines = 0; cin >> num_machines;
		uint64 target = 0; cin >> target;
		vector<uint32> machines(num_machines);
		for (uint32 m_ix = 0; m_ix < num_machines; m_ix++) {
			cin >> machines[m_ix];
		}

		/**
		* Span runs [begin, end).  Meaning if begin == end, the span is empty.
		*/
		uint64 begin = 0, end = TIME_UPPER + 1;
		uint64 answer = 0;
		while (begin < end) {
			uint64 mid = (begin + end) / 2;
			uint64 mid_total = production(machines, mid);

			/**
			* This will work; is there something better?
			*/
			if (mid_total >= target) {
				answer = mid;
				end = mid;
				continue;
			}

			/**
			* This won't work; skip it.
			*/
			begin = mid + 1;
		}
		cout << answer << endl;
	}
	
    return 0;
}
