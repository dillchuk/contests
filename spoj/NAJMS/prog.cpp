#include <iostream>
#include <vector>
#include <algorithm>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* Binary Indexed Tree.
*/

/**
* Suitable for:
* - adding values to all indices across a range O(lg n)
* - querying individual values O(lg n)
*
* Not suitable for:
* - querying sum totals across a range
*
* N.B. Indexed from [1, n].
*/
struct binary_indexed_tree {

	binary_indexed_tree(uint32 n): atom(n + 1), range(n + 1) {}

#ifdef BRUTE_FORCE
	void dump(vector<int64>& dump) const {
		for (uint32 id = 1; id < this->atom.size(); id++) {
			dump[id] = this->get(id);
		}
	}
#endif

	/**
	* Returns the value of the last 1-bit.
	* e.g. 1011000 => 1000
	* (And therefore 1011000 is responsible for [1011000 - 1000 + 1, 1011000])
	*/
#define RESPONSIBILITY(id) (id & -static_cast<int32>(id))

	int64 get(uint32 id) const {
		int64 sum = this->atom[id];
		for (uint32 i = id; i < this->range.size(); i += RESPONSIBILITY(i)) {
			sum += this->range[i];
		}
		return sum;
	}

	void set(uint32 id, int64 val) {
		int64 orig = this->get(id);
		this->atom[id] += val - orig;
	}

	void add(uint32 ida, uint32 idb, int64 val) {
		uint32 i = ida;
		while (i <= idb) {
			bool is_range = false;
			for (;;) {

				/**
				* Looking at 1001, the digits above (rightmost 1) responsibility are 0.
				* That's good and means those ranges are still open and available.
				*
				* Looking at 1011, the digit directly above responsiblility is a 1.
				* That's no good: that range has closed.
				*/
				uint32 ri = RESPONSIBILITY(i);
				bool bigger_range_open = (RESPONSIBILITY(i + ri) >> 1 == ri);
				bool too_low = (i - ri + 1 < ida);
				bool too_high = (i + ri > idb);
				if (!bigger_range_open || too_low || too_high) break;

				i += ri;
				is_range = true;
			}
			vector<int64>& to = is_range ? this->range : this->atom;
			to[i] += val;
			i++;
		}
	}

	vector<int64> atom, range;
};

int main(void) {
	ios::sync_with_stdio(false);

	uint32 num_cases = 0;
	scanf("%d", &num_cases);
	for (uint32 case_id = 1; case_id <= num_cases; case_id++) {
		printf("Case %d:\n", case_id);

		uint32 n = 0, num_query = 0;
		scanf("%d%d", &n, &num_query);
		
		binary_indexed_tree bit(n);
		for (uint32 id = 1; id <= n; id++) {
			int64 init = 0; scanf("%lld", &init);
			bit.add(id, id, init);
		}

		for (uint32 q = 0; q < num_query; q++) {
			char command = '\0';
			uint32 ida = 0, idb = 0;
			int64 val = 0;

			scanf(" %c", &command);
			if (command == 'w') {
				scanf("%d%d%lld", &ida, &idb, &val);
				bit.add(ida, idb, val);
			}

			else if (command == 'm') {
				scanf("%d", &ida);
				bit.set(ida, 0);
			}

			else if (command == 'f') {
				scanf("%d", &ida);
				printf("%lld\n", bit.get(ida));
			}
		}
		if (case_id < num_cases) {
			printf("\n");
		}
	}

	return 0;
}

