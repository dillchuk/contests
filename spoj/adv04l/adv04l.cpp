#include <iostream>
#include <vector>
#include <set>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

#define FIB_LIMIT 1000000000000000LL * 2 // answer is less than (input * 2)

int main(void) {

	/**
	 * Calculate our fibonacci numbers, once.
	 */
	set<uint64> fibonacci;
	uint64 fend = 1, fpen = 1;
	fibonacci.insert(fend);
	for (;;) {
		uint64 fcurr = fend + fpen;
		if (fcurr > FIB_LIMIT) break;
		fibonacci.insert(fibonacci.end(), fcurr);
		fpen = fend;
		fend = fcurr;
	}

	/**
	 * Breakdown, finding the pertinent fib-number using binary search.
	 */
	uint32 num_cases = 0; cin >> num_cases;
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {
		uint64 miles = 0; cin >> miles;
		uint64 km = 0;
		while (miles > 0) {

			/**
			* Find next value *above* miles; that's km.  Then roll back for miles.
			*/
			set<uint64>::iterator km_it = fibonacci.lower_bound(miles + 1);
			set<uint64>::iterator miles_it = km_it; --miles_it;
			km += *km_it;
			miles -= *miles_it;
		}
		cout << km << endl;
	}

	return 0;
}
