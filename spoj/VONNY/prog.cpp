/**
 * http://www.spoj.com/problems/VONNY/
 * 
 * This implementation shows 2D graph backtracking via the concept called "Dancing Links".
 * Further information is here:
 * http://www.derekillchuk.com/archives/282
 */

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <set>
#include <string>
#include <algorithm>

using namespace std;

typedef unsigned int uint32;
typedef int int32;

const uint32 INPUT_WIDTH = 8;
const uint32 INPUT_HEIGHT = 7;
const uint32 DOMINO_MAX = 6;

enum Direction { h, v, size };

struct domino_type {
	uint32 a, b;
	domino_type(uint32 a, uint32 b) {
		this->a = min(a, b);
		this->b = max(a, b);
	}

	bool operator<(const domino_type& other) const {
		if (a < other.a) return true;
		if (a > other.a) return false;
		return (b < other.b);
	}
};

struct node_type {
	vector<node_type*> header;
	vector<node_type*> next;
	vector<node_type*> prev;
	vector<node_type*> neighbor;

	uint32 value;

	node_type(): value(-1) {
		header = vector<node_type*>(Direction::size);
		next = vector<node_type*>(Direction::size);
		prev = vector<node_type*>(Direction::size);
		neighbor = vector<node_type*>(Direction::size);
	}
};

node_type* build_graph(uint32 width, uint32 height) {

	/**
	 * Construct root.
	 */
	node_type* root = new node_type;

	/**
	 * Construct row/column headers.
	 */
	vector<node_type*> row_headers(height);
	for (uint32 v_ix = 0; v_ix < height; v_ix++) {
		row_headers[v_ix] = new node_type;
	}
	root->next[Direction::v] = row_headers[0];
	for (uint32 v_ix = 0; v_ix < height - 1; v_ix++) {
		row_headers[v_ix]->next[Direction::v] = row_headers[v_ix + 1];
	}
	row_headers.back()->next[Direction::v] = root;

	vector<node_type*> col_headers(width);
	for (uint32 h_ix = 0; h_ix < width; h_ix++) {
		col_headers[h_ix] = new node_type;
	}
	root->next[Direction::h] = col_headers[0];
	for (uint32 h_ix = 0; h_ix < width - 1; h_ix++) {
		col_headers[h_ix]->next[Direction::h] = col_headers[h_ix + 1];
	}
	col_headers.back()->next[Direction::h] = root;

	/**
	 * Construct grid.
	 */
	vector< vector<node_type*> > grid(height);
	for (uint32 v_ix = 0; v_ix < height; v_ix++) {
		node_type* row_header = row_headers[v_ix];
		grid[v_ix] = vector<node_type*>(width);
		for (uint32 h_ix = 0; h_ix < width; h_ix++) {
			node_type* col_header = col_headers[h_ix];

			node_type* node = new node_type;
			node->header[Direction::v] = col_header;
			node->header[Direction::h] = row_header;
			node->next[Direction::v] = node->prev[Direction::v] = col_header;
			node->next[Direction::h] = node->prev[Direction::h] = row_header;
			grid[v_ix][h_ix] = node;
		}
	}
	for (uint32 v_ix = 0; v_ix < height; v_ix++) {
		for (uint32 h_ix = 0; h_ix < width; h_ix++) {
			if (v_ix > 0) {
				grid[v_ix][h_ix]->prev[Direction::v] = grid[v_ix - 1][h_ix];
			}
			if (v_ix < (height - 1)) {
				grid[v_ix][h_ix]->next[Direction::v] = grid[v_ix][h_ix]->neighbor[Direction::v] = grid[v_ix + 1][h_ix];
			}
			if (h_ix > 0) {
				grid[v_ix][h_ix]->prev[Direction::h] = grid[v_ix][h_ix - 1];
			}
			if (h_ix < (width - 1)) {
				grid[v_ix][h_ix]->next[Direction::h] = grid[v_ix][h_ix]->neighbor[Direction::h] = grid[v_ix][h_ix + 1];
			}
		}
	}

	for (uint32 v_ix = 0; v_ix < height; v_ix++) {
		row_headers[v_ix]->next[Direction::h] = grid[v_ix][0];
	}
	for (uint32 h_ix = 0; h_ix < width; h_ix++) {
		col_headers[h_ix]->next[Direction::v] = grid[0][h_ix];
	}

	return root;
}

void destroy_graph(node_type* root) {
	vector<node_type*> nodes;
	nodes.push_back(root);
	node_type* col_header = root->next[Direction::h];
	while (col_header != root) {
		nodes.push_back(col_header);
		col_header = col_header->next[Direction::h];
	}
	node_type* row_header = root->next[Direction::v];
	while (row_header != root) {
		nodes.push_back(row_header);
		node_type* node = row_header->next[Direction::h];
		while (node != row_header) {
			nodes.push_back(node);
			node = node->next[Direction::h];
		}
		row_header = row_header->next[Direction::v];
	}
	for (vector<node_type*>::iterator n_it = nodes.begin(); n_it != nodes.end(); ++n_it) {
		delete *n_it;
	}
}

void input_row(node_type* row_header, vector<uint32> row) {
	node_type* node = row_header->next[Direction::h];
	for (vector<uint32>::iterator r_it = row.begin(); r_it != row.end(); ++r_it) {
		node->value = *r_it;
		node = node->neighbor[Direction::h];
	}
}

void pretty_print(node_type* root) {
	node_type* row_header = root->next[Direction::v];
	while (row_header != root) {
		node_type* node = row_header->next[Direction::h];
		while (node != row_header) {
			cout << node->value << ' ';
			node = node->next[Direction::h];
		}
		cout << endl;
		row_header = row_header->next[Direction::v];
	}
}

void pretty_print_transverse(node_type* root) {
	node_type* col_header = root->next[Direction::h];
	while (col_header != root) {
		node_type* node = col_header->next[Direction::v];
		while (node != col_header) {
			cout << node->value << ' ';
			node = node->next[Direction::v];
		}
		cout << endl;
		col_header = col_header->next[Direction::h];
	}
}

node_type* first_available(node_type* node) {
	node_type* row_header = node->next[Direction::v];
	while (row_header != node) {
		node_type* next = row_header->next[Direction::h];
		if (next != row_header) {
			return next;
		}
		row_header = row_header->next[Direction::v];
	}

	return 0;
}

bool is_meta(node_type *node) {
	return (!node->header[Direction::h] && !node->header[Direction::v]);
}

void cover(node_type* node) {
	bool is_error = (!node || is_meta(node));
	if (is_error) {
		throw "cover(node) error";
	}

	for (uint32 dir_id = 0; dir_id < Direction::size; dir_id++) {
		node->prev[dir_id]->next[dir_id] = node->next[dir_id];
		node->next[dir_id]->prev[dir_id] = node->prev[dir_id];
	}
}

void cover(node_type* node, Direction direction) {
	node_type* neighbor = node->neighbor[direction];

	bool is_error = false;
	is_error |= !node;
	is_error |= (is_meta(node) || is_meta(node->next[direction]));
	is_error |= (neighbor != node->next[direction]);
	if (is_error) {
		throw "cover(node, direction) error";
	}

	cover(node);
	cover(neighbor);
}

/**
 * Here's where "Dancing Links" works its magic.  We simply pop our node back in.
 */
void uncover(node_type* node) {
	bool is_error = (!node || is_meta(node));
	if (is_error) {
		throw "uncover(node) error";
	}

	for (uint32 dir_id = 0; dir_id < Direction::size; dir_id++) {
		node->prev[dir_id]->next[dir_id] = node->next[dir_id]->prev[dir_id] = node;
	}
}

void uncover(node_type* node, Direction direction) {
	node_type* neighbor = node->neighbor[direction];

	bool is_error = false;
	is_error |= !node;
	is_error |= (is_meta(node) || is_meta(node->next[direction]));
	is_error |= (node->next[direction] != node->neighbor[direction]);
	if (is_error) {
		throw "uncover(node, direction) error";
	}

	/**
	 * Important: must uncover in stack (i.e. opposite) order of cover!
	 */
	uncover(neighbor);
	uncover(node);
}

/**
 * Which domino cover options are there?
 */
typedef pair<Direction, domino_type> cover_option;
vector<cover_option> get_cover_options(node_type* node) {
	bool is_error = false;
	is_error |= !node || is_meta(node);
	if (is_error) {
		throw "get_cover_options(node) error";
	}

	vector<cover_option> options;
	for (uint32 dir_id = 0; dir_id < Direction::size; dir_id++) {
		if (node->neighbor[dir_id] != node->next[dir_id]) continue;

		cover_option option(
			static_cast<Direction>(dir_id), domino_type(node->value, node->neighbor[dir_id]->value)
		);
		options.push_back(option);
	}
	return options;
}

uint32 count_solutions(node_type* root, set<domino_type>& dominoes) {
	uint32 num_solutions = 0;
	node_type* node = first_available(root);
	if (!node) return 1;

	vector<cover_option> options = get_cover_options(node);
	for (vector<cover_option>::iterator d_it = options.begin(); d_it != options.end(); ++d_it) {
		domino_type domino = d_it->second;
		if (!dominoes.count(domino)) continue;

		cover(node, d_it->first);
		dominoes.erase(domino);
		num_solutions += count_solutions(root, dominoes);
		dominoes.insert(domino);
		uncover(node, d_it->first);
	}

	return num_solutions;
}

int32 main(void) {

	/**
	 * Reusable.
	 */
	node_type* root = build_graph(INPUT_WIDTH, INPUT_HEIGHT);

	uint32 num_cases = 0; cin >> num_cases;
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {

		set<domino_type> dominoes;
		for (uint32 a = 0; a <= DOMINO_MAX; a++) {
			for (uint32 b = a; b <= DOMINO_MAX; b++) {
				dominoes.insert(domino_type(a, b));
			}
		}

		node_type* row_header = root->next[Direction::v];
		for (uint32 r_ix = 0; r_ix < INPUT_HEIGHT; r_ix++) {
			vector<uint32> row(INPUT_WIDTH);
			for (uint32 c_ix = 0; c_ix < INPUT_WIDTH; c_ix++) {
				cin >> row[c_ix];
			}
			input_row(row_header, row);
			row_header = row_header->next[Direction::v];
		}

		cout << count_solutions(root, dominoes) << endl;
	}

	destroy_graph(root);
	return 0;
}
