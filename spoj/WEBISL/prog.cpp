#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
typedef unsigned long uint32;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* WEBISL, Strongly connected components, Kosaraju's algorithm
* @see https://en.wikipedia.org/wiki/Kosaraju%27s_algorithm
*/

struct quick_union {
	quick_union(uint32 max_ix): sets(max_ix + 1) {}

	void insert(uint32 a, uint32 b = 0) {
		if (b == 0) b = a;
		if (a == 0) {
			cout << "Items must have id > 0" << endl;
			exit(EXIT_FAILURE);
		}

		if (this->sets[a] == 0) this->sets[a] = a;
		if (this->sets[b] == 0) this->sets[b] = b;

		/**
		* Note: not balanced because lowest ID possible required
		* as per problem description.  Balanced union tree is better.
		* @see http://www.derekillchuk.com/archives/355
		*/
		a = this->find(a);
		b = this->find(b);
		this->sets[max(a, b)] = min(a, b);
	}

	/**
	* Returns union-set lowest possible ID (as per problem description).
	*/
	uint32 find(uint32 id) {
		if (id >= this->sets.size()) return 0;
		for (; id != this->sets[id]; id = this->sets[id]) {
			this->sets[id] = this->sets[this->sets[id]]; // collapse tree a bit
		}
		return id;
	}

	vector<uint32> sets;
};

struct kosaraju_node_t {
	kosaraju_node_t(): id(0), phase1_touched(false), phase2_touched(false) {}
	uint32 id;

	/**
	* Enables the following to run in O(1):
	*  "Is node in stack?"
	*  "Remove from stack"
	*/
	bool phase1_touched, phase2_touched;
};
typedef kosaraju_node_t node_t;

struct kosaraju_graph_t {
	kosaraju_graph_t(uint32 size): nodes(size), edge_out(size), edge_in(size), components(size){
		for (uint32 id = 0; id < size; id++) {
			this->nodes[id].id = id;
			this->components.insert(id + 1); // [1, size]
		}
	}

	void direct(uint32 id_from, uint32 id_to) {
		this->edge_out[id_from].push_back(&this->nodes[id_to]);
		this->edge_in[id_to].push_back(&this->nodes[id_from]);
	}

	/**
	* Phase 1 (linear time):
	* - grab an unseen node and perform depth-first search.
	* - add node to stack after children processed.
	*/
	void phase1() {
		uint32 v = 0;
		while (this->stack.size() < this->nodes.size()) {
			node_t* node = 0;
			for (; v < this->nodes.size(); v++) {
				if (this->nodes[v].phase1_touched) continue;
				node = &this->nodes[v];
				break;
			}
			phase1_dfs(node);
		}
	}
	void phase1_dfs(node_t* curr) {
		curr->phase1_touched = true;
		for (vector<node_t*>::iterator o_it = this->edge_out[curr->id].begin(); o_it != this->edge_out[curr->id].end(); ++o_it) {
			if ((*o_it)->phase1_touched) continue;
			this->phase1_dfs(*o_it);
		}
		this->stack.push_back(curr);
	}

	/**
	* Phase 2 (linear time):
	* - pop node off stack.
	* - edges traversed against directed edges.
	* - all within traversal belong to one strongly-connected component.
	*/
	void phase2() {
		while (!this->stack.empty()) {
			node_t* node = this->stack.back(); this->stack.pop_back();
			if (node->phase2_touched) continue;

			vector<node_t*> traverse;
			traverse.push_back(node);
			while (!traverse.empty()) {
				node_t* curr = traverse.back(); traverse.pop_back();
				curr->phase2_touched = true;
				for (vector<node_t*>::iterator i_it = this->edge_in[curr->id].begin(); i_it != this->edge_in[curr->id].end(); ++i_it) {
					node_t* prev = *i_it;
					if (prev->phase2_touched) continue;
					this->components.insert(curr->id + 1, prev->id + 1);
					traverse.push_back(prev);
				}
			}
		}
	}

	vector< vector<node_t*> > edge_out, edge_in;

	quick_union components;

	vector<node_t> nodes;
	vector<node_t*> stack;
};

int main(void) {

	/**
	* Nodes [0, ..., num_nodes - 1]
	*/
	uint32 num_nodes = 0, num_edges = 0;
	cin >> num_nodes >> num_edges;
	kosaraju_graph_t graph(num_nodes);

	for (uint32 i = 0; i < num_edges; i++) {
		uint32 id_from = 0, id_to = 0;
		cin >> id_from >> id_to;
		graph.direct(id_from, id_to);
	}

	graph.phase1();
	graph.phase2();
	for (uint32 id = 1; id <= graph.nodes.size(); id++) {
		cout << (graph.components.find(id) - 1) << endl;
	}

	return 0;
}
