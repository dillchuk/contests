#include <iostream>
#include <vector>
#include <string>
#include <map>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

#define NUM_DIGITS 10

vector<bool> digits_tf(const string& input) {
	vector<bool> digits(NUM_DIGITS);
	for (string::const_iterator i_it = input.begin(); i_it != input.end(); ++i_it) {
		digits[*i_it - '0'] = true;
	}
	return digits;
}

inline bool contains_only(const uint64 input, const vector<bool>& match) {
	uint64 n = input;
	do {
		if (!match[n % 10]) {
			return false;
		}
		n /= 10;
	} while (n);
	return true;
}

struct entry_type {

	entry_type(const entry_type& entry, uint64 add):
	active(entry.active + add), active_min(entry.active_min), active_max(entry.active_max), leading_zero(add == 0), sums(entry.sums), match(entry.match) {}

	entry_type(uint64 active, uint64 min, uint64 max, const vector<uint64>& sums, const vector<bool>& match):
		active(active), active_min(min), active_max(max), sums(sums), match(match), leading_zero(active == 0) {}

	/**
	* Take least-significant digit, if we can.
	* Then adjust our running min/max.
	*/
	bool lock_digit() {
		if (this->active > this->active_max) return false;

		uint32 digit = this->active % 10;
		if (!match[digit]) return false;
		this->active /= 10;

		uint32 max_digit = this->active_max % 10;
		this->active_max /= 10;
		if (digit > max_digit) {
			this->active_max--;
		}

		uint32 min_digit = this->active_min % 10;
		this->active_min /= 10;
		if (digit < min_digit) {
			this->active_min++;
		}
		return true;
	}

	bool is_countable() const {
		if (this->leading_zero) return false;
		if (this->active < this->active_min) return false;
		if (this->active > this->active_max) return false;
		if (this->active == 0) return true;
		return contains_only(this->active, match);
	}

	uint64 active, active_min, active_max;
	bool leading_zero;

	const vector<uint64>& sums;
	const vector<bool>& match;
};
bool operator<(const entry_type& lhs, const entry_type& rhs) {
	if (lhs.active < rhs.active) return true;
	if (lhs.active > rhs.active) return false;
	if (lhs.active_min < rhs.active_min) return true;
	if (lhs.active_min > rhs.active_min) return false;
	if (lhs.active_max < rhs.active_max) return true;
	if (lhs.active_max > rhs.active_max) return false;
	return (lhs.leading_zero < rhs.leading_zero);
}

uint64 calc_multiples(const entry_type& entry) {
	uint64 count = 0;
	if (entry.active > entry.active_max) return count;

	static map<entry_type, uint64> cache;
	entry_type curr = entry;
	if (!cache.count(entry)) {
		if (curr.lock_digit()) {
			if (curr.is_countable()) {
				count++;
			}

			for (uint32 i = 0; i < curr.sums.size() && curr.active_max > 0; i++) {
				count += calc_multiples(entry_type(curr, curr.sums[i]));
			}
		}
		cache[entry] = count;
		return count; // optimization
	}
	return cache[entry];
}

int main(void) {
	ios::sync_with_stdio(false);

	uint64 multiple = 0, min = 0, max = 0; cin >> multiple >> min >> max;
	string digits; cin >> digits;

	vector<bool> match = digits_tf(digits);
	vector<uint64> sums;
	for (uint32 i = 0; i < NUM_DIGITS; i++) {
		sums.push_back(multiple * i);
	}

	uint64 count = 0;
	for (uint32 s_ix = 0; s_ix < sums.size(); s_ix++) {
		count += calc_multiples(entry_type(sums[s_ix], min, max, sums, match));
	}
	cout << count << endl;

	return 0;
}

