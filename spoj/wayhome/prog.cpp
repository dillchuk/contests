#include <iostream>
#include <vector>
#include <algorithm>

/**
* Author Derek Illchuk dillchuk@gmail.com
* 
* Bridge crossing, greedy. Pair shuffles across bridge, one returns, and so on.
*/

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

uint32 solve_bridge_crossing(const vector<uint32>& times) {
	uint32 result = 0;

	vector<uint32> T = times;
	sort(T.begin(), T.end());
	while (!T.empty()) {

		/**
		* Finish up.
		*/
		if (T.size() <= 2) {
			result += T.back();
			break;
		}

		/**
		* 3 left, just do it.
		*/
		if (T.size() == 3) {
			result += max(T[0], T[2]) + T[0] + max(T[0], T[1]); // `max()` for clarity
			break;
		}

		/**
		* Many left, so get rid of two highest with one of the following methods:
		*
		* a. `lo` chauffeurs `hi` across, comes back and does the same for `hi2`.
		* b. `lo` chauffeurs `lo2`, `lo` returns, `hi` and `hi2` cross and `lo2 returns. (`lo`/`lo2` order does not matter.)
		*/
		uint32 lo = T[0], lo2 = T[1];
		uint32 hi = T.back(), hi2 = T[T.size() - 2];
		uint32 cost_a = max(lo, hi) + lo + max(lo, hi2) + lo; // `max()` for clarity
		uint32 cost_b = max(lo, lo2) + lo + max(hi, hi2) + lo2; 
		T.erase(T.end() - 2, T.end());
		result += min(cost_a, cost_b);
	}

	return result;
}

int main() {
	int32 num_cases = 0; cin >> num_cases;
	while (num_cases--) {
		uint32 n = 0; cin >> n;
		vector<uint32> times(n);
		for (uint32 i = 0; i < n; i++) {
			cin >> times[i];
		}
		cout << solve_bridge_crossing(times) << endl;
	}
	return 0;
}
