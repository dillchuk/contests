#include <iostream>
#include <vector>
#include <algorithm>
#include <time.h>
#include <limits.h>

using namespace std;
typedef unsigned long uint32;
typedef long int32;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* Longest increasing subsequence, O(nlgn).
*/

vector<int32> longest_non_decreasing(vector<int32>& I) {
	vector<int32> prev(I.size());

	/**
	* M[j] = i means longest increasing sub length j ends on i
	*/
	vector<int32> M(I.size() + 1, LONG_MIN);

	uint32 longest = 0;
	for (uint32 i = 0; i < I.size(); i++) {
		uint32 begin = 1, end = longest + 1;

		while (begin < end) {
			uint32 mid = (begin + end) / 2;
			if (I[M[mid]] <= I[i]) {
				begin = mid + 1;
				continue;
			}
			end = mid;
		}

		uint32 curr_longest = begin;
		prev[i] = M[curr_longest - 1];
		M[curr_longest] = i;
		longest = max(longest, curr_longest);
	}

	vector<int32> result(longest);
	int32 ix = M[longest];
	for (int32 s_ix = longest - 1; s_ix >= 0; s_ix--) {
		result[s_ix] = I[ix];
		ix = prev[ix];
	}
	return result;
}

bool operator<(pair<int32, int32> lhs, pair<int32, int32> rhs) {
	if (lhs.first < rhs.first) return true;
	if (lhs.first > rhs.first) return false;
	return (lhs.second < rhs.second);
}

int main(void) {

	uint32 num_cases = 0; cin >> num_cases;
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {
		uint32 n = 0; cin >> n;
		vector< pair<int32, int32> > bridges(n);
		for (uint32 i = 0; i < n; i++) {
			cin >> bridges[i].first;
		}
		for (uint32 i = 0; i < n; i++) {
			cin >> bridges[i].second;
		}

		/**
		* The solution must produce the longest non-decreasing subsequence of both.
		* How to handle?  Sort all bridges by `first` then find this subsequence on `second`.
		*/
		sort(bridges.begin(), bridges.end());
		vector<int32> I(n);
		for (uint32 i = 0; i < n; i++) {
			I[i] = bridges[i].second;
		}
		cout << longest_non_decreasing(I).size() << endl;
	}

	return 0;
}

