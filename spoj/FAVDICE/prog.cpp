#include <iostream>
#include <stdio.h>

using namespace std;
typedef unsigned long uint32;

/**
* Author Derek Illchuk dillchuk@gmail.com
* 
* Coupon Collector's problem.
* https://en.wikipedia.org/wiki/Coupon_collector%27s_problem
* 
* result = N x (SUM i=1...N)->(1/i)
*/
double coupon_problem(uint32 num_coupons) {
	double num_draws = 0;
	for (uint32 i = 1; i <= num_coupons; i++) {
		num_draws += (1.0 / i);
	}
	num_draws *= num_coupons;
	return num_draws;
}

int main(void) {
	ios::sync_with_stdio(false);

	uint32 num_inputs = 0; cin >> num_inputs;
	for (uint32 i_ix = 0; i_ix < num_inputs; i_ix++) {
		uint32 num_coupons = 0; cin >> num_coupons;
		printf("%.2f\n", coupon_problem(num_coupons));
	}

	return 0;
}
