#include <iostream>
#include <vector>
#include <stdlib.h>
#include <set>
#include <map>
#include <algorithm>
#include <iterator>
#include <string>
#include <cstring>
#include <time.h>

using namespace std;
typedef unsigned long uint32;
typedef long int32;

/**
* Author Derek Illchuk dillchuk@gmail.com
*
* Algorithm X, Queens problem, Dancing links, Backtracking, Exact cover problem.
*/

struct column_type {
	column_type(): ix(-1), is_primary(true) {}
	column_type(uint32 ix, /*string descrip, */bool is_primary): ix(ix), is_primary(is_primary) {}
	int32 ix;

	/**
	* true: must be covered once.
	* false: covered once or not at all.
	*/
	bool is_primary;
};
bool operator<(const column_type& lhs, const column_type& rhs) {
	return (lhs.ix < rhs.ix);
}

struct rank_file_type {
	rank_file_type(): rank(-1), file(-1) {};
	rank_file_type(uint32 rank, uint32 file): rank(rank), file(file) {}
	int32 rank, file;
};
bool operator<(const rank_file_type& lhs, const rank_file_type& rhs) {
	if (lhs.rank < rhs.rank) return true;
	if (lhs.rank > rhs.rank) return false;
	return (lhs.file < rhs.file);
}
bool operator==(const rank_file_type& lhs, const rank_file_type& rhs) {
	return !(lhs < rhs) && !(rhs < lhs);
}
bool operator!=(const rank_file_type& lhs, const rank_file_type& rhs) {
	return !(lhs == rhs);
}

void queen_columns(vector<column_type>& qc/*optimization*/, rank_file_type rf, int32 n) {

	/**
	* Optimization: cache for each n.
	*/
	static map< uint32, map< rank_file_type, vector<column_type> > > qcs;

	if (qcs[n].empty()) {
		bool PRIMARY = true, SECONDARY = false;
		uint32 rank_beg_ix = 0;
		uint32 file_beg_ix = rank_beg_ix + n;
		uint32 diag_beg_dr = file_beg_ix + n;
		uint32 diag_beg_ur = diag_beg_dr + 2 * n - 1;

		for (uint32 i = 0; i < n; i++) {
			for (uint32 j = 0; j < n; j++) {
				rank_file_type rf_curr(i, j);
				vector<column_type> cols;
				cols.push_back(column_type(rank_beg_ix + rf_curr.rank, PRIMARY));
				cols.push_back(column_type(file_beg_ix + rf_curr.file, PRIMARY));
				int32 diff_dr = rf_curr.file - rf_curr.rank;
				uint32 diag_dr = diff_dr + n - 1;
				cols.push_back(column_type(diag_beg_dr + diag_dr,  SECONDARY));
				uint32 diag_ur = rf_curr.rank + rf_curr.file;
				cols.push_back(column_type(diag_beg_ur + diag_ur,  SECONDARY));
				qcs[n][rf_curr] = cols;
			}
		}
	}
	qc = qcs[n][rf];
}

//#define USE_CENTRAL_HEURISTIC
#ifdef USE_CENTRAL_HEURISTIC
/**
* Orders n*n grid from inside out.
* Intent: then we can search from the center, yields faster results.
*/
vector<rank_file_type> order_by_centrality(uint32 n) {
	multimap<uint32, rank_file_type> central; // weight, grid_ix
	for (uint32 r = 0; r < n; r++) {
		for (uint32 f = 0; f < n; f++) {
			uint32 weight = min(r + 1, n - r) + min(f + 1, n - f);
			central.insert(make_pair(weight, rank_file_type(r, f)));
		}
	}
	vector<rank_file_type> result;
	for (multimap<uint32, rank_file_type>::reverse_iterator c_it = central.rbegin();
		c_it != central.rend(); ++c_it) {
		result.push_back(c_it->second);
	}
	return result;
}
#endif

#define MAX_N 50

struct node_type {

	node_type(): l(0), r(0), u(0), d(0), c(0), size(0) {}

	void clear() {
		this->l = this->r = this->u = this->d = this->c = 0;
		this->size = 0;
	}

	static void cover_column(node_type* header) {
		header->l->r = header->r;
		header->r->l = header->l;
		for (node_type* i = header->d; i != header; i = i->d) {
			for (node_type* j = i->r; j != i; j = j->r) {
				j->u->d = j->d;
				j->d->u = j->u;
				j->c->size--;
			}
		}
	}
	static void uncover_column(node_type* header) {
		for (node_type* i = header->u; i != header; i = i->u) {
			for (node_type* j = i->l; j != i; j = j->l) {
				j->c->size++;
				j->u->d = j;
				j->d->u = j;
			}
		}
		header->l->r = header;
		header->r->l = header;
	}

	node_type *l, *r; // left/right
	node_type *u, *d; // up/down
	node_type *c;     // column header

	column_type col;

	/**
	 * Used only in column header.
	 */
	uint32 size;
};
node_type node_cache[MAX_N * MAX_N * 5];
uint32 nc_next_ix = 0;

struct root_type: node_type {

	root_type() {
		nc_next_ix = 0;
	}

	node_type* new_node(column_type col = column_type()) {
		node_type* node = &node_cache[nc_next_ix++];
		node->clear();
		node->col = col;
		return node;
	}

	/**
	 * Creates the entire structure.
	 */
	static root_type* root(uint32 n, set<uint32>& taken_cols, vector< vector<column_type> >& solution) {
		root_type* root = new root_type();

		/**
		* Columns are the following:
		* - rank (n)
		* - file (n)
		* - diagonal (2n - 1)
		* - diagonal (2n - 1)
		* Total: 6n - 2
		*
		* Each row is a possible queen placement, n * n.
		*/
		uint32 num_cols = 6 * n - 2;
		uint32 num_rows = n * n;
#define MAX_COLS (6 * MAX_N - 2)
#define MAX_ROWS (MAX_N * MAX_N)
		static vector< vector<node_type*> > cols(MAX_COLS), rows(MAX_ROWS);
		for (uint32 i = 0; i < cols.size(); i++) cols[i].clear();
		for (uint32 i = 0; i < rows.size(); i++) rows[i].clear();

#ifdef USE_CENTRAL_HEURISTIC
		/**
		* Order rows by centrality -- a good search heuristic.
		*/
		vector<column_type> qc;
		vector<rank_file_type> ordering = order_by_centrality(n);
		for (uint32 o_ix = 0; o_ix < ordering.size(); o_ix++) {
			queen_columns(qc, ordering[o_ix], n);

			bool is_taken = false;
			for (vector<column_type>::iterator qc_it = qc.begin(); qc_it != qc.end(); ++qc_it) {
				if (taken_cols.count(qc_it->ix) == 0) continue;
				is_taken = true;
				break;
			}
			if (is_taken) {
				continue;
			}

			for (uint32 qc_ix = 0; qc_ix < qc.size(); qc_ix++) {
				node_type* node = root->new_node(qc[qc_ix]);
				cols[node->col.ix].push_back(node);
				rows[o_ix].push_back(node);
			}
		}
#else
		vector<column_type> qc;
		for (uint32 r = 0, row_ix = 0; r < n; r++) {
			for (uint32 f = 0; f < n; f++, row_ix++) {
				queen_columns(qc, rank_file_type(r, f), n);

				bool is_taken = false;
				for (vector<column_type>::iterator qc_it = qc.begin(); qc_it != qc.end(); ++qc_it) {
					if (taken_cols.count(qc_it->ix) == 0) continue;
					is_taken = true;
					break;
				}
				if (is_taken) continue;

				for (uint32 qc_ix = 0; qc_ix < qc.size(); qc_ix++) {
					node_type* node = root->new_node(qc[qc_ix]);
					cols[node->col.ix].push_back(node);
					rows[row_ix].push_back(node);
				}
			}
		}
#endif

		/**
		 * Column headers are created/linked.
		 *
		 * It looks like this:
		 * root -> col 0 -> col 1 -> ... -> root
		 */
		node_type* prev_header = root;
		uint32 primary_end = 2 * n;
		for (uint32 col_ix = 0; col_ix < num_cols; col_ix++) {
			if (taken_cols.count(col_ix)) continue;

			node_type* header = root->new_node(column_type(col_ix, col_ix < primary_end));
			header->c = header;
			header->l = prev_header;
			header->l->r = header;
			prev_header = header;
		}
		root->l = prev_header;
		prev_header->r = root;

		/**
		 * Convert the grid to our doubly-linked node lists.
		 */
		for (node_type* header = root->r; header != root; header = header->r) {
			node_type *back = header;
			uint32 size = 0;
			for (vector<node_type*>::iterator c_it = cols[header->col.ix].begin(); c_it != cols[header->col.ix].end(); ++c_it) {
				node_type* curr = *c_it;
				curr->c = header;
				curr->u = back;
				back->d = curr;
				back = curr;
				size++;
			}
			header->u = back;
			back->d = header;
			header->size = size;
		}
		for (uint32 row_ix = 0; row_ix < num_rows; row_ix++) {
			node_type *front = 0, *back = 0;

			for (vector<node_type*>::iterator r_it = rows[row_ix].begin(); r_it != rows[row_ix].end(); ++r_it) {
				node_type* curr = *r_it;

				if (back) {
					curr->l = back;
					back->r = curr;
				}
				if (front == 0) {
					front = curr;
				}
				back = curr;
			}
			if (back) {
				front->l = back;
				back->r = front;
			}
		}

		return root;
	}

	bool search(vector< vector<column_type> >& solution) {

		/**
		* Success: primary covered.
		*/
		bool all_covered = true;
		for (node_type* r = this->r; r != this; r = r->r) {
			if (!r->col.is_primary) continue;
			all_covered = false;
			break;
		}
		if (all_covered) return true;

		/**
		* Failure: column is empty but needs to be covered, no chance.
		*/
		for (node_type* r = this->r; r != this; r = r->r) {
			if (r->size > 0 || !r->col.is_primary) continue;
			return false;
		}

		/**
		* Use the shortest column to choose the next row (search heuristic).
		*/
		node_type* header = 0;
		uint32 min_size = 0xFFFFFFFFLL;
		for (node_type* j = this->r; j != this; j = j->r) {
			if (!j->col.is_primary) continue;
			if (j->size >= min_size) continue;
			header = j;
			min_size = header->size;
		}

		/**
		* Cover this column and all others that can be reached.
		* This removes all other associated rows because they
		* can no longer be reached from root header.
		*/
		vector<column_type> these_cols;
		these_cols.push_back(header->col);
		this->cover_column(header);
		for (node_type* r = header->d; r != header; r = r->d) {

			for (node_type* j = r->r; j != r; j = j->r) {
				these_cols.push_back(j->c->col);
				this->cover_column(j->c);
			}

			solution.push_back(these_cols);
			if (this->search(solution)) return true;
			solution.pop_back();

			for (node_type* j = r->l; j != r; j = j->l) {
				this->uncover_column(j->c);
				these_cols.pop_back();
			}
		}
		this->uncover_column(header);
		return false;
	}
};


int main(void) {
	ios::sync_with_stdio(false);

	for (;;) {
		uint32 n = 0; cin >> n;
		if (cin.eof() || n == 0) break;

		/**
		* Several queen placements are given; assumed to be good.
		*/
		vector< vector<column_type> > solution;
		set<uint32> taken_cols;
		for (uint32 rank_ix = 0; rank_ix < n; rank_ix++) {
			int32 file_ix = 0; cin >> file_ix;
			file_ix--;
			if (file_ix < 0) continue;
			vector<column_type> qc;
			queen_columns(qc, rank_file_type(rank_ix, file_ix), n);
			for (uint32 qc_ix = 0; qc_ix < qc.size(); qc_ix++) {
				taken_cols.insert(qc[qc_ix].ix);
			}
			solution.push_back(qc);
		}
		root_type* root = root_type::root(n, taken_cols, solution);

		/**
		* Solution is a a list of rows, each containing (distinct) columns.
		* These columns must be translated to rank/files and output in rank order.
		*/
		root->search(solution);
		if (solution.empty()) {
			cout << "No solution" << endl;
		}
		else {
			for (uint32 s_ix = 0; s_ix < solution.size(); s_ix++) {
				sort(solution[s_ix].begin(), solution[s_ix].end());
			}
			for (uint32 rank_ix = 0; rank_ix < n; rank_ix++) {
				for (uint32 s_ix = 0; s_ix < solution.size(); s_ix++) {
					if (solution[s_ix][0].ix != rank_ix) continue;
					uint32 file_ix = solution[s_ix][1].ix - n;
					cout << (file_ix + 1) << ' ';// 1-indexed
					break;
				}
			}
			cout << endl;
		}
	}

	return 0;
}

