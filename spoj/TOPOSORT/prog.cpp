#include <iostream>
#include <vector>
#include <stdio.h>
#include <set>
#include <queue>
#include <time.h>
#include <stdlib.h>

using namespace std;
typedef unsigned long uint32;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* TOPOSORT, Topological sort, directed graph, dependencies, adjacency lists, O(|V| + |E|)
* @see https://en.wikipedia.org/wiki/Topological_sorting
*/

struct node_t {
	node_t(): id(0), incoming(0) {}

	void direct_to(node_t* o) {
		outgoing.push_back(o);
		o->incoming++;
	}

	uint32 id;

	/**
	* Incoming: need degree only
	* Outgoing: need list for search
	*/
	uint32 incoming;
	vector<node_t*> outgoing;
};

/**
* Problem description wants lowest IDs first.
*/
struct less_node_id {
	bool operator()(const node_t* lhs, const node_t* rhs) {
		return (lhs->id > rhs->id);
	}
};

int main(void) {

	/**
	* Nodes [1, ..., num_nodes]
	*/
	uint32 num_nodes = 0, num_edges = 0;
	scanf("%d %d", &num_nodes, &num_edges);
	vector<node_t> nodes(num_nodes + 1);
	for (uint32 i = 1; i < nodes.size(); i++) {
		nodes[i].id = i;
	}
	for (uint32 i = 0; i < num_edges; i++) {
		uint32 id_from = 0, id_to = 0;
		scanf("%d %d", &id_from, &id_to);

		node_t *node_from = &nodes[id_from];
		node_t *node_to = &nodes[id_to];
		node_from->direct_to(node_to);
	}

	/**
	* Start with left-most (incoming-free) nodes.
	*/
	vector<node_t*> topog;
	priority_queue<node_t*, vector<node_t*>, less_node_id> priority;
	for (uint32 i = 1; i < nodes.size(); i++) {
		if (nodes[i].incoming) continue;
		priority.push(&nodes[i]);
	}

	/**
	* Search for nodes with no more prerequisites/incoming;
	* they are added to topological sort result.
	*/
	while (!priority.empty()) {
		node_t* from = priority.top();
		priority.pop();
		topog.push_back(from);
		for (uint32 i = 0; i < from->outgoing.size(); i++) {
			node_t* to = from->outgoing[i];
			to->incoming--;
			if (to->incoming > 0) continue;
			priority.push(to);
		}
	}

	/**
	* Incoming edge remains?  This means a node has an unmet prerequisite;
	* graph has a cycle thus topographical sort is impossible.
	*/
	bool has_cycle = false;
	for (uint32 i = 1; i < nodes.size(); i++) {
		if (nodes[i].incoming == 0) continue;
		has_cycle = true;
		break;
	}
	if (has_cycle) {
		printf("Sandro fails.\n");
	}
	else {
		for (uint32 i = 0; i < topog.size(); i++) {
			printf("%d ", topog[i]->id);
		}
		printf("\n");
	}

	return 0;
}
