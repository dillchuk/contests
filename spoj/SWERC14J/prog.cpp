#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <set>
#include <list>
#include <iterator>
#include <string>
#include <time.h>
#include <stdlib.h>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* 2D, Two dimensions, string matching, KMP, Aho-Corasick dictionary
*/

// http://www.cs.uku.fi/~kilpelai/BSA05/lectures/slides04.pdf

struct node_type {

	/**
	* Root constructor.
	*/
	node_type(string alphabet): alphabet(alphabet), root(this), fail(0) {
		this->ix = this->nodes.size();
		this->nodes.push_back(this);

		for (string::iterator a_it = this->alphabet.begin();
			a_it != this->alphabet.end(); ++a_it) {
			this->links[*a_it] = this;
		}
	}

	/**
	* Child constructor.
	*/
	node_type(node_type* parent): root(parent->root), fail(0) {
		this->ix = this->root->nodes.size();
		this->root->nodes.push_back(this);
	}

	node_type* follow(char c) {
		node_type* curr = this;
		while (curr->go(c) == 0) {
			curr = curr->fail;
		}
		return curr->go(c);
	}

	node_type* go(char c) {
		return this->links[c];
	}

	node_type* set_go(char c) {
		node_type* go = this->links[c];
		if (go && go->is_root()) {
			go = 0;
		}
		if (go == 0) {
			this->links[c] = new node_type(this);
		}
		return this->links[c];
	}

	/**
	* Not needed for this problem.
	*/
	void set_word_end(const string& word) {
		//this->words.insert(word);
	}

	bool is_root() const {
		return (this->root->nodes[0] == this);
	}

	uint32 ix;
	map<char, node_type*> links;
	//set<string> words;

	node_type* fail;

	node_type* root;

	/**
	* Root only.
	*/
	string alphabet;
	vector<node_type*> nodes;
};

void phase1_add_words(node_type* root, const vector<string>& words) {
	if (!root->is_root()) {
		cout << "Called `phase1` on non-root";
		exit(1);
	}

	for (vector<string>::const_iterator w_it = words.begin(); w_it != words.end(); ++w_it) {
		node_type* curr = root;
		for (string::const_iterator c_it = w_it->begin(); c_it != w_it->end(); ++c_it) {
			curr = curr->set_go(*c_it);
		}
		//curr->set_word_end(*w_it);
	}
}

void phase2_automatize(node_type* root) {
	if (!root->is_root()) {
		cout << "Called `phase2` on non-root";
		exit(1);
	}
	list<node_type*> queue;
	for (string::const_iterator a_it = root->alphabet.begin(); a_it != root->alphabet.end(); ++a_it) {
		node_type* q = root->go(*a_it);
		if (!q->is_root()) {
			q->fail = q->root;
			queue.push_back(q);
		}
	}
	while (!queue.empty()) {
		node_type* r = queue.front(); queue.pop_front();
		for (string::const_iterator a_it = root->alphabet.begin(); a_it != root->alphabet.end(); ++a_it) {
			node_type* u = r->go(*a_it);
			if (u != 0) {
				queue.push_back(u);
				u->fail = r->fail->follow(*a_it);
				//copy(u->fail->words.begin(), u->fail->words.end(), inserter(u->words, u->words.begin()));
			}
		}
	}
}

node_type* find_word(node_type* root, const string& word) {
	node_type* curr = root;
	for (string::const_iterator c_it = word.begin(); c_it != word.end(); ++c_it) {
		curr = curr->go(*c_it);
		if (curr == 0) return 0;
	}
	return curr;
}

vector<int32> kmp_table(vector<uint32> w) {

	/**
	* Allows daisy-chaining searches; creates a bigger table.
	*/
	w.push_back(0);

	vector<int32> t(max(static_cast<int32>(2), static_cast<int32>(w.size())));

	uint32 pos = 2;
	uint32 cand = 0;

	t[0] = -1;
	t[1] = 0;
	while (pos < w.size()) {
		if (w[pos - 1] == w[cand]) {
			cand++;
			t[pos] = cand;
			pos++;
		}
		else if (cand > 0) {
			cand = t[cand];
		}
		else {
			t[pos] = 0;
			pos++;
		}
	}
	return t;
}

/**
* If found, returns index.  If not, returns size of s (invalid index).
*/
uint32 kmp_search(const vector<uint32>& haystack, const vector<uint32>& needle, const vector<int32>& kmptab, uint32 h_ix = 0, uint32 n_ix = 0) {

	while (h_ix + n_ix < haystack.size()) {
		if (needle[n_ix] == haystack[h_ix + n_ix]) {
			if (n_ix == needle.size() - 1) return h_ix;
			n_ix++;
		}
		else {
			h_ix += n_ix - kmptab[n_ix];
			if (kmptab[n_ix] >= 0) {
				n_ix = kmptab[n_ix];
			}
			else {
				n_ix = 0;
			}
		}
	}

	/**
	* Not found, return string end.
	*/
	return haystack.size();
}

uint32 brute_force(const vector<string>& haystack_rows, const vector<string>& needle_rows) {
	uint32 count = 0;

	for (uint32 hrow_ix = 0; hrow_ix <= haystack_rows.size() - needle_rows.size(); hrow_ix++) {
		for (uint32 hcol_ix = 0; hcol_ix <= haystack_rows[hrow_ix].size() - needle_rows[0].size(); hcol_ix++) {

			bool failed = false;
			for (uint32 nrow_ix = 0; nrow_ix < needle_rows.size() && !failed; nrow_ix++) {
				for (uint32 ncol_ix = 0; ncol_ix < needle_rows[nrow_ix].size(); ncol_ix++) {
					uint32 n = needle_rows[nrow_ix][ncol_ix];
					uint32 h = haystack_rows[hrow_ix + nrow_ix][hcol_ix + ncol_ix];
					if (n == h) continue;

					failed = true;
					break;
				}
			}
			if (!failed) {
				count++;
			}
		}
	}
	return count;
}

#define GEN_INPUT
#define BRUTE_FORCE

int main(void) {
	ios::sync_with_stdio(false);
	srand(time(0));

#ifdef GEN_INPUT
	for (;;) {
#endif

		uint32 needle_h = 0, needle_w = 0;
		uint32 haystack_h = 0, haystack_w = 0;
#ifdef GEN_INPUT
		haystack_h = rand() % 1000 + 100;
		haystack_w = rand() % 1000 + 100;
		needle_h = rand() % 6 + 1;
		needle_w = rand() % 6 + 1;
#else
		cin >> needle_h >> needle_w >> haystack_h >> haystack_w;
#endif
		vector<string> needle_rows(needle_h);
		for (uint32 n_ix = 0; n_ix < needle_rows.size(); n_ix++) {
#ifdef GEN_INPUT
			for (uint32 i = 0; i < needle_w; i++) {
				needle_rows[n_ix] += (rand() % 2) ? 'x' : 'o';
			}
#else
			cin >> needle_rows[n_ix];
#endif
		}
		node_type* root = new node_type("xo");
		phase1_add_words(root, needle_rows);
		phase2_automatize(root);

		vector<string> haystack_rows(haystack_h);
		for (uint32 h_ix = 0; h_ix < haystack_rows.size(); h_ix++) {
#ifdef GEN_INPUT
			for (uint32 i = 0; i < haystack_w; i++) {
				haystack_rows[h_ix] += (rand() % 2) ? 'x' : 'o';
			}
#else
			cin >> haystack_rows[h_ix];
#endif
		}

		vector< vector<uint32> > haystack_state_rows(haystack_rows.size());
		for (uint32 h_ix = 0; h_ix < haystack_rows.size(); h_ix++) {
			node_type* q = root;
			for (string::iterator h_it = haystack_rows[h_ix].begin(); h_it != haystack_rows[h_ix].end(); ++h_it) {
				q = q->follow(*h_it);
				haystack_state_rows[h_ix].push_back(q->ix);
			}
		}

		vector<uint32> needle_search(needle_rows.size());
		for (uint32 i = 0; i < needle_rows.size(); i++) {
			needle_search[i] = find_word(root, needle_rows[i])->ix;
		}

		vector<int32> kmptab = kmp_table(needle_search);
		uint32 result = 0;
		for (uint32 col_ix = needle_rows[0].size() - 1 /*last column of target*/; col_ix < haystack_rows[0].size(); col_ix++) {
			vector<uint32> haystack_state;
			for (uint32 row_ix = 0; row_ix < haystack_state_rows.size(); row_ix++) {
				haystack_state.push_back(haystack_state_rows[row_ix][col_ix]);
			}

			/**
			* KMP search with linear resume.
			*/
			uint32 h_ix = 0, n_ix = 0;
			for (;;) {
				h_ix = kmp_search(haystack_state, needle_search, kmptab, h_ix, n_ix);
				if (h_ix == haystack_state.size()) break;

				n_ix = kmptab.back();
				h_ix += needle_search.size() - n_ix;


				result++;
			}
		}
		cout << result << endl;

#ifdef BRUTE_FORCE
		uint32 bf_result = brute_force(haystack_rows, needle_rows);
		cout << bf_result << " (brute force)" << endl;
		if (result != bf_result) {
			cout << "FOUND IT!!!" << endl;
		}
#endif
#ifdef GEN_INPUT
	}
#endif

	return 0;
}
