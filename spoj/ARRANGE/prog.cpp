/**
* http://www.spoj.com/problems/ARRANGE/
*
* Derek Illchuk dillchuk@gmail.com
*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef unsigned int uint32;
typedef int int32;

int32 main(void) {
	ios::sync_with_stdio(false);

	uint32 num_cases = 0; cin >> num_cases;
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {
		uint32 num_inputs = 0; cin >> num_inputs;

		uint32 inputs_1 = 0;
		vector<uint32> inputs;

		for (uint32 input_ix = 0; input_ix < num_inputs; input_ix++) {
			uint32 input = 0; cin >> input;

			if (input == 1) {
				inputs_1++;
			}
			else {
				inputs.push_back(input);
			}
		}
		sort(inputs.begin(), inputs.end());

		/**
		* Special case: {2, 3} must be reversed.
		* 3^2 GT 2^3
		* but 3^(2^4) LT 2^(3^4)
		*/
		if (inputs.size() == 2 && inputs[0] == 2 && inputs[1] == 3) {
			inputs[0] = 3;
			inputs[1] = 2;
		}

		/**
		* Ones must be kept at the back of the expression to prevent their nullifying effect.
		*/
		while (inputs_1--) {
			cout << "1 ";
		}
		for (
			vector<uint32>::reverse_iterator i_it = inputs.rbegin();
			i_it != inputs.rend(); ++i_it
			) {
			cout << *i_it << ' ';
		}
		cout << endl;
	}

	return 0;
}
