#include <iostream>
#include <vector>
#include <stdlib.h>
#include <limits.h>
#include <map>
#include <stdio.h>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* MST, minimum spanning tree, Prim's algorithm, pairing heap.
*
* @see https://en.wikipedia.org/wiki/Prim%27s_algorithm
*/

/**
* Standard graph node.
*/
struct node_t {
	node_t(): id(0) {}
	uint32 id;
	map<uint32, uint32> edges;
};

/**
* Pairing heap: a simplified fibonacci tree.
* @see https://en.wikipedia.org/wiki/Pairing_heap
*/
template<class T>
struct heap_t {
	heap_t(T item = 0): item(item), key(LLONG_MAX), child(0), sibling(0), parent(0), is_deleted(false) {}

	void invariant() const {
		if (this->is_deleted) {
			cout << "Heap node has been deleted and must be forgotten" << endl;
			exit(EXIT_FAILURE);
		}
	}

	heap_t<T> *child, *sibling, *parent;
	uint64 key;
	bool is_deleted;
	T item;
};

template<class T>
uint64 find_min(heap_t<T>* heap) {
	heap->invariant();
	return heap->key;
}

template<class T>
heap_t<T>* merge(heap_t<T>* heap1, heap_t<T>* heap2) {
	if (heap1) heap1->parent = heap1->sibling = 0;
	if (heap2) heap2->parent = heap2->sibling = 0;
	if (!heap1) return heap2;
	if (!heap2) return heap1;
	heap1->invariant();
	heap2->invariant();

	heap_t<T>* top = (heap1->key < heap2->key) ? heap1 : heap2;
	heap_t<T>* newchild = (top == heap1) ? heap2 : heap1;
	newchild->sibling = top->child;
	top->child = newchild;
	newchild->parent = top;
	return top;
}

template<class T>
heap_t<T>* insert(heap_t<T>* node, heap_t<T>* heap) {
	return merge(node, heap);
}

/**
* To initialize heap, link all heap nodes as siblings.
*/
template<class T>
heap_t<T>* merge_pairs(heap_t<T>* list) {
	typedef pair<heap_t<T>*, heap_t<T>*> heap_pair;
	vector<heap_pair> pairs;
	for (heap_t<T>* c = list; c; c = c->sibling->sibling) {
		pairs.push_back(make_pair(c, c->sibling));
		if (!c->sibling) break;
	}
	heap_t<T>* heap = 0;
	typedef typename vector<heap_pair>::reverse_iterator vp_rev_it;
	for (vp_rev_it p_it = pairs.rbegin(); p_it != pairs.rend(); ++p_it) {
		heap = merge(merge(p_it->first, p_it->second), heap);
	}
	return heap;
}

template<class T>
heap_t<T>* delete_min(heap_t<T>* heap) {
	heap->invariant();
	heap->is_deleted = true;
	heap_t<T>* new_heap = merge_pairs(heap->child);
	return new_heap;
}

template<class T>
inline void pop_from_parent(heap_t<T>* node) {
	heap_t<T>* parent = node->parent;
	if (!parent) return;
	if (parent->child == node) {
		parent->child = node->sibling;
	}
	else {
		heap_t<T>* left_sib = parent->child;
		for (; left_sib->sibling != node; left_sib = left_sib->sibling);
		left_sib->sibling = node->sibling;
	}
	node->sibling = node->parent = 0;
}

template<class T>
heap_t<T>* decrease_key(uint64 key, heap_t<T>* node) {
	node->invariant();
	if (key > node->key) {
		cout << "`decrease_key()` is not decreasing key!" << endl;
		exit(EXIT_FAILURE);
	}

	node->key = key;
	while (node->parent && node->parent->key > node->key) {
		heap_t<T>* par = node->parent;

		pop_from_parent(node);
		if (par->parent) {
			heap_t<T>* par_par = par->parent;
			pop_from_parent(par);
			node->sibling = par_par->child;
			par_par->child = node;
			node->parent = par_par;
		}
		par->sibling = node->child;
		node->child = par;
		par->parent = node;
	}

	heap_t<T>* root = node;
	for (; root->parent; root = root->parent);
	return root;
}

int main(void) {

	uint32 num_nodes = 0, num_edges = 0;
	scanf("%d %d", &num_nodes, &num_edges);

	vector<node_t> nodes(num_nodes + 1);
	for (uint32 id = 1; id < nodes.size(); id++) {
		nodes[id].id = id;
	}
	for (uint32 i = 0; i < num_edges; i++) {
		uint32 edge_a = 0, edge_b = 0, weight = 0;
		scanf("%d %d %d", &edge_a, &edge_b, &weight);
		nodes[edge_a].edges[edge_b] = weight;
		nodes[edge_b].edges[edge_a] = weight;
	}

	typedef heap_t<node_t*> hp_t;
	vector<hp_t> heaps(num_nodes + 1);
	for (uint32 id = 1; id < heaps.size(); id++) {
		heaps[id].item = &nodes[id];
	}
	for (uint32 id = 1; id < nodes.size() - 1; id++) {
		heaps[id].sibling = &heaps[id + 1];
	}
	hp_t* heap = merge_pairs(&heaps[1]);

	uint64 min_span = 0;
	heap->key = 0;
	while (heap) {

		/**
		* This node is cheapest; add it.
		*/
		min_span += heap->key;
		node_t* node = heap->item;
		heap = delete_min(heap);

		/**
		* From this node, any cheaper edges?
		*/
		for (map<uint32, uint32>::iterator e_it = node->edges.begin(); e_it != node->edges.end(); ++e_it) {
			uint32 to_id = e_it->first, new_key = e_it->second;
			hp_t* heap_to = &heaps[to_id];
			if (heap_to->is_deleted) continue;
			if (heap_to->key <= new_key) continue;
			heap = decrease_key(new_key, heap_to);
		}
	}
	cout << min_span << endl;

	return 0;
}

