#include <iostream>
#include <vector>
#include <math.h>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
* Quadratic formula, binary search.
*/

inline uint32 solve_produced(uint32 capability, uint32 time) {

	/**
	* x is units produced
	*
	* capability(x)(x+1)/2 <= time
	*
	* x^2 + x - 2*time/capability <= 0
	*
	* Result is floor(largest quadratic formula)
	*/
	return (-1 + sqrt(1 + 4 * 2.0 * time / capability)) / 2;
}

/**
* All units produced by n must be handled by m, so watch for it.
*/
#define N_TOO_LONG -1
inline int32 score(vector<uint32>& n_cap, vector<uint32>& m_cap, uint32 time_n, uint32 time_nm) {

	uint32 time_m = time_nm - time_n;
	uint32 units_m = 0;
	for (uint32 i = 0; i < m_cap.size(); i++) {
		units_m += solve_produced(m_cap[i], time_m);
	}
	uint32 units_n = 0;
	for (uint32 i = 0; i < n_cap.size(); i++) {
		units_n += solve_produced(n_cap[i], time_n);
		if (units_n > units_m) {
			return N_TOO_LONG; // short-circuit
		}
	}

	return units_n;
}

int main(void) {
	ios::sync_with_stdio(false);

	uint32 num_cases = 0; cin >> num_cases;
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {
		uint32 time = 0; cin >> time;
		uint32 n = 0, m = 0; cin >> n >> m;
		vector<uint32> n_cap(n), m_cap(m);
		for (uint32 i = 0; i < n; i++) cin >> n_cap[i];
		for (uint32 i = 0; i < m; i++) cin >> m_cap[i];

		/**
		* Max result increases right up until n produces too much.
		*/
		uint32 max_result = 0;
		uint32 begin = 0, end = time;
		while (begin < end) {
			uint32 mid = (begin + end) / 2;
			int32 result = score(n_cap, m_cap, mid, time);

			if (result == N_TOO_LONG) {
				end = mid;
				continue;
			}

			if (result > max_result) {
				max_result = result;
			}
			begin = mid + 1;
		}
		cout << max_result << endl;
	}

	return 0;
}
