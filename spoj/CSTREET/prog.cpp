#include <iostream>
#include <algorithm>
#include <vector>
#include <stdio.h>

using namespace std;
typedef unsigned long uint32;
typedef unsigned long long uint64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* MST, minimum spanning tree, Kruskal's algorithm.
* @see https://en.wikipedia.org/wiki/Kruskal%27s_algorithm
*/

struct quick_union {
	quick_union(): sets(1), size(1) {}

	void insert(uint32 a, uint32 b = 0) {
		if (b == 0) b = a;

		if (a == 0) {
			cout << "Items must have id > 0" << endl;
			exit(EXIT_FAILURE);
		}

		/**
		* Grow.
		*/
		uint32 max_id = max(max(a, b), static_cast<uint32>(this->sets.size() - 1));
		if (max_id >= this->sets.size()) {
			this->sets.resize(max_id + 1);
			this->size.resize(max_id + 1);
		}
		if (this->sets[a] == 0) this->sets[a] = a;
		if (this->sets[b] == 0) this->sets[b] = b;
		this->size[a] = max(this->size[a], static_cast<uint32>(1));
		this->size[b] = max(this->size[b], static_cast<uint32>(1));

		a = this->find(a);
		b = this->find(b);
		if (a == b) return;

		/**
		* Weighted union; keep tree balanced.
		*/
		if (this->size[a] < this->size[b]) {
			this->sets[a] = b;
			this->size[b] += this->size[a];
		}
		else {
			this->sets[b] = a;
			this->size[a] += this->size[b];
		}
	}

	uint32 find(uint32 id) {
		if (id >= this->sets.size()) {
			cout << "find() ID too large, ID not inserted yet" << endl;
			exit(EXIT_FAILURE);
		}
		for (; id != this->sets[id]; id = this->sets[id]) {
			this->sets[id] = this->sets[this->sets[id]]; // collapse tree a bit
		}
		if (id == 0) {
			cout << "find() ID not inserted yet" << endl;
			exit(EXIT_FAILURE);
		}
		return id;
	}

	vector<uint32> sets, size;
};

struct edge_t {
	edge_t(uint32 id1, uint32 id2, uint32 weight): id1(min(id1, id2)), id2(max(id1, id2)), weight(weight) {}
	uint32 id1, id2, weight;
};
bool operator<(const edge_t& lhs, const edge_t& rhs) {
	return (lhs.weight < rhs.weight);
}

int main(void) {

	uint32 num_cases = 0;
	scanf("%d", &num_cases);
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {
		uint32 price_per = 0, num_nodes = 0, num_edges = 0;
		scanf("%d %d %d", &price_per, &num_nodes, &num_edges);

		vector<edge_t> edges;
		for (uint32 i = 0; i < num_edges; i++) {
			uint32 edge_a = 0, edge_b = 0, weight = 0;
			scanf("%d %d %d", &edge_a, &edge_b, &weight);
			edges.push_back(edge_t(edge_a, edge_b, weight));
		}
		
		/**
		* Kruskal's algorithm
		* - for each edge by weight ascending
		* - if nodes are in same component, forget
		* - else take edge and link two components into one
		*/
		uint64 min_span = 0;
		sort(edges.begin(), edges.end());
		quick_union qu;
		for (uint32 i = 0; i < edges.size(); i++) {
			edge_t& edge = edges[i];
			qu.insert(edge.id1);
			qu.insert(edge.id2);
			if (qu.find(edge.id1) == qu.find(edge.id2)) continue;
			min_span += edge.weight;
			qu.insert(edge.id1, edge.id2);
		}

		cout << (min_span * price_per) << endl;
	}

	return 0;
}

