#include <iostream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <set>
#include <list>
#include <map>
#include <string>

using namespace std;

typedef unsigned int uint32;
typedef int int32;

struct solution_index {
	char problem;
	string team;
	solution_index(char problem, string team): problem(problem), team(team) {}

	bool operator<(solution_index other) const {
		if (problem != other.problem) {
			return (problem < other.problem);
		}
		return (team < other.team);
	}
};

struct solution_stats {
	bool accepted;
	uint32 submissions;
	uint32 time;

	solution_stats(): accepted(false), submissions(0), time(0) {}
};

struct input {
	uint32 time;
	string team;
	char problem;
	char result;

	input(): time(0), problem('0'), result('0') {}
};

struct accumulator {
	uint32 count;
	uint32 submissions;
	uint32 time;
	accumulator(): count(0), submissions(0), time(0) {}

	const void accumulate(const solution_stats &stats) {
		count++;
		submissions += stats.submissions;
		time += stats.time;
	}
};

struct output {
	uint32 accepted;
	double avg_submitted;
	double avg_time;

	output(): accepted(0), avg_submitted(0), avg_time(0) {}
};

int32 main(void) {
	uint32 num_cases = 0; cin >> num_cases;
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {
		uint32 num_inputs = 0; cin >> num_inputs;

		/**
		 * Input.
		 */
		vector<input> inputs;
		for (uint32 input_ix = 0; input_ix < num_inputs; input_ix++) {
			input curr;
			cin >> curr.time;
			cin >> curr.team;
			cin >> curr.problem;
			cin >> curr.result;
			inputs.push_back(curr);
		}

		/**
		 * Record solution stats.
		 */
		map<solution_index, solution_stats> solutions;
		for (vector<input>::iterator i_it = inputs.begin(); i_it != inputs.end(); ++i_it) {
			solution_index sol_ix = solution_index(i_it->problem, i_it->team);

			if (solutions[sol_ix].accepted) continue;
			solutions[sol_ix].submissions++;
			if (i_it->result != 'A') continue;
			solutions[sol_ix].accepted = true;
			solutions[sol_ix].time = i_it->time;
		}

		/**
		 * Remove team/problem stats for those not solved.
		 */
		vector<solution_index> sol_remove;
		for (
			map<solution_index, solution_stats>::iterator s_it = solutions.begin();
			s_it != solutions.end(); ++s_it
			) {
			if (!s_it->second.accepted) {
				sol_remove.push_back(s_it->first);
			}
		}
		for (vector<solution_index>::iterator sr_it = sol_remove.begin(); sr_it != sol_remove.end(); ++sr_it) {
			solutions.erase(*sr_it);
		}

		/**
		 * Calculate output.
		 */
		map<char, accumulator> accums;
		for (
			map<solution_index, solution_stats>::iterator s_it = solutions.begin();
			s_it != solutions.end(); ++s_it
			) {
			accums[s_it->first.problem].accumulate(s_it->second);
		}

		cout.setf(ios::fixed);
		cout.setf(ios::showpoint);
		cout.precision(2);
		for (char o_ix = 'A'; o_ix <= 'I'; o_ix++) {
			cout << o_ix << ' ' << accums[o_ix].count;
		
			double avg_submissions = 0, avg_time = 0;
			if (accums[o_ix].count) {
				avg_submissions = static_cast<double>(accums[o_ix].submissions) / accums[o_ix].count;
				avg_time = static_cast<double>(accums[o_ix].time) / accums[o_ix].count;
				cout << ' ' << avg_submissions << ' ' << avg_time;
			}

			cout << endl;
		}


	}
	return 0;
}
