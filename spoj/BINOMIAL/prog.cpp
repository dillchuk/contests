#include <iostream>
#include <vector>
#include <limits.h>
#include <algorithm>

using namespace std;
typedef unsigned long uint32;
typedef unsigned long long uint64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* BINOMIAL, binomial coefficent, n choose k.
*/

#define MAX_NK 1001

/**
* Coefficents get pretty big; limit according to problem description.
*/
#define COEF_SKIP LONG_MAX

int main(void) {

	vector< vector<uint64> > G(MAX_NK);
	for (uint32 i = 0; i < G.size(); i++) {
		G[i].resize(MAX_NK);
	}
	for (uint32 i = 0; i < G.size(); i++) {
		G[i][0] = G[i][i] = 1;
	}
	for (uint32 i = 1; i < G.size(); i++) {
		for (uint32 j = 1; j < i; j++) {
			G[i][j] = min(static_cast<uint64>(COEF_SKIP), G[i - 1][j - 1] + G[i - 1][j]);
		}
	}

	uint32 num_cases = 0; cin >> num_cases;
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {
		uint32 n = 0, k = 0; cin >> n >> k;
		cout << G[n][k] << endl;
	}
	return 0;
}

