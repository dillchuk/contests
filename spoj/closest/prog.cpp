#include <iostream>
#include <stdio.h>
#include <vector>
#include <cmath>
#include <algorithm>

//#define DEBUG

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* Given a large list of points, determine the closest triplet. O(n logn).
* For more information, see accompanying PDF in C:\dev\research\classic\closest-pair.
*/

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

struct point_type {
	point_type(): x(0), y(0) {}
	point_type(int32 x, int32 y): x(x), y(y) {}
	int32 x, y;
};

double point_distance(point_type a, point_type b) {
	return sqrt(
		pow(static_cast<double>(b.x - a.x), 2) + pow(static_cast<double>(b.y - a.y), 2)
	);
}

struct point_triplet {
	point_triplet(): distance(0xFFFFFFFFLL) {}

	point_triplet(point_type a, point_type b, point_type c) {
		this->points.push_back(a);
		this->points.push_back(b);
		this->points.push_back(c);
		this->distance = point_distance(a, b) + point_distance(b, c) + point_distance(c, a);
	}

	vector<point_type> points;

	/**
	* Distance of a lap through all points.
	*/
	double distance;
};

struct pt_x_sort {
	bool operator()(const point_type a, const point_type b) const {
		if (a.x < b.x) return true;
		if (a.x > b.x) return false;
		return (a.y < b.y);
	}
};
struct pt_y_sort {
	bool operator()(const point_type a, const point_type b) const {
		if (a.y < b.y) return true;
		if (a.y > b.y) return false;
		return (a.x < b.x);
	}
};

#define STRIP_SPAN 8 // given, from closest-pair algorithm
#define BRUTE_FORCE_THRESH 40 // trial & error for speed

/**
* Return closest triplet in the specified strip by directly searching neighbors.
*/
point_triplet closest_triplet_strip(vector<point_type>& S) {
	point_triplet result;

	/**
	* Imperative to use `STRIP_SPAN` optimization.
	*/
	sort(S.begin(), S.end(), pt_y_sort());

	for (uint32 i = 0; i < S.size(); i++) {

		/**
		* Search only within `STRIP_SPAN` neighbors; this is tricky so see accompanying PDF Fig 3b.
		* Because `S` is sorted by `y` and all are within 2 * best-distance (horizontally),
		* say S[0], S[1], S[10] is the answer, but contradiction: there's no way to squeeze
		* S[2...9] in between while having them not be the best-distance answer to
		* one of the previous divide & conquer calls.
		*/
		uint32 i_end = i + STRIP_SPAN;
		for (uint32 j = i + 1; j < i_end && j < S.size(); j++) {
			for (uint32 k = j + 1; k < i_end && k < S.size(); k++) {
				double curr_dist = point_distance(S[i], S[j]) + point_distance(S[j], S[k]) + point_distance(S[k], S[i]);
				if (curr_dist >= result.distance) continue;

				/**
				* Optimization: dont create `point_triplet` each loop to check distance; way too slow.
				*/
				result = point_triplet(S[i], S[j], S[k]);
			}
		}
	}
	return result;
}

/**
* Return closest triplet using divide & conquer.
*/
point_triplet closest_triplet(const vector<point_type>& P, uint32 begin = 0, uint32 end = 0xFFFFFFFFLL, bool brute_force = false) {
	end = min(end, static_cast<uint32>(P.size()));
	point_triplet result;

	/**
	* Base case: brute force.
	*/
	if (end - begin <= BRUTE_FORCE_THRESH || brute_force) {
		for (uint32 i = begin; i < end; i++) {
			for (uint32 j = i + 1; j < end; j++) {
				for (uint32 k = j + 1; k < end; k++) {
					double curr_dist = point_distance(P[i], P[j]) + point_distance(P[j], P[k]) + point_distance(P[k], P[i]);
					if (curr_dist >= result.distance) continue;

					/**
					* Optimization: dont create `point_triplet` each loop to check distance; way too slow.
					*/
					result = point_triplet(P[i], P[j], P[k]);
				}
			}
		}
		return result;
	}

	/**
	* Divide into left & right.
	*/
	uint32 begin2 = (begin + end) / 2;
	point_triplet left = closest_triplet(P, begin, begin2);
	point_triplet right = closest_triplet(P, begin2, end);
	result = (left.distance < right.distance) ? left : right;

	/**
	* Then search strip across this division.  Note that `strip_distance`
	* is half of `result.distance`: triplet distance/perimeter is greater
	* than any there-and-back between two points.
	*/
	double strip_distance = result.distance / 2;
	vector<point_type> S;
	int32 div_x = P[begin2].x;
	for (uint32 i = begin; i < end; i++) {
		if (abs(div_x - P[i].x) >= strip_distance) continue;
		S.push_back(P[i]);
	}
	if (S.size() >= 3) {
		point_triplet curr = closest_triplet_strip(S);
		if (curr.distance < result.distance) {
			result = curr;
		}
	}

	return result;
}

//#define GEN_INPUT 3000

int main() {

#ifdef GEN_INPUT
	{
		srand(time(0));
		uint32 n = GEN_INPUT;
		cout << n << endl;
		for (uint32 i = 0; i < n; i++) {
			cout << (rand() % 50000 - 50000) << ' ' << (rand() % 50000 - 50000) << ' ' << (rand() % 50000 - 50000) << endl;
		}
		cout << endl;
		return 0;
	}
#endif

	for (;;) {
		int32 n = 0; cin >> n;
		if (n < 3) break;

		vector<point_type> P(n);
		for (uint32 i = 0; i < n; i++) {
			cin >> P[i].x >> P[i].y;
		};
		sort(P.begin(), P.end(), pt_x_sort());
		point_triplet cp = closest_triplet(P);
		printf("%.3f\n", cp.distance);

#ifdef DEBUG
		bool brute_force = true;
		cp = closest_triplet(P, 0, 0xFFFFFFFFLL, brute_force);
		printf("%.3f\n", cp.distance);
#endif
	}

	return 0;
}
