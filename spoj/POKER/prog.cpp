/**
* http://www.spoj.com/problems/POKER/
*
* A simple poker hand evaluator.
*/

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <set>
#include <map>
#include <string>
#include <algorithm>

using namespace std;

typedef unsigned int uint32;
typedef int int32;

#define HAND_SIZE 5
#define ACE 1
#define LOW_CARD_DIGIT 2
#define HIGH_CARD_DIGIT 9

struct card_type {
	uint32 value;
	char suit;

	card_type(string rep) {

		map<char, uint32> value_map;
		value_map['T'] = 10;
		value_map['J'] = 11;
		value_map['Q'] = 12;
		value_map['K'] = 13;
		value_map['A'] = ACE;
		for (uint32 i = LOW_CARD_DIGIT; i <= HIGH_CARD_DIGIT; i++) {
			value_map['0' + i] = i;
		}
		value = value_map[rep[0]];
		suit = rep[1];
	}

	bool operator<(const card_type& other) const {
		if (value < other.value) return true;
		if (value > other.value) return false;
		return (suit < other.suit);
	}
};

bool is_flush(vector<card_type> cards) {
	char suit = cards[0].suit;
	for (vector<card_type>::iterator c_it = cards.begin(); c_it != cards.end(); c_it++) {
		if (c_it->suit != suit) {
			return false;
		}
	}
	return true;
}

bool is_straight_simple(vector<card_type> cards) {
	uint32 start = cards[0].value;
	for (uint32 card_ix = 1; card_ix < cards.size(); card_ix++) {
		if (cards[card_ix].value != start + card_ix) {
			return false;
		}
	}
	return true;
}

bool is_straight_high(vector<card_type> cards) {
	if (cards[0].value != ACE || cards[1].value != 10) {
		return false;
	}
	uint32 start = cards[1].value;
	for (uint32 card_ix = 2; card_ix < cards.size(); card_ix++) {
		if (cards[card_ix].value != start + card_ix - 1) {
			return false;
		}
	}
	return true;
}

vector<uint32> count_matches(vector<card_type> cards) {
	map<uint32, uint32> counts;
	for (vector<card_type>::iterator c_it = cards.begin(); c_it != cards.end(); ++c_it) {
		counts[c_it->value]++;
	}
	vector<uint32> summary;
	for (map<uint32, uint32>::iterator c_it = counts.begin(); c_it != counts.end(); ++c_it) {
		if (c_it->second <= 1) continue;
		summary.push_back(c_it->second);
	}
	sort(summary.rbegin(), summary.rend());
	return summary;
}

string get_result(bool flush, bool straight_high, bool straight_simple, const vector<uint32> &matches) {
	if (flush) {
		if (straight_high) {
			return "royal flush";
		}
		if (straight_simple) {
			return "straight flush";
		}
		return "flush";
	}
	if (straight_high || straight_simple) {
		return "straight";
	}
	if (matches.size() == 2) {
		if (matches[0] == 3) {
			return "full house";
		}
		return "two pairs";
	}
	if (matches.size() == 1) {
		if (matches[0] == 3) {
			return "three of a kind";
		}
		if (matches[0] == 4) {
			return "four of a kind";
		}
		return "pair";
	}
	return "high card";
}

int32 main(void) {
	uint32 num_inputs = 0; cin >> num_inputs;
	for (uint32 input_ix = 0; input_ix < num_inputs; input_ix++) {
		vector<card_type> cards;
		for (uint32 card_ix = 0; card_ix < HAND_SIZE; card_ix++) {
			string card_input; cin >> card_input;
			cards.push_back(card_type(card_input));
		}
		sort(cards.begin(), cards.end());

		cout << get_result(
			is_flush(cards), is_straight_high(cards), is_straight_simple(cards), count_matches(cards)
		) << endl;
	}
	return 0;
}
