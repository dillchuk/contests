#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <cmath>
#include <ctime>
#include <stdlib.h>
#include <bitset>
#include <algorithm>
#include <numeric>
#include <list>
#include <set>
#include <queue>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
* Dynamic Programming, Subset-Sum Difference, (Equal-size, half) Partition Problem.
*
* GIVES WRONG ANSWER!
*/

uint32 gcd(uint32 a, uint32 b) {
	while (b != 0) {
		uint64 t = b;
		b = a % t;
		a = t;
	}
	return a;
}

//#define DEBUG
#define SAMPLE_TRIES 20
#define SAMPLE_SIZE 10
#define MAX_W 10000LL
#define MAX_N 2000LL
#define MAX_N_HALF (MAX_N/2)

struct search_type {
	search_type(uint32 v_ix, uint32 balance): v_ix(v_ix), balance(balance) {}
	uint32 v_ix, balance;
};
bool operator<(const search_type& lhs, const search_type& rhs) {
	return (lhs.v_ix < rhs.v_ix);
}

/*
* If one value overpowers all others, solution is:
* {overpowering & smallest} {remaining}
*/
#define NO_FREE_LUNCH -1
int32 free_lunch(vector<uint32>/*COPY*/ vegs) {
	sort(vegs.rbegin(), vegs.rend());

	if (vegs.size() > 2) {
		uint32 op_begin = vegs.size() / 2 + 1;

		/**
		* underpower =   [1,...,vegs/2]
		* overpower = [0]              [vegs/2+1,...,end]
		*/
		uint32 overpower = vegs[0] + accumulate(vegs.begin() + op_begin, vegs.end(), 0);
		uint32 underpower = accumulate(vegs.begin() + 1, vegs.begin() + op_begin, 0);
		if (overpower >= underpower) {
			return overpower - underpower;
		}
	}
	return NO_FREE_LUNCH;
}

/**
* 1. Those congruent with dominant mod/offset first.
* 2. Then largest to smallest.
*/
struct dominant_mod_less {
	dominant_mod_less(uint32 mod, uint32 offset): mod(mod), offset(offset) {}

	bool operator()(uint32 lhs, uint32 rhs) {
		bool aligned_l = ((lhs - this->offset) % this->mod == 0);
		bool aligned_r = ((rhs - this->offset) % this->mod == 0);
		if (aligned_l && !aligned_r) return true;
		if (!aligned_l && aligned_r) return false;

		return (lhs > rhs);
	}

	uint32 mod, offset;
};

struct searcher_type {
	searcher_type(vector<uint32>& vegs): vegs(vegs), target(0) {
		this->calc_target();
		this->sort_by_dominant();
		this->init_weights();
	}

	uint32 search() {

		int32 free = free_lunch(vegs);
		if (free > NO_FREE_LUNCH) {
#ifdef DEBUG
			cout << "Used greedy solution" << endl;
#endif
			return free;
		}

		return search_loop();
	}

	uint32 search_loop() {
		uint32 best = 0xFFFFFFFFLL;

		vector<uint32> vmax = vector<uint32>(this->vegs.size() + 1);
		for (int32 v_ix = this->vegs.size() - 1; v_ix >= 0; v_ix--) {
			vmax[v_ix] = vmax[v_ix + 1] + this->vegs[v_ix];
		}

		vector< set<uint32> > hits(this->vegs.size());

		priority_queue<search_type> queue;
		queue.push(search_type(0, 0));
		while (!queue.empty()) {

			/**
			* If we hit target + 1, we'll never hit target.  So take it.
			*/
			if (best <= this->target + 1) break;

			search_type search = queue.top(); queue.pop();
			if (search.v_ix == this->vegs.size()) {
				if (search.balance < best) {
					best = search.balance;
#ifdef DEBUG
					cout << "New best: " << best << endl;
#endif
				}
				continue;
			}

#ifdef DEBUG
			if (rand() % 50000 == 0) {
				cout << "q: " << queue.size() << endl;
				cout << "v: " << search.v_ix << endl;
				cout << "b: " << search.balance << endl;
				cout << "t: " << this->target << endl;
				cout << "beat: " << best << endl << endl;
			}
#endif

			if (hits[search.v_ix].count(search.balance)) continue;
			hits[search.v_ix].insert(search.balance);

			/**
			* No way to beat the best; drop it.
			*/
			if (search.balance >= best + vmax[search.v_ix]) continue;

			uint32 diff_lt = abs(static_cast<int32>(search.balance) - static_cast<int32>(this->vegs[search.v_ix]));
			uint32 diff_gt = search.balance + this->vegs[search.v_ix];
			queue.push(search_type(search.v_ix + 1, diff_lt));
			queue.push(search_type(search.v_ix + 1, diff_gt));
		}
		return best;
	}

	/**
	* We can add any X to all vegs without affecting the final answer.
	* This is because vegs are split into equal counts so this factor perfectly balances.
	*
	* N/2*X - N/2*X = 0
	*/
	void init_weights() {

		/**
		* Low hanging fruit.
		*/
		uint32 min = *min_element(this->vegs.begin(), this->vegs.end());
		for (uint32 v_ix = 0; v_ix < this->vegs.size(); v_ix++) {
			this->vegs[v_ix] -= min;
		}

		/**
		* For each, add "punitive weight"
		* that cancels itself out when set is split equally in half.
		* Purpose: all non-equal splits yield terrible answers
		*/
		uint32 punitive = accumulate(this->vegs.begin(), this->vegs.end(), 0);
		for (uint32 v_ix = 0; v_ix < this->vegs.size(); v_ix++) {
			this->vegs[v_ix] += punitive;
		}
	}

	/**
	* Look at the differences between all vegs, and see if there's
	* a dominant factor.  That factor may stymie the search,
	* putting things into a rut that keeps returning result * factor.
	* Then put *unaffected* at the tail-end where they're the most helpful.
	*/
	void sort_by_dominant() {
		map< pair<uint32, uint32>, uint32> counts; // <mod, offset> => count

		/**
		* Use random sampling.
		*/
		for (uint32 try_ix = 0; try_ix < SAMPLE_TRIES; try_ix++) {
			vector<uint32> samples;
			uint32 min_veg = 0xFFFFFFFFLL;
			for (uint32 i = 0; i < SAMPLE_SIZE; i++) {
				uint32 a = this->vegs[rand() % this->vegs.size()];
				uint32 b = this->vegs[rand() % this->vegs.size()];
				uint32 diff = (a > b) ? a - b : b - a;
				if (diff == 0) continue;

				samples.push_back(diff);
				min_veg = min(min(min_veg, a), b);
			}
			if (samples.empty()) continue;

			uint32 gcd_all = samples[0];
			for (uint32 i = 0; i < samples.size(); i++) {
				gcd_all = gcd(gcd_all, samples[i]);
			}

			counts[make_pair(gcd_all, min_veg % gcd_all)]++;
		}

		pair<uint32, uint32> dominant = make_pair(1, 0);
		map< pair<uint32, uint32>, uint32 >::iterator max_it = counts.begin();
		for (map< pair<uint32, uint32>, uint32 >::iterator c_it = counts.begin(); c_it != counts.end(); ++c_it) {
			if (c_it->second <= max_it->second) continue;
			max_it = c_it;
		}
		if (max_it != counts.end()) {
			dominant = max_it->first;
		}
		sort(this->vegs.begin(), this->vegs.end(), dominant_mod_less(dominant.first, dominant.second));
	}

	/**
	* Starting with 0 mod <smallest non-zero>, calculate congruences all down the line;
	* the smallest becomes our search target.
	*/
	void calc_target() {
		sort(this->vegs.begin(), this->vegs.end());

		uint32 v_ix = 0;
		uint32 mod = 0;
		for (v_ix = 0; v_ix < vegs.size() && mod == 0; v_ix++) {
			if (this->vegs[v_ix] == 0) continue;
			mod = this->vegs[v_ix];
		}
		if (mod == 0) { // shouldn't happen, but will be handled easily
			this->target = 0;
			return;
		}

		vector< set<uint32> > congruences(2); // prev, curr
		uint32 answer_ix = 0;
		for (; v_ix < vegs.size(); v_ix++) {
			uint32 curr_ix = answer_ix = v_ix % 2;
			uint32 prev_ix = curr_ix ^ 1;

			if (congruences[prev_ix].empty()) { // initial
				congruences[prev_ix].insert(0);
			}
			congruences[curr_ix].clear();
			for (set<uint32>::iterator c_it = congruences[prev_ix].begin();
				c_it != congruences[prev_ix].end(); ++c_it) {
				uint32 veg = this->vegs[v_ix];
				int32 a = static_cast<int32>(*c_it) - this->vegs[v_ix];
				while (a < 0) a += mod;
				a %= mod;
				uint32 b = (*c_it + this->vegs[v_ix]) % mod;
				congruences[curr_ix].insert(a);
				congruences[curr_ix].insert(b);
			}

			/**
			* All congruences have been found thus none can be taken away.
			*/
			if (congruences[curr_ix].size() == mod) break;
		}
		this->target = *congruences[answer_ix].begin();
	}

	vector<uint32>& vegs;
	uint32 target;
};

//#define GEN_INPUT MAX_N_HALF
//#define INLINE_INPUT

int main(void) {
	srand(time(0));

#ifdef GEN_INPUT
	{

		uint32 num_cases = 1;
		cout << num_cases << endl;
		for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {
			cout << GEN_INPUT << endl;
			for (uint32 i = 0; i < 2 * GEN_INPUT - 1; i++) {
				if (i == 0) cout << 10 << endl;
				else cout << 5 << endl;
			}
			cout << endl;
		}
		return 0;
	}
#endif

	ios::sync_with_stdio(false);

	uint32 num_cases = 0;
#ifdef INLINE_INPUT
	num_cases = 1000;
#else
	cin >> num_cases;
#endif
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {
		uint32 result = 0;

		uint32 half_n = 0;
#ifdef INLINE_INPUT
		half_n = rand() % MAX_N_HALF + 1;
#else
		cin >> half_n;
#endif
		vector<uint32> vegs(half_n * 2);
		for (uint32 v_ix = 0; v_ix < vegs.size(); v_ix++) {
#ifdef INLINE_INPUT
			vegs[v_ix] = rand() % MAX_W + 1;
#else
			cin >> vegs[v_ix];
#endif
			if (vegs[v_ix] > MAX_W) {
				cout << "MAX_W too small";
				exit(1);
			}
		}

		searcher_type searcher(vegs);
		result = searcher.search();

#ifdef DEBUG
		cout << "Result: ";
#endif
		cout << result << endl;
	}

	return 0;
}

