#include <iostream>
#include <vector>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* Dynamic programing.
*
* How many distinct ways can you build a 4xN wall with 4x1 bricks?
*/

/**
* Must be able to check curr_ix - 4.
*/
#define KEEP_STATES 5

int main(void) {
	uint32 num_cases = 0; cin >> num_cases;
	for (uint32 c_ix = 0; c_ix < num_cases; c_ix++) {
		uint32 n = 0; cin >> n;

		vector<uint64> DP(KEEP_STATES, 1);

		uint64 count = 0;
		for (uint32 i = 0; i < n; i++) {
			uint32 horiz_ix = i % KEEP_STATES;
			uint32 prev_ix = (i + 3) % KEEP_STATES;
			uint32 curr_ix = (i + 4) % KEEP_STATES;

			/**
			* Vertical case: vertical brick appended to previous.
			*/
			DP[curr_ix] = DP[prev_ix];

			/**
			* Horizontal case: horizontal brick appended way back.
			*/
			if (i >= 3) {
				DP[curr_ix] += DP[horiz_ix];
			}

			count = DP[curr_ix];
		}
		cout << count << endl;
	}

	return 0;
}

