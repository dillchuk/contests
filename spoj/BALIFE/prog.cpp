#include <iostream>
#include <vector>
#include <stdlib.h>

using namespace std;

typedef unsigned int uint32;
typedef int int32;

int32 main(void) {
	for (;;) {

		/**
		 * Input.
		 */
		int32 num_procs = 0;
		cin >> num_procs;
		if (num_procs < 1) {
			break;
		}
		uint32 num_jobs = 0;
		vector<int32> jobs(num_procs);
		for (uint32 proc_ix = 0; proc_ix < num_procs; proc_ix++) {
			cin >> jobs[proc_ix];
			num_jobs += jobs[proc_ix];
		}

		/**
		 * Can we rebalance?
		 */
		bool can_rebalance = (num_jobs % num_procs == 0);
		if (!can_rebalance) {
			cout << -1 << endl;
			continue;
		}

		/**
		 * OK, see what needs rebalancing.
		 */
		uint32 load = num_jobs / num_procs;
		int32 delta = 0, max_delta = 0;
		for (uint32 proc_ix = 0; proc_ix < jobs.size(); proc_ix++) {
			delta += jobs[proc_ix] - load;

			if (abs(delta) > max_delta) {
				max_delta = abs(delta);
			}
		}
		cout << max_delta << endl;
	}

	return 0;
}

