#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

#define MAX_N 100000

struct rectangle_type {
	rectangle_type(uint32 id, int32 x1, int32 y1, int32 x2, int32 y2): id(id) {
		this->xl = min(x1, x2);
		this->xh = max(x1, x2);
		this->yl = min(y1, y2);
		this->yh = max(y1, y2);
		if (this->xl == this->xh) throw "rectangle_type() parameter error";
		if (this->yl == this->yh) throw "rectangle_type() parameter error";
	}

	/**
	* Coords low/high.
	*/
	int32 xl, xh;
	int32 yl, yh;

	uint32 id;
};

/**
* Sort rectangles initially by `yl`; imperative for working of `stab_y`.
*/
struct rectangle_sort_yl {
	bool operator()(const rectangle_type* a, const rectangle_type* b) {
		if (a->yl < b->yl) return true;
		if (a->yl > b->yl) return false;
		return (a < b);
	}
};

/**
* Project a light from above the two rectangles and see how long the shadow is.
* If it's longer than their combined widths, they don't intersect.
* Do the same from the side -- longer than combined heights, they don't intersect.
* Else they do.
*/
bool intersects(const rectangle_type& a, const rectangle_type& b) {
	uint32 x_proj = max(a.xh, b.xh) - min(a.xl, b.xl);
	uint32 y_proj = max(a.yh, b.yh) - min(a.yl, b.yl);
	uint32 x_sum = a.xh - a.xl + b.xh - b.xl;
	uint32 y_sum = a.yh - a.yl + b.yh - b.yl;
	return (x_proj < x_sum && y_proj < y_sum);
}

void report_pair(rectangle_type* a, rectangle_type* b) {
	// Intersection a & b
}

void stab_y(const vector<rectangle_type*>& a, const vector<rectangle_type*>& b) {

	/**
	* Ends of arrays are marked with a null pointer.
	*/
	uint32 i = 0, j = 0;
	while (a[i] != 0 && b[j] != 0) {

		if (a[i]->yl < b[j]->yl) {
			uint32 k = j;
			while (b[k] != 0 && b[k]->yl < a[i]->yh) {
				report_pair(a[i], b[k]);
				k++;
			}
			i++;
		}
		else {
			uint32 k = i;
			while (a[k] != 0 && a[k]->yl < b[j]->yh) {
				report_pair(b[j], a[k]);
				k++;
			}
			j++;
		}
	}
}

#define ONLY_IN_1ST(r, x1_max) (r->xh <= x1_max)
#define ONLY_IN_2ND(r, x2_min) (r->xl >= x2_min)
#define IN_1ST_OVER_2ND(r, x1_max, x2_max) (r->xl <= x1_max && r->xh > x2_max)
#define IN_2ND_OVER_1ST(r, x2_min, x1_min) (r->xh >= x2_min && r->xl < x1_min)
#define IN_BOTH(r, x1_max, x2_min) (r->xl <= x1_max && r->xh >= x2_min)

/**
* Optimization: allocate these temporary arrays once.
*/
vector<rectangle_type*> s1_only(MAX_N + 1), s2_only(MAX_N + 1), s1_jump_2(MAX_N + 1), s2_jump_1(MAX_N + 1);

void detect(vector<rectangle_type*>& rs, const vector<int32>& xs, uint32 begin = 0, uint32 end = -1) {
	end = min(end, static_cast<uint32>(xs.size()));
	int32 size = end - begin;
	if (size < 2) return;
	uint32 begin2 = (begin + end) / 2;

	int32 x1_min = xs[begin], x1_max = xs[begin2 - 1];
	int32 x2_min = xs[begin2], x2_max = xs[end - 1];

	uint32 s1o = 0, s2o = 0, s1j2 = 0, s2j1 = 0;
	for (uint32 i = 0; i < rs.size(); i++) {
		rectangle_type* rect = rs[i];

		if (ONLY_IN_1ST(rect, x1_max)) {
			s1_only[s1o++] = rect;
		}
		else if (ONLY_IN_2ND(rect, x2_min)) {
			s2_only[s2o++] = rect;
		}
		else if (IN_1ST_OVER_2ND(rect, x1_max, x2_max)) {
			s1_jump_2[s1j2++] = rect;
		}
		else if (IN_2ND_OVER_1ST(rect, x2_min, x1_min)) {
			s2_jump_1[s2j1++] = rect;
		}
	}
	s1_only[s1o] = s2_only[s2o] = s1_jump_2[s1j2] = s2_jump_1[s2j1] = 0; // sentinel end-of-array
	stab_y(s1_jump_2, s2_only);
	stab_y(s2_jump_1, s1_only);
	stab_y(s1_jump_2, s2_jump_1);

	vector<rectangle_type*> r1, r2;
	for (uint32 i = 0; i < rs.size(); i++) {
		rectangle_type* rect = rs[i];

		if (IN_BOTH(rect, x1_max, x2_min) || ONLY_IN_1ST(rect, x1_max) || IN_1ST_OVER_2ND(rect, x1_max, x2_max)) {
			r1.push_back(rect);
		}
		if (IN_BOTH(rect, x1_max, x2_min) || ONLY_IN_2ND(rect, x2_min) || IN_2ND_OVER_1ST(rect, x2_min, x1_min)) {
			r2.push_back(rect);
		}
	}
	detect(r1, xs, begin, begin2);
	detect(r2, xs, begin2, end);
}

int main(void) {
	ios::sync_with_stdio(false);

	/**
	* Input:
	* num_rectangles
	* x1 y1 x2 y2 (for each rectangle)
	*/
	uint32 n = 0; cin >> n;

	vector<rectangle_type> rectangles;
	for (uint32 i = 1; i <= n; i++) {
		int32 x1 = 0, y1 = 0, x2 = 0, y2 = 0;
		cin >> x1 >> y1 >> x2 >> y2;
		rectangles.push_back(rectangle_type(i, x1, y1, x2, y2));
	}

	vector<int32> xs;
	vector<rectangle_type*> rs;
	for (uint32 i = 0; i < rectangles.size(); i++) {
		xs.push_back(rectangles[i].xl);
		xs.push_back(rectangles[i].xh);
		rs.push_back(&rectangles[i]);
	}
	sort(rs.begin(), rs.end(), rectangle_sort_yl());
	sort(xs.begin(), xs.end());
	detect(rs, xs);

	return 0;
}
