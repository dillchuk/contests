#include <iostream>
#include <vector>
#include <algorithm>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* Binary Indexed Tree, Fenwick Tree, query running totals.
*
* @see http://community.topcoder.com/tc?module=Static&d1=tutorials&d2=binaryIndexedTrees
* @see https://en.wikipedia.org/wiki/Fenwick_tree
*/

//#define BRUTE_FORCE
#ifdef BRUTE_FORCE
struct brute_force {

	brute_force(uint32 n): tree(n + 1) {}

	int64 read(uint32 id) {
		return this->tree[id];
	}

	void set(uint32 id, int64 val) {
		this->tree[id] = val;
	}

	void set_all(int64 val) {
		fill(this->tree.begin() + 1, this->tree.end(), val);
	}

	void add(uint32 ida, uint32 idb, int64 val) {
		for (uint32 id = ida; id <= idb; id++) {
			this->tree[id] += val;
		}
	}

	vector<int64> tree;
};
#endif

/**
* Suitable for:
* - adding values to all indices across a range O(lg n)
* - querying individual values O(lg n)
*
* Not suitable for:
* - querying sum totals across a range
*
* N.B. Indexed from [1, n].
*/
struct binary_indexed_tree {

	binary_indexed_tree(uint32 n): atom(n + 1), range(n + 1) {}

#ifdef BRUTE_FORCE
	void dump(vector<int64>& dump) const {
		for (uint32 id = 1; id < this->atom.size(); id++) {
			dump[id] = this->get(id);
		}
	}
#endif

	/**
	* Returns the value of the last 1-bit.
	* e.g. 1011000 => 1000
	* (And therefore 1011000 is responsible for [1011000 - 1000 + 1, 1011000])
	*/
#define RESPONSIBILITY(id) (id & -static_cast<int32>(id))

	int64 get(uint32 id) const {
		int64 sum = this->atom[id];
		for (uint32 i = id; i < this->range.size(); i += RESPONSIBILITY(i)) {
			sum += this->range[i];
		}
		return sum;
	}

	void set(uint32 id, int64 val) {
		int64 orig = this->get(id);
		this->atom[id] += val - orig;
	}

	void add(uint32 ida, uint32 idb, int64 val) {
		uint32 i = ida;
		while (i <= idb) {
			bool is_range = false;
			for (;;) {

				/**
				* Looking at 1001, the digits above (rightmost 1) responsibility are 0.
				* That's good and means those ranges are still open and available.
				*
				* Looking at 1011, the digit directly above responsiblility is a 1.
				* That's no good: that range has closed.
				*/
				uint32 ri = RESPONSIBILITY(i);
				bool bigger_range_open = (RESPONSIBILITY(i + ri) >> 1 == ri);
				bool too_low = (i - ri + 1 < ida);
				bool too_high = (i + ri > idb);
				if (!bigger_range_open || too_low || too_high) break;

				i += ri;
				is_range = true;
			}
			vector<int64>& to = is_range ? this->range : this->atom;
			to[i] += val;
			i++;
		}
	}

	vector<int64> atom, range;
};

/**
* Indexed from [1, n].
*/
struct cumulative_indexed_tree {

	cumulative_indexed_tree(uint32 n): tree(n + 1), lazy(n + 1) {}

	// TODO
	int32 sum_to(uint32 id) const {
		int64 sum = 0;
		for (int32 i = id; i; i -= RESPONSIBILITY(i)) {
			sum += this->tree[id];
		}
		return sum;
	}

	int64 read(uint32 id) const {
		int64 sum = this->tree[id];
		if (id > 0) {
			uint32 z = id - RESPONSIBILITY(id);
			id--;
			while (id != z) {
				sum -= this->tree[id];
				id -= RESPONSIBILITY(id);
			}
		}
		for (uint32 i = id; i < this->lazy.size(); i += RESPONSIBILITY(i)) {
			sum += lazy[i];
		}
		return sum;
	}

	void set(uint32 id, int64 val) {
		this->add(id, val - this->read(id));
	}

	void set_all(int64 val) {
		for (uint32 id = 1; id < this->tree.size(); id++) {
			this->tree[id] = RESPONSIBILITY(id) * val;
		}
#ifdef DEBUG
		this->print();
#endif
	}

	inline void add(uint32 ida, uint32 idb, int64 val) {
		uint32 i = ida;
		while (i <= idb) {
			int32 z = i;
			int32 rz = RESPONSIBILITY(z);
			while (rz == ((rz << 1) - rz) && (z + rz <= idb)) {
				z += rz;
				rz = RESPONSIBILITY(z);
			}
			if (z == i) {
				this->add(z, val);
			}
			else {
				this->lazy[z] += val;
			}
			i = z + 1;
		}
#ifdef DEBUG
		this->print();
#endif
	}

	void add(uint32 id, int64 val) {
		for (uint32 i = id; i < this->tree.size(); i += RESPONSIBILITY(i)) {
			this->tree[i] += val;
		}
#ifdef DEBUG
		this->print();
#endif
	}

#ifdef DEBUG
	void print() const {
		for (uint32 id = 1; id < this->tree.size(); id++) {
			cout << this->read(id) << " ";
		}
		cout << endl;
	}
#endif

	vector<int64> tree, lazy;
};

//#define GEN_INPUT
#ifdef GEN_INPUT
//#define NO_OUTPUT
#endif
#ifdef NO_OUTPUT
#undef BRUTE_FORCE
#endif
//#define TINKER

int main(void) {

#ifdef TINKER

	binary_indexed_tree bit(16);
	for (uint32 i = 1; i <= 16; i++) {
		bit.add(1, i, 1000000000);
	}
	bit.set(4, 0);
	for (uint32 i = 1; i <= 16; i++) {
		bit.add(1, i, 1000000000);
	}
	for (uint32 i = 1; i <= 16; i++) {
		printf("%lld ", bit.get(i));
	}
	return 0;
#endif

	ios::sync_with_stdio(false);
#ifdef GEN_INPUT
#define RAND_VAL() (rand() % 1000000001)
	srand(time(0));
#endif

	uint32 num_cases = 0;
#ifdef GEN_INPUT
	num_cases = 10000;
	printf("%d\n", num_cases);
#else
	scanf("%d", &num_cases);
#endif
	for (uint32 case_id = 1; case_id <= num_cases; case_id++) {
#ifndef NO_OUTPUT
		printf("Case #%d\n", case_id);
#endif

		uint32 n = 0, num_query = 0;
		int64 init = 0;
#ifdef GEN_INPUT
		n = num_query = rand() % 100 + 1;
		init = RAND_VAL();
		printf("%d %d %lld\n", n, num_query, init);
#else
		scanf("%d%d%lld", &n, &num_query, &init);
#endif
#ifdef BRUTE_FORCE
		brute_force bf(n);
		vector<int64> dump(bf.tree.size());
		bf.set_all(init);
#endif
		binary_indexed_tree bit(n);
		bit.add(1, n, init);

		for (uint32 q = 0; q < num_query; q++) {
			char command[10] = "";
			uint32 ida = 0, idb = 0;
			int64 val = 0;

#ifdef GEN_INPUT
			uint32 comm = rand() % 6;
			if (q % 2 == 1) {
				comm = 5;
			}
			do {
				ida = rand() % n + 1;
				idb = rand() % n + 1;
			} while (ida > idb);
			val = RAND_VAL();

			if (comm == 0) {
				printf("add %d %d %lld\n", ida, idb, val);
#else
			scanf("%s", command);
			if (strcmp(command, "add") == 0) {
				scanf("%d%d%lld", &ida, &idb, &val);
#endif
#ifdef BRUTE_FORCE
				bf.add(ida, idb, val);
#endif
				bit.add(ida, idb, val);
			}

#ifdef GEN_INPUT
			else if (comm == 1) {
				printf("minus %d %d %lld\n", ida, idb, val);
#else
			else if (strcmp(command, "minus") == 0) {
				scanf("%d%d%lld", &ida, &idb, &val);
#endif
#ifdef BRUTE_FORCE
				bf.add(ida, idb, -val);
#endif
				bit.add(ida, idb, -val);
			}


#ifdef GEN_INPUT
			else if (comm == 2) {
				printf("addin %d %lld\n", ida, val);
#else
			else if (strcmp(command, "addin") == 0) {
				scanf("%d%lld", &ida, &val);
#endif
#ifdef BRUTE_FORCE
				bf.add(ida, ida, val);
#endif
				bit.add(ida, ida, val);
			}
#ifdef GEN_INPUT
			else if (comm == 3) {
				printf("minusin %d %lld\n", ida, val);
#else
			else if (strcmp(command, "minusin") == 0) {
				scanf("%d%lld", &ida, &val);
#endif
#ifdef BRUTE_FORCE
				bf.add(ida, ida, -val);
#endif
				bit.add(ida, ida, -val);
			}

#ifdef GEN_INPUT
			else if (comm == 4) {
				printf("reset %d %lld\n", ida, val);
#else
			else if (strcmp(command, "reset") == 0) {
				scanf("%d%lld", &ida, &val);
#endif
#ifdef BRUTE_FORCE
				bf.set(ida, val);
#endif
				bit.set(ida, val);
			}

#ifdef GEN_INPUT
			else if (comm == 5) {
				printf("find %d\n", ida);
#else
			else if (strcmp(command, "find") == 0) {
				scanf("%d", &ida);
#endif
#ifndef NO_OUTPUT
				printf("%lld\n", bit.get(ida));
#endif
			}
#ifdef BRUTE_FORCE
			bit.dump(dump);
			if (bf.tree != dump) {
				cout << "NOOOOO! " << endl;
				exit(1);
			}
#endif
		}
#ifndef NO_OUTPUT
		if (case_id < num_cases) {
			printf("\n");
		}
#endif
	}

	return 0;
}

