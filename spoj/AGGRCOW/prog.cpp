#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

/**
* Author: Derek Illchuk dillchuk@gmail.com
*
* Binary search, maximum minimum.
*/

bool can_place(const vector<uint32>& stalls, uint32 num_cows, uint32 distance) {

	uint32 prev_ix = 0;
	uint32 s_ix = 1;
	num_cows--;

	for (; s_ix < stalls.size() && num_cows > 0; s_ix++) {
		if (stalls[s_ix] - stalls[prev_ix] < distance) continue;
		num_cows--; // watch for underflow
		prev_ix = s_ix;
	}
	return (num_cows == 0);
}

int main( void )
{
	ios::sync_with_stdio(false);

	uint32 num_cases = 0; cin >> num_cases;
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {

		uint32 num_stalls = 0, num_cows = 0;
		cin >> num_stalls >> num_cows;

		vector<uint32> stalls(num_stalls);
		for (uint32 s_ix = 0; s_ix < stalls.size(); s_ix++) {
			cin >> stalls[s_ix];
		}
		sort(stalls.begin(), stalls.end());

		uint32 begin = 1, end = stalls.back() - stalls.front() + 1;
		uint32 answer = 1;
		while (begin < end) {
			uint32 mid = (begin + end) / 2;

			/**
			* This will work; is there something higher?
			*/
			if (can_place(stalls, num_cows, mid)) {
				answer = mid;
				begin = mid + 1;
				continue;
			}

			/**
			* This won't work; look lower.
			*/
			end = mid;
		}
		cout << answer << endl;
	}
	
    return 0;
}
