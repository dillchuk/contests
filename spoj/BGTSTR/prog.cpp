/**
* Marshalled by Derek Illchuk dillchuk@gmail.com
* Suffix tree, suffix array, suffix sort, matching substrings.
*/

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <algorithm>

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

#define SENTINEL '\x7B' // immediately after 'z'

/**
* `suffix_sort` O(n lgn) from http://web.stanford.edu/class/cs97si/suffix-array.pdf
* Suffix Arrays - A Programming Contest Approach
* (There are O(n) algorithms.)
*/
struct entry {
	int nr[2], p;
};
bool entry_less(struct entry a, struct entry b) {
	return (a.nr[0] == b.nr[0]) ? (a.nr[1] < b.nr[1]) : (a.nr[0] < b.nr[0]);
}
vector<uint32> suffix_sort(char* input, uint32 input_len) {

	vector< vector<uint32> > P;
	P.push_back(vector<uint32>(input_len));
	for (uint32 i = 0; i < input_len; i++) {
		P[0][i] = input[i] - 'a';
	}
	uint32 stp = 1;
	vector<entry> L(input_len);
	for (uint32 cnt = 1; cnt >> 1 < input_len; stp++, cnt <<= 1) {
		P.push_back(vector<uint32>(input_len));
		for (uint32 i = 0; i < input_len; i++) {
			L[i].nr[0] = P[stp - 1][i];
			L[i].nr[1] = (i + cnt < input_len) ? P[stp - 1][i + cnt] : -1;
			L[i].p = i;
		}
		sort(L.begin(), L.begin() + input_len, entry_less);
		for (uint32 i = 0; i < input_len; i++) {
			P[stp][L[i].p] = (i > 0 && L[i].nr[0] == L[i - 1].nr[0] && L[i].nr[1] == L[i - 1].nr[1]) ?
				P[stp][L[i - 1].p] : i;
		}
	}

	/**
	* Go from (original column => rank) to (ordering => original column)
	*/
	vector<uint32> suftab(input_len);
	for (uint32 i = 0; i < suftab.size(); i++) {
		suftab[P.back()[i]] = i;
	}
	return suftab;
}


/**
* AboKurOhl2004.pdf Algorithm 4.1 Computation of lcp-intervals.
* Replacing suffix trees with enhanced suffix arrays - Abouelhoda et al.
*/
vector<uint32> lcp_table(const char* input, const vector<uint32>& suftab) {
	vector<uint32> sufinv(suftab.size()), lcptab(suftab.size());
	for (uint32 i = 0; i < sufinv.size(); i++) {
		sufinv[suftab[i]] = i;
	}
	uint32 h = 0;
	for (uint32 i = 0; i < sufinv.size(); i++) {
		if (sufinv[i] == 0) continue;
		uint32 k = suftab[sufinv[i] - 1];
		while (input[i + h] == input[k + h]) {
			h++;
		}
		lcptab[sufinv[i]] = h;
		if (h > 0) {
			h--;
		}
	}
	return lcptab;
}

struct interval_type {
	interval_type(uint32 lcp = 0, uint32 lb = 0, uint32 rb = 0xFFFFFFFF): lcp(lcp), lb(lb), rb(rb) {}
	uint32 lcp;
	uint32 lb, rb;

	uint32 size() const {
		return this->rb - this->lb + 1;
	}
};
vector<interval_type> lcp_intervals(const vector<uint32>& lcptab) {
	vector<interval_type> result, stack;
	stack.push_back(interval_type());
	for (uint32 i = 0; i < lcptab.size(); i++) {
		int32 lb = i - 1;
		while (lcptab[i] < stack.back().lcp) {
			interval_type popped = stack.back();
			stack.pop_back();
			popped.rb = i - 1;
			result.push_back(popped);
			lb = popped.lb;
		}
		if (lcptab[i] > stack.back().lcp) {
			stack.push_back(interval_type(lcptab[i], lb));
		}
	}
	return result;
}

/**
* Find longest substring which occurs `min_count` times, or -1 if not found.
* Answer is the rightmost substring start-index (from problem description).
*/
struct solve_type {
	solve_type():lcp(0), index(-1) {}
	uint32 lcp;
	int32 index;
};
solve_type solve(const vector<uint32>& suftab, const vector<interval_type>& intervals, uint32 min_count) {
	solve_type solution;

	/**
	* First determine maximum length.
	*/
	for (vector<interval_type>::const_iterator it = intervals.begin();
		it != intervals.end(); ++it) {
		if (it->size() < min_count) continue;
		solution.lcp = max(solution.lcp, it->lcp);
	}
	for (vector<interval_type>::const_iterator it = intervals.begin();
		it != intervals.end(); ++it) {
		if (it->size() < min_count) continue;
		solution.lcp = max(solution.lcp, it->lcp);
	}

	/**
	* Then pin down rightmost (matching) substring;
	* must go through `suftab` to get actual/input index.
	*/
	for (vector<interval_type>::const_iterator it = intervals.begin();
		it != intervals.end(); ++it) {
		if (it->size() < min_count || it->lcp < solution.lcp) continue;

		for (uint32 i = it->lb; i <= it->rb; i++) {
			solution.index = max(solution.index, static_cast<int32>(suftab[i]));
		}
	}
	return solution;
}

#define MAX_INPUT_LEN 50000
//#define GEN_INPUT MAX_INPUT_LEN

int main(void) {
	ios::sync_with_stdio(false);
#ifdef GEN_INPUT
	{
		srand(time(0));
		uint32 num_cases = 1;
		cout << num_cases << endl;
		for (uint32 i = 0; i < num_cases - 1; i++) {
			for (uint32 j = 0; j < GEN_INPUT; j++) {
				cout << (char)('a' + (rand() % 10));
			}
			cout << ' ' << (rand() % 11) << endl;
		}
		// Fibonacci sequence
		string prev = "a";
		string curr = "ab";
		for (;;) {
			string next = prev + curr;
			prev = curr;
			curr = next;
			if (curr.size() > MAX_INPUT_LEN) {
				cout << prev << endl << 1000;
				break;
			}
		}
		return 0;
	}
#endif

	uint32 num_cases = 0;
	scanf("%d", &num_cases);
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {
		char input[MAX_INPUT_LEN + 2] = "";
		scanf("%s", &input);
		uint32 input_len = strlen(input);
		uint32 orig_len = input_len;
		input[input_len + 1] = '\0';
		input[input_len] = SENTINEL;
		input_len++;

		uint32 min_count = 1;
		scanf("%d", &min_count);

		/**
		* Algorithm doesn't like when substring/solution matches entire string.
		*/
		if (min_count == 1) {
			cout << orig_len << ' ' << 1 << endl;
			continue;
		}

		/**
		* `suftab` and `lcptab` are described in Abouelhoda PDF.
		*/
		vector<uint32> suftab = suffix_sort(input, input_len);
		vector<uint32> lcptab = lcp_table(input, suftab);

		vector<interval_type> intervals = lcp_intervals(lcptab);
		solve_type solution = solve(suftab, intervals, min_count);
		if (solution.index >= 0) {
			cout << solution.lcp << ' ' << (solution.index + 1) << endl;
		}
		else {
			cout << "HATE" << endl;
		}
	}

	return 0;
}
