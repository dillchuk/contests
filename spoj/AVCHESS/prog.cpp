#include <iostream>
#include <vector>
#include <set>
#include <cmath>
#include <algorithm>
#include <iterator>
#include <list>
#include <ctime>

/**
* Simple chess problem:
*   - knight, rook, bishop on board
*   - move K R B K R B etc.
*   - minimum number of moves to find K on original B, R on original K, B on original R
*
* Solved not by simulation but by determining the minimum number of moves for each piece to get to target.
* I watch for blocking out of the gate -- but assume no blocking after since there are so many ways to avoid.
*
* NOTE: I don't think this is right.  It doesn't account for the rook getting stuck behind bishop:
* 1
* 1 3 5 3 3 3
*/

using namespace std;
typedef unsigned long uint32;
typedef long int32;
typedef unsigned long long uint64;
typedef long long int64;

typedef pair<int32, int32> ipr;

#define BOARD_END 8

struct km_type {
	km_type(): pos(BOARD_END, BOARD_END), num_turns(0) {}
	km_type(ipr pos, uint32 num_turns = 0) : num_turns(num_turns), pos(pos) {}

	vector<km_type> calc_next() {
		vector<km_type> next;
		for (int32 i = -2; i <= 2; i++) {
			if (i == 0) continue;
			for (int32 j = -2; j <= 2; j++) {
				if (j == 0) continue;

				if (abs(i) + abs(j) != 3) continue;
				ipr pos_next(this->pos.first + i, this->pos.second + j);
				if (pos_next.first < 0 || pos_next.first >= BOARD_END) continue;
				if (pos_next.second < 0 || pos_next.second >= BOARD_END) continue;

				next.push_back(km_type(pos_next, this->num_turns + 1));
			}
		}
		return next;
	}

	uint32 num_turns;
	ipr pos;
};
bool operator<(const km_type& a, const km_type& b) {
	if (a.num_turns < b.num_turns) return true;
	if (a.num_turns > b.num_turns) return false;
	return (a.pos < b.pos);
}

int32 min_knight_moves(ipr k_pos, ipr r_pos, ipr b_pos) {
	ipr target_pos = b_pos;

	km_type km(k_pos);
	vector<km_type> moves_first = km.calc_next();
	for (uint32 i = 0; i < moves_first.size();) {

		/**
		* First move can be blocked.
		*/
		if (moves_first[i].num_turns == 1) {
			if (moves_first[i].pos == r_pos || moves_first[i].pos == b_pos) {
				moves_first.erase(moves_first.begin() + i);
				continue;
			}
		}
		i++;
	}

	/**
	* Knight moves are searched breadth-first.
	*/
	set<km_type> queue(moves_first.begin(), moves_first.end());
	while (!queue.empty()) {
		km_type curr = *queue.begin();
		queue.erase(queue.begin());

		if (curr.pos == target_pos) {
			return curr.num_turns;
		}

		vector<km_type> next = curr.calc_next();
		for (uint32 i = 0; i < next.size(); i++) {
			queue.insert(next[i]);
		}
	}

	/**
	* Knight blocked from the get-go.
	*/
	return -1;
}

int32 min_bishop_moves(ipr b_pos, ipr r_pos) {
	ipr target_pos = r_pos;

	/**
	* Bishop can't get to half of the board.
	*/
	bool bish_diag = (b_pos.first + b_pos.second) % 2;
	bool target_diag = (target_pos.first + target_pos.second) % 2;
	if (bish_diag != target_diag) {
		return -1;
	}
	
	/**
	* Might take one; don't worry about being blocked because it moves last out of the gate.
	*/
	ipr delta(
		target_pos.first - b_pos.first,
		target_pos.second - b_pos.second
	);
	if (abs(delta.first) == abs(delta.second)) return 1;

	/**
	* The rest always in two.
	*/
	return 2;
}

int32 min_rook_moves(ipr r_pos, ipr k_pos) {
	ipr target_pos = k_pos;

	ipr delta(
		target_pos.first - r_pos.first,
		target_pos.second - r_pos.second
	);
	return (delta.first == 0 || delta.second == 0) ? 1 : 2;
}

int main( void )
{
	ios::sync_with_stdio(false);

	uint32 num_cases = 0; cin >> num_cases;
	for (uint32 case_ix = 0; case_ix < num_cases; case_ix++) {
		ipr k_pos; cin >> k_pos.first >> k_pos.second;
		ipr r_pos; cin >> r_pos.first >> r_pos.second;
		ipr b_pos; cin >> b_pos.first >> b_pos.second;

		int32 min_k = min_knight_moves(k_pos, r_pos, b_pos);
		int32 min_r = min_rook_moves(r_pos, k_pos);
		int32 min_b = min_bishop_moves(b_pos, r_pos);

		/**
		* Not possible.
		*/
		int32 result = -1;
		if (min_k < 2 || min_b < 1 || min_r < 1) {
			
		}

		/**
		* Early on, the last move depends.
		*/
		else if (min_k == 2) {
			min_r = max(min_r, min_b);
			result = min_k + min_r + min_b;
		}

		/**
		* Later on, the last move is the knight.
		*/
		else {
			result = 3 * min_k - 2;
		}

		cout << result << endl;
	}

	return 0;
}
